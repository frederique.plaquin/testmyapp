import {Utilisateur} from "../app/model/Utilisateur";
import {Projet} from "../app/model/projet";
import {Plan} from "../app/model/plan";
import {User} from "../app/model/user";

export const user: User = {
  id: 0,
  nom: '',
  prenom: '',
  imageUrl: '',
  role: ''
};

export const userSet = false;

export const projet: Projet = {
  nom: '',
  version: ''
}

export const projetSet = false;

export const plan: Plan = {
  nom: '',
  projet_projet_id: 0,
  archive: false
}

export const planSet = false;

export const composant = '';

export const composantSet = false;
