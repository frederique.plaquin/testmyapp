export const environment = {
  production: true,
  apiBaseUrl: 'http://10.8.0.11:8080',
  createUserUrl:"http://10.8.0.11:8080/utilisateur/add",
  loginUserUrl:"http://10.8.0.11:8080/utilisateur/connexion"
};
