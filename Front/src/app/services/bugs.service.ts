import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Bug} from "../model/bug";



@Injectable({
  providedIn: 'root'
})
export class BugsService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr: any = localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) {
  }

  // envoi de la bug à créer vers le backend
  public addBug(bug: Bug): Observable<Bug> {
    return this.http.post<Bug>(`${this.apiServerUrl}/bug/add`, bug)
  }

  // recuperation des bugs depuis la bdd
  public getBugs(): Observable<Bug[]> {
    return this.http.get<Bug[]>(`${this.apiServerUrl}/bug/all`, {headers: {Authorization: this.tokenStr}})
  }

  // update bug
  public updateBug(bug: Bug): Observable<Bug> {
    return this.http.put<Bug>(`${this.apiServerUrl}/bug/update`, bug, {headers: {Authorization: this.tokenStr}});
  }

  // delete bug
  public deleteBug(bugId: number): Observable<void> {  // void because we will just return a status
    return this.http.delete<void>(`${this.apiServerUrl}/bug/delete/${bugId}`, {headers: {Authorization: this.tokenStr}});
  }

  // delete bug
  public archiveBug(bug: Bug): Observable<Bug> {  // void because we will just return a status
    return this.http.put<Bug>(`${this.apiServerUrl}/bug/archive/`, bug, {headers: {Authorization: this.tokenStr}});
  }

  public getBugById(id: number | undefined): Observable<Bug> {
    return this.http.get<Bug>(`${this.apiServerUrl}/bug/find/${id}`, {headers: {Authorization: this.tokenStr}});
  }

  public getBugsByProjetId(id: string): Observable<Bug[]> {
    return this.http.get<Bug[]>(`${this.apiServerUrl}/bug/findByProjet/${id}`, {headers: {Authorization: this.tokenStr}})
  }

  public getBugsByFonctionnaliteId(id: string): Observable<Bug[]> {
    return this.http.get<Bug[]>(`${this.apiServerUrl}/bug/findByFonctionnalite/${id}`, {headers: {Authorization: this.tokenStr}})
  }

  public getActivesBugByProjetId(id: string): Observable<Bug[]> {
    return this.getBugsByProjetId(id).pipe(
      // @ts-ignore
      map(data => data.filter(bug => bug.archive = false)));
  }

  public getActivesBugByFonctionnaliteId(id: string): Observable<Bug[]> {
    return this.getBugsByFonctionnaliteId(id).pipe(
      // @ts-ignore
      map(data => data.filter(bug => bug.archive = false)));
  }

}
