import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Projet} from "../model/projet";
import {Plan} from "../model/plan";
import {Groupe} from "../model/groupe";
import {Fonctionnalite} from "../model/fonctionnalite";
import {Utilisateur} from "../model/Utilisateur";
import {ProjetFull} from "../model/projetFull";
import {Bug} from "../model/bug";

@Injectable({
  providedIn: 'root'
})
export class ProjetService {
  public tesplansProjet: Plan[] =  [];
  public groupesFonctionnalitesProjet: Groupe[] = [];
  public fonctionnalitesProjet: Fonctionnalite[] = [];
  public projetBugs: Bug[] = [];
  public projetMembres: Utilisateur[] = [];
  public utilisateurProjetsFull: ProjetFull[] = [];

  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) { }

  // envoi du projet à créer vers le backend
  public addProjet(projet: Projet): Observable<Projet> {


    return this.http.post<Projet>(`${this.apiServerUrl}/projet/add`,
     projet);
  }

  // recuperation des projets depuis la bdd
  public getProjets(): Observable<Projet[]> {
    return this.http.get<Projet[]>(`${this.apiServerUrl}/projet/all`,{headers:{Authorization:this.tokenStr}});
  }

  public getUserProjets(id: String): Observable<Projet[]> {
    return this.http.get<Projet[]>(`${this.apiServerUrl}/projet/findByUser/${id}`,{headers:{Authorization : this.tokenStr}});
  }

  public getProjetById(id: number | undefined): Observable<Projet> {
    return this.http.get<Projet>(`${this.apiServerUrl}/projet/find/${id}`,{headers:{Authorization : this.tokenStr}});
  }

  public updateProjet(projet: Projet): Observable<Projet> {
    return this.http.put<Projet>(`${this.apiServerUrl}/projet/update`, projet, {headers: {Authorization: this.tokenStr}});
  }

  public deleteProjet(projetId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/utilisateur/delete/${projetId}`, {headers: {Authorization: this.tokenStr}});
  }

  public getProjetsDetails(utilisateur: Utilisateur): ProjetFull[] {
    // @ts-ignore
    this.getUserProjets(utilisateur.id.toString()).subscribe(
      (response: Projet[]) => {
        for(let projet of response){
          if(projet.testPlans){
            for(let testplan of projet.testPlans) {
              this.tesplansProjet.push(testplan);
              if(testplan.groupeFonctionnalites){
                for(let groupe of testplan.groupeFonctionnalites){
                  this.groupesFonctionnalitesProjet.push(groupe);
                  if(groupe.fonctionnalites) {
                    for(let fonctionnalite of groupe.fonctionnalites){
                      this.fonctionnalitesProjet.push(fonctionnalite);
                    }
                  }
                }
              }
            }
          }
          if(projet.id) {
            let projetFull: ProjetFull = {
              id: projet.id,
              nom: projet.nom,
              details: projet.details,
              testPlans: this.tesplansProjet,
              groupesFonctionnalites: this.groupesFonctionnalitesProjet,
              fonctionnalites: this.fonctionnalitesProjet,
              bugs: this.projetBugs,
              membres: this.projetMembres,
              version: projet.version,
              lastVisit: projet.lastVisit,
              archive: projet.archive
            }
            this.utilisateurProjetsFull.push(projetFull);
          }
        }
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
    return this.utilisateurProjetsFull;
  }

  public getProjetDetails(projet: Projet): ProjetFull {
    let projetFull: ProjetFull;
    let projetTestplans = projet.testPlans
    let projetGroupesFonctionnalites: Groupe[] = [];
    let projetFonctionnalites: Fonctionnalite[] = [];

    if(projetTestplans)
    for(let plan of projetTestplans) {
      if(plan.groupeFonctionnalites) {
        for(let groupe of plan.groupeFonctionnalites) {
          projetGroupesFonctionnalites.push(groupe);
        }
      }
    }

    if(projetGroupesFonctionnalites)
      for(let groupe of projetGroupesFonctionnalites) {
        if(groupe.fonctionnalites)
          for(let fonctionnalite of groupe.fonctionnalites) {
            projetFonctionnalites.push(fonctionnalite);
          }
      }

    projetFull = {
      // @ts-ignore
      id: projet.id,
      nom: projet.nom,
      details: projet.details,
      testPlans: projetTestplans,
      groupesFonctionnalites: projetGroupesFonctionnalites,
      fonctionnalites: projetFonctionnalites,
      bugs: this.projetBugs,
      membres: this.projetMembres,
      version: projet.version,
      lastVisit: projet.lastVisit,
      archive: projet.archive
    }
    return projetFull;
  }

}


