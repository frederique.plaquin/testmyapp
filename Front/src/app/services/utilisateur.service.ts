import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { Utilisateur } from "../model/Utilisateur"

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr: any = localStorage.getItem("token") ?? '[]';

  constructor(private http: HttpClient) {
  }

  public getProjets(): Observable<any> {
    return this.http.get<Utilisateur[]>(`${this.apiServerUrl}/projet/all`, {headers: {Authorization: this.tokenStr}});
  }

  public getUtilisateurs(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(`${this.apiServerUrl}/utilisateur/all`, {headers: {Authorization: this.tokenStr}})
  }

  public getUtilisateurById(id: number): Observable<Utilisateur> {
    return this.http.get<Utilisateur>(`${this.apiServerUrl}/utilisateur/find/${id}`, {headers: {Authorization: this.tokenStr}})
  }

  public addUtilisateurs(utilisateur: Utilisateur): Observable<Utilisateur> {

    return this.http.post<Utilisateur>(`${this.apiServerUrl}/utilisateur/add`, utilisateur, {headers: {Authorization: this.tokenStr}})
  }

  public updateUtilisateurs(utilisateur: Utilisateur): Observable<Utilisateur> {
    return this.http.put<Utilisateur>(`${this.apiServerUrl}/utilisateur/update`, utilisateur, {headers: {Authorization: this.tokenStr}});
  }

  public deleteUtilisateurs(utilisateurId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/utilisateur/delete/${utilisateurId}`, {headers: {Authorization: this.tokenStr}});
  }
}
