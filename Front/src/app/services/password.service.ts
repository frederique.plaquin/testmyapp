import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Password} from "../model/password";

@Injectable({
  providedIn: 'root'
})
export class PasswordService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public addPassword(password: Password): Observable<Password> {
    return this.http.post<Password>(`${this.apiServerUrl}/password/add`, password);
  }
}

