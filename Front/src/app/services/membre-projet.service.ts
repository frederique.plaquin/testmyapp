import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Projet} from "../model/projet";
import {MembreProjet} from "../model/membreProjet";

@Injectable({
  providedIn: 'root'
})
export class MembreProjetService {

  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http:HttpClient) { }


  public getMembres(): Observable<MembreProjet[]> {
    return this.http.get<MembreProjet[]>(`${this.apiServerUrl}/membreProjet/all`,{headers:{Authorization : this.tokenStr}})
  }

  public addMembre(membreProjet: MembreProjet): Observable<MembreProjet> {

    return this.http.post<MembreProjet>(`${this.apiServerUrl}/membreProjet/add`, membreProjet,{headers:{Authorization : this.tokenStr}})
  }

  public updateMembre(membreProjet: MembreProjet): Observable<MembreProjet>{
    return this.http.put<MembreProjet>(`${this.apiServerUrl}/membreProjet/update`, membreProjet,{headers:{Authorization : this.tokenStr}});
  }

  public deleteMembre(membreProjetId: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/membreProjet/delete/${membreProjetId}`,{headers:{Authorization : this.tokenStr}});
  }

  getDevs(projet: Projet) {
    let projetDevs: MembreProjet[] = [];
    if(projet.membresProjets){
      for(let membre of projet.membresProjets) {
        if(membre.role.id == 3 || membre.role.id == 2) {
          projetDevs.push(membre);
        }
      }
    }
    return projetDevs;
  }


}
