import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {TypeErreur} from "../model/typeErreur";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TypesErreursService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token") ?? '[]';

  constructor(private http: HttpClient) { }

  public addTypeErreur(type: TypeErreur): Observable<TypeErreur> {
    return this.http.post<TypeErreur>(`${this.apiServerUrl}/type/add`, type)
  }

  public getTypesErreurs(): Observable<TypeErreur[]> {
    return this.http.get<TypeErreur[]>(`${this.apiServerUrl}/type/all`, {headers:{Authorization:this.tokenStr}})
  }

  public updateTypeErreur(type: TypeErreur): Observable<TypeErreur> {
    return this.http.put<TypeErreur>(`${this.apiServerUrl}/type/update`, type, {headers:{Authorization:this.tokenStr}})
  }

  public deleteTypeErreur(typeId: number): Observable<void> {
    return this.http.delete<void>(`${this.apiServerUrl}/type/delete/${typeId}`,{headers:{Authorization : this.tokenStr}})
  }

  public archiveTypeErreur(type: TypeErreur): Observable<TypeErreur> {
    return this.http.put<TypeErreur>(`${this.apiServerUrl}/type/archive/`, type, {headers:{Authorization : this.tokenStr}})
  }

  public getTypeErreurById(id: number): Observable<TypeErreur> {
    return this.http.get<TypeErreur>(`${this.apiServerUrl}/type/find/${id}`,{headers:{Authorization : this.tokenStr}})
  }
}
