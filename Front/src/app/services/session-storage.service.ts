// source: https://firstclassjs.com/persist-data-using-local-storage-and-angular/
// making data persistent between browser windows (creating cash so data removed if cash cleared)
import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {
  localStorage: Storage;

  changes$ = new Subject();

  constructor() {
    // create local storage
    this.localStorage = window.sessionStorage;
  }

  get(key: string): any {
    if (this.isLocalStorageSupported) {
      // @ts-ignore
      return JSON.parse(this.localStorage.getItem(key)  ?? '[]');
    }
    return null;
  }

  set(key: string, value: any): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.setItem(key, JSON.stringify(value));
      this.changes$.next({
        type: 'set',
        key,
        value
      });
      return true;
    }
    return false;
  }

  remove(key: string): boolean {
    if (this.isLocalStorageSupported) {
      this.localStorage.removeItem(key);
      this.changes$.next({
        type: 'remove',
        key
      });
      return true;
    }
    return false;
  }

  get isLocalStorageSupported(): boolean {
    return !!this.localStorage
  }
}
