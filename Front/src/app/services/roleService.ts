import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Projet} from "../model/projet";
import {Plan} from "../model/plan";
import {Groupe} from "../model/groupe";
import {Fonctionnalite} from "../model/fonctionnalite";
import {Utilisateur} from "../model/Utilisateur";
import {ProjetFull} from "../model/projetFull";
import {Bug} from "../model/bug";
import {Role} from "../model/role";

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) { }

  public getUserRole(id: String): Observable<Role> {
    return this.http.get<Role>(`${this.apiServerUrl}/role/findByUser/${id}`,{headers:{Authorization : this.tokenStr}});
  }

}


