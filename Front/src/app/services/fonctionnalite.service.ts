import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Fonctionnalite} from "../model/fonctionnalite";

@Injectable({
  providedIn: 'root'
})
export class FonctionnaliteService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) { }

  // envoi de la fonctionnalité à créer vers le backend
  public addFonctionnalite(fonctionnalite: Fonctionnalite): Observable<Fonctionnalite> {
    return this.http.post<Fonctionnalite>(`${this.apiServerUrl}/fonctionnalite/add`, fonctionnalite, {headers:{Authorization:this.tokenStr}})
  }

  // recuperation des fonctionnalités depuis la bdd
  public getFonctionnalites(): Observable<Fonctionnalite[]> {
    return this.http.get<Fonctionnalite[]>(`${this.apiServerUrl}/fonctionnalite/all`, {headers:{Authorization:this.tokenStr}})
  }

  // update fonctionnalite
  public updateFonctionnalite(fonctionnalite: Fonctionnalite): Observable<Fonctionnalite> {
    return this.http.put<Fonctionnalite>(`${this.apiServerUrl}/fonctionnalite/update`, fonctionnalite,{headers:{Authorization:this.tokenStr}});
  }

  // delete fonctionnalite
  public deleteFonctionnalite(fonctionnaliteId: number): Observable<void> {  // void because we will just return a status
    return this.http.delete<void>(`${this.apiServerUrl}/fonctionnalite/delete/${fonctionnaliteId}`,{headers:{Authorization : this.tokenStr}});
  }

  // delete fonctionnalite
  public archiveFonctionnalite(fonctionnalite: Fonctionnalite): Observable<Fonctionnalite> {  // void because we will just return a status
    return this.http.put<Fonctionnalite>(`${this.apiServerUrl}/fonctionnalite/archive/`, fonctionnalite,{headers:{Authorization : this.tokenStr}});
  }

  public getFonctionnaliteById(id: number | undefined): Observable<Fonctionnalite> {
    return this.http.get<Fonctionnalite>(`${this.apiServerUrl}/fonctionnalite/find/${id}`,{headers:{Authorization : this.tokenStr}});
  }
}
