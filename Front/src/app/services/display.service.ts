import { Injectable } from '@angular/core';
import {Projet} from "../model/projet";
import {Router} from "@angular/router";
import {Plan} from "../model/plan";
import {User} from "../model/user";
import {SessionStorageService} from "./session-storage.service";
import {Groupe} from "../model/groupe";
import {Bug} from "../model/bug";


@Injectable({
  providedIn: 'root'
})
export class DisplayService {
  userSet?: boolean;
  projetSet?: boolean;
  planSet?: boolean;
  groupeSet?: boolean;
  composantSet?: boolean;
  bugsSet?: boolean;
  user?: User;
  projet?: Projet;
  plan?: Plan;
  groupe?: Groupe;
  bugs?: Bug[];
  composant?: String;
  url?: String;
  loggedInUser?: User;

  constructor(
    private router: Router,
    private sessionStorageService: SessionStorageService) {
  }

  public setData(type: String, objet: any){
    if(type == 'user') {
      this.userSet = true;
      this.user = objet;
      this.persist('loggedInUtilisateur', objet);
    } else if(type == 'composant') {
      if (objet == 'dashboard'){
        this.persist('display', 'userDashboard');
      } if (objet == 'dashboard-projet') {
        this.persist('display', 'projetDashboard');
      } if (objet == 'dashboard-plan') {
        this.persist('display', 'planDashboard');
      } if (objet == 'dashboard-GROUPE') {
        this.persist('display', 'groupeDashboard');
      }
      this.composantSet = true;
      this.composant = objet;
      this.persist('composant', objet);
    } else if(type == 'projet') {
      this.projetSet = true;
      this.projet = objet;
      this.persist('projet', objet);
    } else if(type == 'plan') {
      this.planSet = true;
      this.plan = objet;
      this.persist('plan', objet);
    } else if(type == 'groupe') {
      this.groupeSet = true;
      this.groupe = objet;
      this.persist('groupe', objet);
    } else if(type == 'bugs') {
      this.bugsSet = true;
      this.bugs = objet;
      this.persist('bugs', objet);
    }
  }

  public unsetData(type: String){
    if(type == 'user') {
      this.userSet = false;
      delete this.user;
    } else if(type == 'composant') {
      this.composantSet = false;
      delete this.composant;
    } else if(type == 'projet') {
      this.projetSet = false;
      delete this.projet;
    } else if(type == 'plan') {
      this.planSet = false;
      delete this.plan;
    } else if(type == 'groupe') {
      this.groupeSet = false;
      delete this.groupe;
    } else if(type == 'bugs') {
      this.bugsSet = false;
      delete this.bugs;
    }
  }

  public displayManager(): any {
    if(this.composant == 'dashboard') {
      // @ts-ignore
      this.url = `/${this.composant}/${this.user.id}`;
    } else if(this.composant == 'dashboard-projet') {
      if (this.projet) {
        this.url = `/dashboard-projet/${this.projet.id}`;
      } else {
        alert("L'identifiant du projet est manquant !");
      }
    } else if(this.composant == 'dashboard-plan') {
      this.url = `/dashboard-plan/${this.plan?.id}`;
    } else if(this.composant == 'dashboard-groupe') {
      this.url = `/dashboard-groupe/${this.groupe?.id}`;
    } else if(this.composant == 'gestionnaire-de-bugs') {
      this.url = `/gestionnaire-de-bugs`
    }
    return this.url;
  }

  persist(key: string, value: any) {
    this.sessionStorageService.set(key, value);
  }


}
