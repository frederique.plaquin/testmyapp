import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Plan} from "../model/plan";

@Injectable({
  providedIn: 'root'
})
export class PlanService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) { }

  // envoi du plan de test à créer vers le backend
  public addPlan(plan: Plan): Observable<Plan> {
    return this.http.post<Plan>(`${this.apiServerUrl}/plan/add`, plan,{headers:{Authorization:this.tokenStr}})
  }

  // recuperation des projets depuis la bdd
  public getPlans(): Observable<Plan[]> {
    return this.http.get<Plan[]>(`${this.apiServerUrl}/plan/all`,{headers:{Authorization:this.tokenStr}})
  }

  // update plan
  public updatePlan(plan: Plan): Observable<Plan> {
    return this.http.put<Plan>(`${this.apiServerUrl}/plan/update`, plan,{headers:{Authorization:this.tokenStr}});
  }

  // delete plan
  public deletePlan(planId: number): Observable<void> {  // void because we will just return a status
    return this.http.delete<void>(`${this.apiServerUrl}/plan/delete/${planId}`,{headers:{Authorization : this.tokenStr}});
  }

  // delete plan
  public archivePlan(plan: Plan): Observable<Plan> {  // void because we will just return a status
    return this.http.put<Plan>(`${this.apiServerUrl}/plan/archive/`, plan,{headers:{Authorization : this.tokenStr}});
  }

  public getPlanById(id: number | undefined): Observable<Plan> {
    return this.http.get<Plan>(`${this.apiServerUrl}/plan/find/${id}`,{headers:{Authorization : this.tokenStr}});
  }
}
