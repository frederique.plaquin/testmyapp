import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

private createUserUrl=environment.createUserUrl;
private loginUserUrl=environment.loginUserUrl;
islogin = false;
  admin = false;
  suser = false;

  constructor(private http : HttpClient) { }

   httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  
//send query to the back to create a user
  public createUtilisateur(user : Object):Observable<Object>{
    return this.http.post(`${this.createUserUrl}`, user);// this.httpOptions);
  }

 //send email and password to login
 public login(auth:Object):Observable<Object>  {
  return this.http.post(`${this.loginUserUrl}`,(auth));
 }




}
