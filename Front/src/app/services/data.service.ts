// source : https://fireship.io/lessons/sharing-data-between-angular-components-four-methods/
import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private projetSource = new BehaviorSubject(JSON);
  currentProjet = this.projetSource.asObservable();



  private utilisateurSource = new BehaviorSubject(Object);
  currentUtilisateur = this.projetSource.asObservable();

  constructor() { }

  changeProjet(projet: any) {
    this.projetSource.next(projet)
  }

  changeUtilisateur(utilisateur: any) {
    this.projetSource.next(utilisateur)
  }
}


