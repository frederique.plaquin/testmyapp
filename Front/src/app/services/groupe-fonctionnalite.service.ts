import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Groupe } from "../model/groupe";

@Injectable({
  providedIn: 'root'
})
export class GroupeFonctionnaliteService {
  // url du backend
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http: HttpClient) { }

// envoi du plan de test à créer vers le backend
  public addGroupe(groupe: Groupe): Observable<Groupe> {
    return this.http.post<Groupe>(`${this.apiServerUrl}/groupe/add`, groupe,{headers:{Authorization:this.tokenStr}})
  }

  // recuperation des projets depuis la bdd
  public getGroupes(): Observable<Groupe[]> {
    return this.http.get<Groupe[]>(`${this.apiServerUrl}/groupe/all`,{headers:{Authorization:this.tokenStr}})
  }

  // update plan
  public updateGroupe(groupe: Groupe): Observable<Groupe> {
    return this.http.put<Groupe>(`${this.apiServerUrl}/groupe/update`, groupe,{headers:{Authorization:this.tokenStr}});
  }

  // delete plan
  public deleteGroupe(groupeId: number): Observable<void> {  // void because we will just return a status
    return this.http.delete<void>(`${this.apiServerUrl}/groupe/delete/${groupeId}`,{headers:{Authorization : this.tokenStr}});
  }

  // delete plan
  public archiveGroupe(groupe: Groupe): Observable<Groupe> {  // void because we will just return a status
    return this.http.put<Groupe>(`${this.apiServerUrl}/groupe/archive/`, groupe,{headers:{Authorization : this.tokenStr}});
  }

  public getGroupeById(id: number | undefined): Observable<Groupe> {
    return this.http.get<Groupe>(`${this.apiServerUrl}/groupe/find/${id}`,{headers:{Authorization : this.tokenStr}});
  }
}
