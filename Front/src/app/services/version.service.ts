import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Version} from "../model/version";

@Injectable({
  providedIn: 'root'
})
export class VersionService {
  private apiServerUrl = environment.apiBaseUrl;
  tokenStr:any=localStorage.getItem("token")  ?? '[]';

  constructor(private http:HttpClient) { }


  public getVersions(): Observable<Version[]> {
    return this.http.get<Version[]>(`${this.apiServerUrl}/version/all`,{headers:{Authorization : this.tokenStr}})
  }

  public addVersion(version: Version): Observable<Version> {

    return this.http.post<Version>(`${this.apiServerUrl}/version/add`, version,{headers:{Authorization : this.tokenStr}})
  }

  public updateVersion(version: Version): Observable<Version>{
    return this.http.put<Version>(`${this.apiServerUrl}/version/update`, version,{headers:{Authorization : this.tokenStr}});
  }

  public deleteVersion(versionId: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/version/delete/${versionId}`,{headers:{Authorization : this.tokenStr}});
  }
}
