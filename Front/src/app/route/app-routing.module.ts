import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../component/home/home.component";
import { GestionnairePlansComponent } from "../component/gestionnairePlans/gestionnaire-plans.component";
import { DashboardComponent } from "../component/dashboard/dashboard/dashboard.component";
import { UtilisateurResolver } from "../resolver/utilisateur.resolver";
import { TableauProjetsComponent } from "../component/tableau-projets/tableau-projets.component";
import { ProjetResolver } from "../resolver/projet.resolver";
import { TableauPlansComponent } from "../component/tableau-plans/tableau-plans.component";
import { GestionnaireGroupeFonctionnalitesComponent } from "../component/gestionnaireGroupesFonctionnaltites/gestionnaire-groupe-fonctionnalites.component";
import { GroupeResolver } from "../resolver/groupe.resolver";
import {
  GestionnaireFonctionnalitesComponent
} from "../component/gestionnaireFonctionnalites/gestionnaire-fonctionnalites.component";
import {FonctionnalitesResolver} from "../resolver/fonctionnalites.resolver";
import {ListeFonctionnalitesComponent} from "../component/listeFonctionnalites/liste-fonctionnalites.component";
import {TypesErreursResolver} from "../resolver/types-erreurs.resolver";
import {GestionnaireRolesComponent} from "../component/gestionnaireRoles/gestionnaire-roles.component";
import {
  GestionnaireTypesErreursComponent
} from "../component/gestionnaireTypesErreurs/gestionnaire-types-erreurs.component";
import {GestionnaireBugs} from "../component/gestionnaireBugs/gestionnaire-bugs.component";
import {BugsResolver} from "../resolver/bugs.resolver";

const routes: Routes=[
  { path: '', component: HomeComponent },
  { path: 'gestionnaire-de-plans/:id',
    component: GestionnairePlansComponent,
    resolve:{ projet: ProjetResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'app-gestionnaire-roles', component: GestionnaireRolesComponent },
  { path: 'gestionnaire-de-groupes-de-fonctionnalites/:id',
    component: GestionnaireGroupeFonctionnalitesComponent,
    resolve:{ plan: GroupeResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'gestionnaire-de-fonctionnalites/:id',
    component: GestionnaireFonctionnalitesComponent,
    resolve:{ groupe: FonctionnalitesResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'gestionnaire-de-types-d-erreurs',
    component: GestionnaireTypesErreursComponent,
    resolve:{ types: TypesErreursResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'gestionnaire-de-bugs/:id',
    component: GestionnaireBugs,
    resolve:{ bugs: BugsResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'dashboard/:id',
    component: DashboardComponent,
    children: [{ path: ':id', component: TableauProjetsComponent }],
    data: { title : 'Liste des Utilisateurs'},
    resolve:{ userProjets: UtilisateurResolver },
    runGuardsAndResolvers: 'always'
  },
  { path: 'dashboard-projet/:id',
    component: DashboardComponent,
    resolve:{ projet: ProjetResolver },
    runGuardsAndResolvers: 'always',
    children: [
      { path: ':id' + '',
        component: TableauPlansComponent,
        resolve: { plans: ProjetResolver },
        runGuardsAndResolvers: 'always'
      }
    ]
  },
  {
    path: 'dashboard-plan/:id',
    component: DashboardComponent,
    resolve:{ plan : GroupeResolver },
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'dashboard-groupe/:id',
    component: DashboardComponent,
    resolve:{ groupe : FonctionnalitesResolver },
    runGuardsAndResolvers: 'always',
    children: [
      { path: ':id' + '',
        component: ListeFonctionnalitesComponent,
        resolve: { groupe: FonctionnalitesResolver },
        runGuardsAndResolvers: 'always'
      }
    ]
  }
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[RouterModule],

})
export class AppRoutingModule{}
