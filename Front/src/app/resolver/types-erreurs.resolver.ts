import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import {Utilisateur} from "../model/Utilisateur";
import {SessionStorageService} from "../services/session-storage.service";
import {TypesErreursService} from "../services/types-erreurs.service";
import {TypeErreur} from "../model/typeErreur";

@Injectable({
  providedIn: 'root'
})
export class TypesErreursResolver implements Resolve<TypeErreur[]> {
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur');

  constructor(
    private sessionStorageService: SessionStorageService,
    private typeErreurService: TypesErreursService) {

  }

  // @ts-ignore
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TypeErreur[]> {
    // @ts-ignore
    if(this.authUser.role.id == 1) {
      return this.typeErreurService.getTypesErreurs();
    }
  }
}
