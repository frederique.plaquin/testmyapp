import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import {Observable} from 'rxjs';
import {ProjetService} from "../services/projet.service";
import {Projet} from "../model/projet";
import {Utilisateur} from "../model/Utilisateur";
import {SessionStorageService} from "../services/session-storage.service";

@Injectable({
  providedIn: 'root'
})
export class UtilisateurResolver implements Resolve<Projet[]> {
  public isAdmin!: boolean;
  public userRole!: number | undefined;

  public projets!: Projet[];
  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur');

  constructor(
    private projetService: ProjetService,
    private sessionStorageService: SessionStorageService) {
    this.userRole = this.authUser.role?.id;

    this.isAdmin = this.userRole == 1;
  }

  // @ts-ignore

  resolve(route: ActivatedRouteSnapshot,
          // @ts-ignore
                state: RouterStateSnapshot): Observable<Projet[]> {
    if (this.isAdmin) {
      // @ts-ignore
      return this.projetService.getProjets();
    } else {
      // const userId = route.paramMap.get('id');
      // @ts-ignore
      const userId = this.authUser.id.toString();
      // @ts-ignore
      return this.projetService.getUserProjets(userId);
    }
  }
}
