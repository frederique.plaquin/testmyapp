import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable} from 'rxjs';
import {Utilisateur} from "../model/Utilisateur";
import {SessionStorageService} from "../services/session-storage.service";
import {PlanService} from "../services/plan.service";
import {Plan} from "../model/plan";

@Injectable({
  providedIn: 'root'
})
export class GroupeResolver implements Resolve<Plan> {
  public isChefProjet!: boolean;
  public planIdFromReload!: any;

  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur');
  planId!: any;
  plan!: Plan;

  data: any = {};
  routeState: any;

  constructor(
    private planService: PlanService,
    private sessionStorageService: SessionStorageService) {
    this.isChefProjet = true;

    this.plan = this.sessionStorageService.get('plan');
    // @ts-ignore
    this.planId = +this.plan.id;
  }

  resolve(route: ActivatedRouteSnapshot,
          // @ts-ignore
          state: RouterStateSnapshot): Observable<Plan> {
    // @ts-ignore
    if(this.isChefProjet){
      return this.planService.getPlanById(this.planId);
    }
  }
}
