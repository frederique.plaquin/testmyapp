import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable} from 'rxjs';
import {Bug} from "../model/bug";
import {Utilisateur} from "../model/Utilisateur";
import {Groupe} from "../model/groupe";
import {GroupeFonctionnaliteService} from "../services/groupe-fonctionnalite.service";
import {SessionStorageService} from "../services/session-storage.service";
import {BugsService} from "../services/bugs.service";
import {Projet} from "../model/projet";
import {PlanService} from "../services/plan.service";
import {FonctionnaliteService} from "../services/fonctionnalite.service";
import {ProjetService} from "../services/projet.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Plan} from "../model/plan";
import {Fonctionnalite} from "../model/fonctionnalite";
import {TypeErreur} from "../model/typeErreur";

@Injectable({
  providedIn: 'root'
})
export class BugsResolver implements Resolve<Bug[]> {
  public isChefProjet!: boolean;

  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur');

  projet!: Projet;
  plan!: Plan;
  groupe!: Groupe;
  fonctionnalite!: Fonctionnalite;
  targetId!: number;
  bugs!: Bug[];
  typesErreurs!: TypeErreur[];



  target!: string;


  constructor(
    private bugsService: BugsService,
    private projetService: ProjetService,
    private plansService: PlanService,
    private groupesFonctionnalitesService: GroupeFonctionnaliteService,
    private fonctionnaliteService: FonctionnaliteService,
    private sessionStorageService: SessionStorageService) {

    this.target = this.sessionStorageService.get('target');
    this.targetId = +document.location.href.substring(document.location.href.lastIndexOf('/') + 1);
  }

  resolve(route: ActivatedRouteSnapshot,
          // @ts-ignore
          state: RouterStateSnapshot): Observable<Bug[]> {
    // @ts-ignore
    if(this.target == 'projet'){
      this.projetService.getProjetById(this.targetId).subscribe(
        (response: Projet) => {
          this.projet = response;
          // @ts-ignore
          const projetId = this.projet.id.toString();

          this.bugsService.getBugsByProjetId(projetId).subscribe(
            (response: Bug[]) => {
              this.bugs = response;
              return this.bugs;
            }
          );
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      )
    } if(this.target == 'testplan') {
      this.plansService.getPlanById(this.targetId).subscribe(
        (response: Plan) => {
          this.plan = response;
          return this.plan.bugs;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      )
    } if(this.target == 'groupeFonctionnalite') {
      this.groupesFonctionnalitesService.getGroupeById(this.targetId).subscribe(
        (response: Groupe) => {
          this.groupe = response;
          return this.groupe.bugs;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      )
    } if(this.target == 'fonctionnalite') {
      this.fonctionnaliteService.getFonctionnaliteById(this.targetId).subscribe(
        (response: Fonctionnalite) => {
          this.fonctionnalite = response;
          return this.groupe.bugs;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      )
    } else {
      return this.bugsService.getBugs();
    }
  }
}
