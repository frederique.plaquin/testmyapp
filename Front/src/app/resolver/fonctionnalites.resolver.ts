import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import {Utilisateur} from "../model/Utilisateur";
import {SessionStorageService} from "../services/session-storage.service";
import {Groupe} from "../model/groupe";
import {GroupeFonctionnaliteService} from "../services/groupe-fonctionnalite.service";

@Injectable({
  providedIn: 'root'
})
export class FonctionnalitesResolver implements Resolve<Groupe> {
  public isChefProjet!: boolean;
  public groupeIdFromReload!: any;

  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur')  ?? '[]';
  groupeId!: any;
  groupe!: Groupe;

  data: any = {};
  routeState: any;

  constructor(
    private groupeService: GroupeFonctionnaliteService,
    private sessionStorageService: SessionStorageService) {
    this.isChefProjet = true;

    this.groupe = this.sessionStorageService.get('groupe');
    // @ts-ignore
    this.groupeId = +this.groupe.id;

  }

  resolve(route: ActivatedRouteSnapshot,
          // @ts-ignore
          state: RouterStateSnapshot): Observable<Groupe> {
    // @ts-ignore
    if(this.isChefProjet){
      return this.groupeService.getGroupeById(this.groupeId);
    }
  }
}
