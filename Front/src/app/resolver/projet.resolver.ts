import { Injectable } from '@angular/core';
import {Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable} from 'rxjs';
import {Utilisateur} from "../model/Utilisateur";
import {ProjetService} from "../services/projet.service";
import {SessionStorageService} from "../services/session-storage.service";
import {Projet} from "../model/projet";

@Injectable({
  providedIn: 'root'
})
export class ProjetResolver implements Resolve<Projet> {
  public isChefProjet!: boolean;
  public projetIdFromReload!: any;

  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public authUser: Utilisateur = this.sessionStorageService.get('loggedInUtilisateur');
  projetId?: number;
  projet!: Projet;

  data: any = {};
  routeState: any;

  constructor(
    private projetService: ProjetService,
    private sessionStorageService: SessionStorageService) {
    this.isChefProjet = true;

    this.projet = this.sessionStorageService.get('projet');
    this.projetId = this.projet.id;
  }

  resolve(route: ActivatedRouteSnapshot,
          // @ts-ignore
          state: RouterStateSnapshot): Observable<Projet> {
    // @ts-ignore
    if(this.isChefProjet){
      return this.projetService.getProjetById(this.projetId);
    }
  }
}
