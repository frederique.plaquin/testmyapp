import {Password} from "./password";
import {Role} from "./role";
import {MembreProjet} from "./membreProjet";
export interface Utilisateur {
  id?: number;
  nom: string;
  prenom:string;
  mail:string;
  imageUrl?: string;
  confirmed?: boolean;
  role?: Role;
  password: Password;
  token:string;
  membresProjets: MembreProjet[];
}
