import {Plan} from "./plan";
import {Groupe} from "./groupe";
import {Fonctionnalite} from "./fonctionnalite";
import {Bug} from "./bug";
import {Utilisateur} from "./Utilisateur";

export interface ProjetFull {
  id: number;
  nom: string;
  details?: string;
  testPlans?: Plan[];
  groupesFonctionnalites?: Groupe[];
  fonctionnalites?: Fonctionnalite[];
  fonctionnalites_obsoletes?: Fonctionnalite[];
  bugs?: Bug[];
  membres?: Utilisateur[];
  version: string;
  lastVisit?: Date;
  archive?: boolean;
}
