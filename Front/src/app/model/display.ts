import {User} from "./user";
import {Projet} from "./projet";
import {Plan} from "./plan";
import {Injectable} from "@angular/core";

export interface Display {
  userSet: boolean,
  projetSet: boolean,
  planSet: boolean,
  composantSet: boolean,
  user: User,
  projet: Projet,
  plan: Plan,
  composant: String
}
