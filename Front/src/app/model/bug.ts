import {Fonctionnalite} from "./fonctionnalite";
import {TypeErreur} from "./typeErreur";
import {Utilisateur} from "./Utilisateur";
import {Projet} from "./projet";
import {Plan} from "./plan";
import {Groupe} from "./groupe";

export interface Bug {
  id?: number;
  archive: boolean;
  commentaire?: string;
  date: Date;
  details?: string;
  groupeFonctionnalites: Groupe;
  testPlan: Plan;
  projet_projet_id: Projet;
  status: boolean;
  version_ouvert: string;
  version_resolu?: string;
  assigne_a_utilisateur_id?: Utilisateur;
  betaTesteur?: Utilisateur;
  typeErreur?: TypeErreur;
  fonctionnalite?: Fonctionnalite;
}
