import {Projet} from "./projet";
import {Utilisateur} from "./Utilisateur";

export interface MembreProjet{
  id?: number;
  projet_projet_id: Projet;
  utilisateur_utilisateur_id: Utilisateur;
}
