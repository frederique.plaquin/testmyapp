export interface User {
  id?: number;
  nom: string;
  prenom:  string;
  imageUrl: string;
  role: string;
}
