import {Plan} from "./plan";
import {Bug} from "./bug";
import {Utilisateur} from "./Utilisateur";

export interface Projet {
  id?: number;
  nom: string;
  details?: string;
  version: string;
  lastVisit?: Date;
  membresProjets?: Array<any>;
  testPlans?: Plan[];
  archive?: boolean;
  bugs?: Bug[];
  chefProjet?: Utilisateur;
}
