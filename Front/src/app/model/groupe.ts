import {Fonctionnalite} from "./fonctionnalite";
import {Bug} from "./bug";

export interface Groupe {
  id?: number;
  nom: string;
  periodicite?: string;
  test_plan_plan_id: number;
  draft: boolean;
  archive: boolean;
  fonctionnalites?: Fonctionnalite[];
  bugs?: Bug[];
}
