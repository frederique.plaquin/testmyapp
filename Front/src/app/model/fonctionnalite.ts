import {Bug} from "./bug";

export interface Fonctionnalite {
  id?: number;
  nom: string;
  details?: string;
  resultatAttendu?: string;
  scenario?: string;
  periodicite?: number;
  priorite?: number;
  testee?: Date;
  archive: boolean;
  groupe_fonctionnalites_groupe_id: any;
  bugs?: Bug[];
}
