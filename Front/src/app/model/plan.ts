import {Groupe} from "./groupe";
import {Bug} from "./bug";

export interface Plan {
  id?: number;
  nom: string;
  details?:  string;
  projet_projet_id: number;
  archive: boolean;
  groupeFonctionnalites?: Groupe[];
  bugs?: Bug[];
}
