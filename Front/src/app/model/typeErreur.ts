export interface TypeErreur {
  id?: number;
  nom: string;
  details?: string;
  priorite?: number;
  niveau?: number;
  couleur?: string;
  archive?: boolean;
}
