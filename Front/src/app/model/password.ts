export interface Password {
  password_id?: number;
  hash: string;
  crypt?: string;
}

