import {Component, Input, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PlanService} from "../../services/plan.service";
import {Plan} from "../../model/plan";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {Projet} from "../../model/projet";
import {SessionStorageService} from "../../services/session-storage.service";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-add-plan-popup',
  templateUrl: './add-plan-popup.component.html',
  styleUrls: ['./add-plan-popup.component.scss']
})
export class AddPlanPopupComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  // popup(modal)
  closeResult!: string;
  modalOptions!: NgbModalOptions;

  // create plan form
  createPlanForm!: FormGroup;
  submitted = false;

  projet!: Projet;
  subscription!: Subscription;



  constructor(
    private modalService: NgbModal,
    private planService: PlanService,
    private formBuilder: FormBuilder,
    private sessionStorageService: SessionStorageService
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }

    this.loggedInUser = this.sessionStorageService.get('loggedInUtilisateur');
  }

  ngOnInit(): void {
    this.initForm();
    this.projet = this.sessionStorageService.get('projet');
  }

  // initialisation formulaire
  initForm(): any{
    this.createPlanForm = this.formBuilder.group({
      nom: ['', Validators.required],
      details: ['', Validators.required],
      projet_projet_id: this.projet,
      archive: false
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddPlanPopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(event: any, content: any) {
    event.preventDefault();
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createPlanForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createPlanForm.invalid) {
      return;
    }

    // set new plan

    const newPlan: Plan = {
      nom: this.createPlanForm.value.nom,
      details: this.createPlanForm.value.details,
      // @ts-ignore
      projet_projet_id: this.projet.id,
      archive: false
    }


    // send new plan to service for saving to bdd
    this.planService.addPlan(newPlan).subscribe(
      (response: Plan) => {
        alert('Le plan de test ' +  response.nom+ ' a bien été créé !')
        this.createPlanForm.reset();
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        this.createPlanForm.reset();
      }
    )
  }

  onReset() {
    this.submitted = false;
    this.createPlanForm.reset();
  }

}
