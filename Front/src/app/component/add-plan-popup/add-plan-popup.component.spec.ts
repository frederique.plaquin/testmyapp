import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlanPopupComponent } from './add-plan-popup.component';

describe('AddPlanPopupComponent', () => {
  let component: AddPlanPopupComponent;
  let fixture: ComponentFixture<AddPlanPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPlanPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlanPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
