import {Component, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PlanService} from "../../services/plan.service";
import {Plan} from "../../model/plan";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {SessionStorageService} from "../../services/session-storage.service";
import {Groupe} from "../../model/groupe";
import {GroupeFonctionnaliteService} from "../../services/groupe-fonctionnalite.service";

@Component({
  selector: 'app-add-groupe-fonctionnalite-popup',
  templateUrl: './add-groupe-fonctionnalites-popup.component.html',
  styleUrls: ['./add-groupe-fonctionnalites-popup.component.scss']
})
export class AddGroupeFonctionnalitesPopupComponent implements OnInit {
  // popup(modal)
  closeResult!: string;
  modalOptions!: NgbModalOptions;

  // create plan form
  createGroupeForm!: FormGroup;
  submitted = false;

  plan!: Plan;
  subscription!: Subscription;



  constructor(
    private modalService: NgbModal,
    private planService: PlanService,
    private formBuilder: FormBuilder,
    private sessionStorageService: SessionStorageService,
    private groupeFonctionnalitesService: GroupeFonctionnaliteService
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }

  }

  ngOnInit(): void {
    this.initForm();
    this.plan = this.sessionStorageService.get('plan');
  }

  // initialisation formulaire
  initForm(): any{
    this.createGroupeForm = this.formBuilder.group({
      nom: ['', Validators.required],
      periodicite: ['', Validators.required],
      draft:'',
      test_plan_plan_id: this.plan,
      archive: false
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddGroupeFonctionnalitesPopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createGroupeForm.controls; }

  // envoi du formulaire
  onSubmit() {
    console.log(this.createGroupeForm.value.draft+" aaaaaaaaaaaaaaaaaaa");
    this.submitted = true;

    // stop here if form is invalid
    if (this.createGroupeForm.invalid) {
      return;
    }

    // set new plan
   

    const newGroupe: Groupe = {
      nom: this.createGroupeForm.value.nom,
      periodicite: this.createGroupeForm.value.periodicite,
      // @ts-ignore
      test_plan_plan_id: this.plan.id,
      draft: this.createGroupeForm.value.draft,
      archive: this.createGroupeForm.value.archive
    }


    // send new plan to service for saving to bdd
      this.groupeFonctionnalitesService.addGroupe(newGroupe).subscribe(
      (response: Groupe) => {
        alert('Le groupe de fonctionnalités ' +  response.nom + ' a bien été créé !')
        this.createGroupeForm.reset();
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.createGroupeForm.reset();
      }
    )
  }

  onReset() {
    this.submitted = false;
    this.createGroupeForm.reset();
  }

}
