import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGroupeFonctionnalitesPopupComponent } from './add-groupe-fonctionnalites-popup.component';

describe('AddPlanPopupComponent', () => {
  let component: AddGroupeFonctionnalitesPopupComponent;
  let fixture: ComponentFixture<AddGroupeFonctionnalitesPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddGroupeFonctionnalitesPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGroupeFonctionnalitesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
