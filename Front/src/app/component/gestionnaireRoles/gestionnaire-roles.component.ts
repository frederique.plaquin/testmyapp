import { Component, OnInit } from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {HttpErrorResponse} from "@angular/common/http";
import {UtilisateurService} from "../../services/utilisateur.service";
import {NgbModalConfig, NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Role} from "../../model/role";
import {FormGroup} from "@angular/forms";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";
import {MembreProjet} from "../../model/membreProjet";
import {MembreProjetService} from "../../services/membre-projet.service";
import {parseJsonSchemaToSubCommandDescription} from "@angular/cli/utilities/json-schema";
import {conditionallyCreateMapObjectLiteral} from "@angular/compiler/src/render3/view/util";


@Component({
  selector: 'app-gestionnaire-roles',
  templateUrl: './gestionnaire-roles.component.html',
  styleUrls: ['./gestionnaire-roles.component.scss']
})

export class GestionnaireRolesComponent implements OnInit {
  public utilisateurs!: Utilisateur[];
  public editUtilisateur!: Utilisateur;
  public utilisateurProjet!: Utilisateur;
  public projets!: Projet[];
  public addmembreProjet!: MembreProjet;
  username!: string;
  showModal!: boolean;

  utilisateurId!: number;
  utilisateurNom!: string;
  utilisateurPrenom!: string;
  utilisateurMail!: string;
  utilisateurRole!: string;
  projetsUtilisateur!: Projet[];
  projet!: Projet;
  projetsList: Projet[] = [];

  role!: Role;

  closeResult = '';

  constructor(config: NgbModalConfig, private modalService: NgbModal ,private utilisateurService: UtilisateurService,
              private projetService: ProjetService, private membreProjet: MembreProjetService) { }

  ngOnInit(): void {
    this.getUtilisateurs();
    this.getProjet();
  }


  public getUtilisateurs(): void{
    this.utilisateurService.getUtilisateurs().subscribe(
      (response: Utilisateur[])=>{
        this.utilisateurs = response;
      },
      (error: HttpErrorResponse) =>{
        alert(error.message)
      }
    )
  }

  public getProjet(): void{
    this.projetService.getProjets().subscribe(
      (response: Projet[])=>{
        this.projets = response;
      },
      (error: HttpErrorResponse)=>{
        alert(error.message)
      }
    )
  }

  public onUpdateUtilisateur(modal: any, editForm: FormGroup): void {
    // @ts-ignore
    if(editForm['role'] == 1) {
      const newRole : Role={
        // @ts-ignore
        id: editForm['role'],
        role: 'admin'
      };
      this.role = newRole;
      // @ts-ignore
    } else if(editForm['role'] == 2) {
      const newRole : Role={
        // @ts-ignore
        id: editForm['role'],
        role: 'chefProjet'
      };
      this.role = newRole;
      // @ts-ignore
    } else if(editForm['role'] == 3) {
      const newRole : Role={
        // @ts-ignore
        id: editForm['role'],
        role: 'dev'
      };
      this.role = newRole;
      // @ts-ignore
    } else if(editForm['role'] == 4) {
      const newRole : Role={
        // @ts-ignore
        id: editForm['role'],
        role: 'beta'
      };
      this.role = newRole;
    }

    // @ts-ignore
    this.editUtilisateur.nom = editForm['nom'];
    // @ts-ignore
    this.editUtilisateur.prenom = editForm['prenom'];
    // @ts-ignore
    this.editUtilisateur.mail = editForm['mail'];
    // @ts-ignore
    this.editUtilisateur.imageUrl = editForm['imageUrl'];
    // @ts-ignore
    this.editUtilisateur.role = this.role;

    this.utilisateurService.updateUtilisateurs(this.editUtilisateur).subscribe(
      (response: Utilisateur) => {
        this.getUtilisateurs();
        alert(response.nom + ' a bien été mis à jour');
        this.close(modal)
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  //Modification des box d'utilisateur add , edit et delete
  public onOpenModal(utilisateur: Utilisateur, mode: string): void{
    const container = document.getElementById('main-container-gestionnaire-roles');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add'){
      button.setAttribute('data-target', '#addUtilisateurModal');
    }
    if (mode === 'edit'){
      button.setAttribute('data-target', '#updateUtilisateurModal');
    }
    if (mode === 'delete'){
      button.setAttribute('data-target', '#deleteUtilisateurModal');
    }
    container!.appendChild(button);
    button.click();
  }

  onClick(event: any, objet: any)
  {
    this.showModal = true; // Show-Hide Modal Check

    this.utilisateurId = objet.id;

    this.utilisateurNom = objet.nom;

    this.utilisateurPrenom = objet.prenom;

    this.utilisateurMail = objet.mail;

    this.utilisateurRole = objet.role;

  }

  hide(modal: string)
  {
    if(modal == "myModal") {
      this.showModal = false;
    }
  }

  // open(utilisateur:Utilisateur) {
  //  this.modalService.open(utilisateur);
  //}

  open(content: any, utilisateur: Utilisateur) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireRolesComponent.getDismissReason(reason)}`;
    });
    this.editUtilisateur=utilisateur;
  }

  close(content: any) {
    this.modalService.dismissAll(content);
  }

  openProjet(pascontent: any, utilisateur: Utilisateur){
    this.modalService.open(pascontent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) =>{
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireRolesComponent.getDismissReason(reason)}`;
    });
    this.utilisateurProjet = utilisateur;

    this.setProjetsList()
  }

  setProjet(projet: Projet){
    this.projet = projet;

  }

  getUtilisateurProjets(){
    this.projetsList = []

    // @ts-ignore
    this.projetService.getUserProjets(this.utilisateurProjet.id.toString()).subscribe(
      (response: Projet[]) => {
        this.projetsUtilisateur = response;
        this.projetsList = this.projets.filter(o1 => !this.projetsUtilisateur.some(o2 => o1.id === o2.id));
      }
    )
  }

  setProjetsList(){
    this.getUtilisateurProjets()

  }


  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addProjet(modal: any, form: FormGroup){
    const newMembre: MembreProjet = {
      // @ts-ignore
      projet_projet_id: form["projet"],
      // @ts-ignore
      utilisateur_utilisateur_id: form["utilisateur"],
    }

    this.membreProjet.addMembre(newMembre).subscribe(
      (response: MembreProjet)=>{
        this.addmembreProjet = response;
        alert(this.utilisateurProjet.nom + 'a bien été ajouté au projet ');
        this.close(modal)
      },
      (error: HttpErrorResponse)=>
        alert(error.message)
    )
  }

  setUtilisateur() {
    // @ts-ignore
    this.username = this.utilisateur['prenom'];
  }

}

//7
