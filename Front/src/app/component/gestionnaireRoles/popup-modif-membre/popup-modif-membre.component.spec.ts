import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupModifMembreComponent } from './popup-modif-membre.component';

describe('PopupModifMembreComponent', () => {
  let component: PopupModifMembreComponent;
  let fixture: ComponentFixture<PopupModifMembreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupModifMembreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupModifMembreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
