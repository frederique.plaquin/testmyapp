import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireRolesComponent } from './gestionnaire-roles.component';

describe('GestionnaireRolesComponent', () => {
  let component: GestionnaireRolesComponent;
  let fixture: ComponentFixture<GestionnaireRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireRolesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
