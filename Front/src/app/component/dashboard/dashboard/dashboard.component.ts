import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Observable} from "rxjs";
import {SessionStorageService} from "../../../services/session-storage.service";
import {User} from "../../../model/user";
import {Utilisateur} from "../../../model/Utilisateur";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  dashboard!: string;
  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  loggedInUtilisateur!: User;
  username!: string;

  utilisateur!: Utilisateur;

  data: any = {};
  routeState: any;
  projetDashboard!: boolean;
  planDashboard!: boolean;
  userDashboard!: boolean;
  groupeDashboard!: boolean;
  public login!: Observable<any>;

  constructor(
    private router: Router,
    private sessionStorageService: SessionStorageService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    // const url = window.location.protocol + "//" + window.location.host + "/#/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.dashboard = splitUrl[1];

    if( !localStorage.getItem("token")  ?? '[]'){
      this.router.navigateByUrl("");
    }
    if (this.dashboard == 'dashboard-projet'){
      this.projetDashboard = true;
    } else if (this.dashboard == 'dashboard-plan') {
      this.planDashboard = true;
    } else if (this.dashboard == 'dashboard') {
      this.userDashboard = true;
    } else if (this.dashboard == 'dashboard-groupe') {
      this.groupeDashboard = true;
    }

    this.route.data.subscribe(value=>{
      this.login = value['userProjets'];
    });

    // @ts-ignore
    this.utilisateur = this.sessionStorageService.get('loggedInUtilisateur');

    this.setUtilisateur();
  }

  setUtilisateur() {
    // @ts-ignore
    this.username = this.utilisateur.prenom;
  }

  // recuperation donnees du localStorage
  persist(key: string, value: any) {
    this.sessionStorageService.set(key, value);
  }


}
