import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";
import {BugsService} from "../../services/bugs.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tests-actifs',
  templateUrl: './tests-actifs.component.html',
  styleUrls: ['./tests-actifs.component.scss']
})
export class TestsActifsComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  projet!: Projet;
  projetId!: number;

  constructor(private projetService: ProjetService,
              private bugService: BugsService,
              private router: Router,) {
    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];

    this.projetService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;
      }
    );
  }

  ngOnInit(): void {
  }

}
