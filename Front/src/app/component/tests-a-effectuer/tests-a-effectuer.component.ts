import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ProjetService} from "../../services/projet.service";
import {Projet} from "../../model/projet";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FonctionnaliteService} from "../../services/fonctionnalite.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {FormGroup, NgForm} from "@angular/forms";
import {Bug} from "../../model/bug";
import {TypeErreur} from "../../model/typeErreur";
import {PlanService} from "../../services/plan.service";
import {Plan} from "../../model/plan";
import {BugsService} from "../../services/bugs.service";
import {ProjetFull} from "../../model/projetFull";
import {UtilisateurService} from "../../services/utilisateur.service";
import {TypesErreursService} from "../../services/types-erreurs.service";

@Component({
  selector: 'app-tests-a-effectuer',
  templateUrl: './tests-a-effectuer.component.html',
  styleUrls: ['./tests-a-effectuer.component.scss', '../../../styles/gestionnaires.scss', '../../../styles/bugs.scss', '../../../styles/tests.scss']
})
export class TestsAEffectuerComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  @Input() projet!: Projet;
  public projetId!: number;
  public projetFull!: ProjetFull;

  // modal
  closeResult!: string;
  modalOptions:NgbModalOptions;
  // fin modal
  public dateNow = Date.now();
  testPlans: Plan[] = [];
  public derniersTestsRepertoriesNonAssignes!: Fonctionnalite[];
  public derniersTestsRepertories!: Fonctionnalite[];
  testDetails!: Fonctionnalite;
  public userProjets!: ProjetFull[];
  public membresProjet: Utilisateur[] = [];


  public showForm!: boolean;
  public signaleBugForm!: NgForm;
  public typeErreurs!: TypeErreur[];
  public submitted = false;
  public fonctionnaliteTestplan!: Plan;
  public fonctionnaliteProjet!: Projet;
  public bugSignaled!: boolean;

  constructor(private router: Router,
              private utilisateursService: UtilisateurService,
              private fonctionnalitesService: FonctionnaliteService,
              private projetsService: ProjetService,
              private modalService: NgbModal,
              private testplansService: PlanService,
              private bugService: BugsService,
              private typesErreursService: TypesErreursService) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];

    this.projetsService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;
        this.projetFull = this.projetsService.getProjetDetails(response);

        console.log(this.projetFull)



        this.bugService.getBugs().subscribe(
          (response: Bug[]) => {
            this.projetFull.bugs = response.filter(bug => bug.projet_projet_id.id == this.projetFull.id);
          }
        );

        if(this.projet.membresProjets)
          for(let membre of this.projet.membresProjets){
            if(membre.utilisateur_utilisateur_id)
              this.utilisateursService.getUtilisateurById(membre.utilisateur_utilisateur_id).subscribe(
                (response: Utilisateur) => {
                  this.membresProjet.push(response);
                }
              )
          }

        this.projetFull.membres = this.membresProjet;

        if(this.projetFull.fonctionnalites)
          this.derniersTestsRepertories = this.projetFull.fonctionnalites.filter(test => !test.archive);
          this.projetFull.fonctionnalites_obsoletes = this.get_projet_fonctionnalites_obsoletes(this.projetFull);

        // @ts-ignore
        this.derniersTestsRepertoriesNonAssignes = this.derniersTestsRepertories.filter(test => !test.testee || new Date(new Date(test.testee).setMonth(new Date(test.testee).getMonth() + test.periodicite)) < new Date(Date.now()))
          // @ts-ignore
          .sort((x, y) => x?.priorite - +y?.priorite);
      }
    );

    this.typesErreursService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.typeErreurs = response;
      }
    );

  }

  get_projet_fonctionnalites_obsoletes(projet: ProjetFull) {
    let fonctionnalite_a_tester!: any;
    let fonctionnalites_obsoletes: Fonctionnalite[] = [];
    // @ts-ignore
    for(let fonctionnalite of projet.fonctionnalites) {
      fonctionnalite_a_tester = fonctionnalite;
      if(this.verifyObsolete(fonctionnalite_a_tester)){
        fonctionnalites_obsoletes.push(fonctionnalite_a_tester)
      }
    }
    return fonctionnalites_obsoletes
  }

  get_testplan_fonctionnalites_obsoletes(tesplan: Plan) {
    let tesplan_fonctionalites = this.get_tesplan_fonctionnalites(tesplan);
    let fonctionnalite_a_tester!: any;
    let fonctionnalites_obsoletes: Fonctionnalite[] = [];
    // @ts-ignore
    for(let fonctionnalite of tesplan_fonctionalites) {
      fonctionnalite_a_tester = fonctionnalite;
      if(this.verifyObsolete(fonctionnalite_a_tester)){
        fonctionnalites_obsoletes.push(fonctionnalite_a_tester)
      }
    }
    console.log('fonctionnalites_obsoletes')
    console.log(fonctionnalites_obsoletes)
    return fonctionnalites_obsoletes
  }

  get_tesplan_fonctionnalites(testplan: Plan){
    let testplan_fonctionnalites: Fonctionnalite[]= [];
    if(testplan.groupeFonctionnalites)
      for(let groupe of testplan.groupeFonctionnalites) {
        if(groupe.fonctionnalites)
          for(let fonctionnalite of groupe.fonctionnalites){
            testplan_fonctionnalites.push(fonctionnalite)
          }
      }
    console.log('testplan_fonctionnalites')
    console.log(testplan_fonctionnalites)
    return testplan_fonctionnalites
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${TestsAEffectuerComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, fonctionnalite: Fonctionnalite) {
    this.bugSignaled = false;
    this.modalService.open(content, { centered: true });
    this.testDetails = fonctionnalite;
    this.getFonctionnaliteTestplan(this.testDetails.groupe_fonctionnalites_groupe_id);
    this.isBugSignaled(this.testDetails);
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  navigateTo(type: string, objet?: any) {
    if(type == 'bug') {
      this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
    }
  }

  validateTest(event: any, test: Fonctionnalite) {
    event.preventDefault();
    test.testee = new Date(Date.now());
    this.fonctionnalitesService.updateFonctionnalite(test).subscribe(
      (response: Fonctionnalite) => {
        alert('le test # ' + response.id + ' ' + response?.nom + ' a bien été validé.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  signalerBug(signaleForm: FormGroup, test: Fonctionnalite) {
    this.submitted = true;
    // @ts-ignore
    if(signaleForm['commentaire'] && test && signaleForm['typeErreur']){
      const newBug: Bug = {
        archive: false,
        // @ts-ignore
        commentaire: signaleForm['commentaire'],
        date: new Date(Date.now()),
        details: test.details,
        groupeFonctionnalites: test.groupe_fonctionnalites_groupe_id.id,
        // @ts-ignore
        testPlan: this.fonctionnaliteTestplan,
        // @ts-ignore
        projet_projet_id: this.fonctionnaliteProjet,
        status: true,
        version_ouvert: this.fonctionnaliteProjet.version,
        // @ts-ignore
        typeErreur: signaleForm['typeErreur'],
        fonctionnalite: test
      }

      this.bugService.addBug(newBug).subscribe(
        (response: Bug) => {
          alert('le test # ' + response.id + ' ' + response?.typeErreur?.nom + ' a bien été signalé.')
          window.location.reload();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    } else {
      alert('Veuillez remplir tous les champs');
    }
  }

  getFonctionnaliteTestplan(groupeFonctionnalite: number){
    this.testplansService.getPlans().subscribe(
      // @ts-ignore
      (response: Plan[]) => {
        for(let testplan of response) {
          if(testplan.groupeFonctionnalites){
            for(let groupe of testplan.groupeFonctionnalites){
              if(groupe.id == groupeFonctionnalite ){
                this.fonctionnaliteTestplan = testplan;
                this.getFonctionnaliteProjet(this.fonctionnaliteTestplan);
              }
            }
          }
        }
      }
    )
  }

  getFonctionnaliteProjet(testplan: Plan){
    this.projetsService.getProjets().subscribe(
      // @ts-ignore
      (response: Projet[]) => {
        for(let projet of response) {
          if(projet.testPlans){
            for(let testPlan of projet.testPlans){
              if(testPlan.id == testplan.id ){
                this.fonctionnaliteProjet = projet;
              }
            }
          }
        }
      }
    )
  }

  showFormDiv() {
    this.showForm = true;
  }

  isBugSignaled(fonctionnalite: Fonctionnalite){
    this.bugSignaled = false;
    this.bugService.getBugs().subscribe(
      (response: Bug[]) => {
        for(let bug of response) {
          if(bug.fonctionnalite?.id == fonctionnalite.id) {
            this.bugSignaled = true;
          }
        }
      }
    );
  }

  obsolete(test: Fonctionnalite) {
    const periodicite = test.periodicite;
    const lastTested = test.testee;
    let obsolete = false;

    if(lastTested) {
      const date = new Date(lastTested);
      // @ts-ignore
      const dateButtoire = date.setMonth(date.getMonth()+periodicite);

      if(dateButtoire < Date.now()) {
        obsolete = true;
      }
    } else if(!lastTested ){
      obsolete = true
    }
    return obsolete;
  }

  // @ts-ignore
  getTestPlanFonctionnalites(testplan: Plan): Fonctionnalite[] {
    let derniersTestsRepertories: Fonctionnalite[] = [];
    if(testplan.groupeFonctionnalites) {
      let groupesFonctionnalites = testplan.groupeFonctionnalites;
      for (let groupe of groupesFonctionnalites) {
        if (groupe.fonctionnalites) {
          derniersTestsRepertories = groupe.fonctionnalites.filter(fonctionnalite => !fonctionnalite.archive);
        }
      }
    }
    derniersTestsRepertories = derniersTestsRepertories.filter(test => !test.testee || new Date(new Date(test.testee).setMonth(new Date(test.testee).getMonth() + 5)) < new Date(Date.now()))
      // @ts-ignore
      .sort((x, y) => x?.priorite - +y?.priorite);
    return derniersTestsRepertories;
  }

  getTestplanDerniersTests(testplan: Plan){
    let testplanFonctionnalites: Fonctionnalite[] = [];
    if(testplan.groupeFonctionnalites)
      for(let groupe of testplan.groupeFonctionnalites){
        if(groupe.fonctionnalites)
          for(let fonctionnalite of groupe.fonctionnalites) {
            testplanFonctionnalites.push(fonctionnalite);
          }
      }
    return testplanFonctionnalites
  }

  getTestplanDerniersTestsEffectues(testplan: Plan) {
    let testplanFonctionnalites = this.getTestplanDerniersTests(testplan);
    return testplanFonctionnalites.filter(fonctionnalite => fonctionnalite.testee)
  }

  getTestplanTestsInvalides(testplan: Plan) {
    let projetBugs = this.projetFull.bugs;
    let testplanFonctionnalites = this.getTestplanDerniersTests(testplan);
    let testInvalides: Fonctionnalite[] = [];

    for(let fonctionnalite of testplanFonctionnalites) {
      if(projetBugs)
      for(let bug of projetBugs) {
        if(!bug.archive) {
          if(bug.fonctionnalite?.id == fonctionnalite.id)
            testInvalides.push(fonctionnalite);
        }
      }
    }
    return testInvalides
  }

// @ts-ignore
  getDateButtoire(fonctionnalite: Fonctionnalite){
    if(fonctionnalite.testee){
      const periodicite = fonctionnalite.periodicite;
      const lastTested = fonctionnalite.testee;
      const date = new Date(lastTested);
      // @ts-ignore
      return new Date(date.setMonth(date.getMonth() + periodicite))
    }
  }

  verifyObsolete(fonctionnalite: Fonctionnalite){
    let obsolete = false;
    let now = new Date(Date.now());
    if(fonctionnalite.testee){
      let dateButtoire = this.getDateButtoire(fonctionnalite);
      // @ts-ignore
      if(dateButtoire < now){
        obsolete = true;
      }
    } else {
      if(this.projetFull.bugs?.length != 0) {
        let fonctionnaliteBugs: Bug[] = [];

        // @ts-ignore
        for(let bug of this.projetFull.bugs) {
          if(bug.id){
            if(bug.fonctionnalite?.id == fonctionnalite.id) {
              fonctionnaliteBugs.push(bug);
            }
          }

          if(fonctionnaliteBugs.length != 0){
            for(let bug of fonctionnaliteBugs){
              obsolete = bug.archive;
            }
          } else {
            obsolete = true;
          }
        }
      } else {
        if(fonctionnalite.testee == null) {
          obsolete = true
        }
      }
    }
    return obsolete;
  }

  // @ts-ignore
  getGroupeFonctionnalite(groupeId: number) {
    if(this.projetFull.groupesFonctionnalites)
      for(let groupe of this.projetFull.groupesFonctionnalites){
        if(groupe.id == groupeId)
          return groupe;
      }
  }
}
