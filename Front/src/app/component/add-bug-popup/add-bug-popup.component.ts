import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {SessionStorageService} from "../../services/session-storage.service";
import {Groupe} from "../../model/groupe";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {Projet} from "../../model/projet";
import {BugsService} from "../../services/bugs.service";
import {Bug} from "../../model/bug";
import {TypeErreur} from "../../model/typeErreur";
import {Plan} from "../../model/plan";
import {Utilisateur} from "../../model/Utilisateur";
import {UtilisateurService} from "../../services/utilisateur.service";
import {MembreProjet} from "../../model/membreProjet";

@Component({
  selector: 'app-add-bug-popup',
  templateUrl: './add-bug-popup.component.html',
  styleUrls: ['./add-bug-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddBugPopupComponent implements OnInit, OnChanges {
  @Input() loggedInUser!: Utilisateur;

  // popup(modal)
  closeResult!: string;
  modalOptions!: NgbModalOptions;

  // create plan form
  createBugForm!: FormGroup;
  submitted = false;

  projet!: Projet;
  subscription!: Subscription;

  @Input()
  typesErreurs!: TypeErreur[]; // decorate the property with @Input()

  testPlan!: Plan;
  @Input()
  testPlans!: Plan[] | undefined;

  groupeFonctionnalite!: Groupe;
  groupesFonctionnalites!: Groupe[];

  fonctionnalite!: Fonctionnalite;
  fonctionnalites!: Fonctionnalite[];

  membreProjets!: MembreProjet[];
  developpeurs!: Utilisateur[];

  selectedTestPlan!: Plan;

  showAssigner: boolean = false;
  assigneA!: Utilisateur;

  constructor(
    private modalService: NgbModal,
    private bugsService: BugsService,
    private formBuilder: FormBuilder,
    private sessionStorageService: SessionStorageService,
    private utilisateursService: UtilisateurService
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }
    this.projet = this.sessionStorageService.get('projet');

    if(this.projet.membresProjets) this.membreProjets = this.projet.membresProjets;

    for(let membre in this.projet.membresProjets){
      this.utilisateursService.getUtilisateurs().subscribe(
        {
          next:utilisateurs => {
            for(let utilisateur of utilisateurs) {
              // @ts-ignore
              if(membre['utilisateur_utilisateur_id'] == utilisateur.id) {
                // @ts-ignore
                this.membreProjets.push(utilisateur);
              }
            }
          }
        }
      );
    }

    for(let membre in this.membreProjets) {
      this.utilisateursService.getUtilisateurs().subscribe(
        {
          next:utilisateurs => {
            // @ts-ignore
            this.developpeurs = utilisateurs.filter(utilisateur => utilisateur.role?.id == 3 || utilisateur.role?.id == 2);
          }
        }
      );
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.createBugForm = this.formBuilder.group({
      commentaire: ['', Validators.required],
      details: ['', Validators.required],
      groupeFonctionnalites: ['', Validators.required],
      testPlan: ['', Validators.required],
      typeErreur: ['', Validators.required],
      fonctionnalite: ['', Validators.required]
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddBugPopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createBugForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createBugForm.invalid) {
      return;
    }

    const newBug: Bug = {
      archive: false,
      commentaire: this.createBugForm.value.commentaire,
      date: new Date(Date.now()),
      details: this.createBugForm.value.details,
      groupeFonctionnalites: this.createBugForm.value.groupeFonctionnalites,
      testPlan: this.createBugForm.value.testPlan,
      projet_projet_id: this.projet,
      status: true,
      version_ouvert: this.projet.version,
      typeErreur: this.createBugForm.value.typeErreur,
      fonctionnalite: this.createBugForm.value.fonctionnalite
    }

    // send new plan to service for saving to bdd
    this.bugsService.addBug(newBug).subscribe(
      (response: Bug) => {
        alert('Le bug ' + response.typeErreur?.nom + ' #' + response.id + ' a bien été crée !')
        this.createBugForm.reset();
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message)
        this.createBugForm.reset();
      }
    )
  }

  onReset() {
    this.submitted = false;
    this.createBugForm.reset();
  }

  // @ts-ignore
  setItem(type: string, objet?: any){
    if(type == 'testplan') {
        this.testPlan = objet;
        if(this.testPlan.groupeFonctionnalites) {
          this.groupesFonctionnalites= [];
          this.groupesFonctionnalites= this.testPlan.groupeFonctionnalites;
        }
    } else if(type == 'groupeFonctionnalite'){
      this.groupeFonctionnalite = objet;
      if(this.groupeFonctionnalite.fonctionnalites) {
        this.fonctionnalites = [];
        this.fonctionnalites = this.groupeFonctionnalite.fonctionnalites;
      }
    } else if(type == "removeTestPlan") {
      this.groupesFonctionnalites= [];
    } else if(type == "developpeur") {
      this.assigneA = objet;
      return true;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    // for (let key in changes) {
    //  console.log('(((((((((((((((((((((((((((console.log(`${key} changed.')
    //  console.log(`${key} changed.
    //  Current: ${changes[key].currentValue}.
    //  Previous: ${changes[key].previousValue}`);
    // }
  }
}
