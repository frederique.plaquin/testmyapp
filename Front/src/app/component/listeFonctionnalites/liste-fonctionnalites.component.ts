// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {DisplayService} from "../../services/display.service";
import {Groupe} from "../../model/groupe";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {FonctionnaliteService} from "../../services/fonctionnalite.service";
import {GroupeFonctionnaliteService} from "../../services/groupe-fonctionnalite.service";
import {Utilisateur} from "../../model/Utilisateur";
import {TypeErreur} from "../../model/typeErreur";
import {TypesErreursService} from "../../services/types-erreurs.service";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Projet} from "../../model/projet";
import {Plan} from "../../model/plan";
import {ProjetService} from "../../services/projet.service";
import {PlanService} from "../../services/plan.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-liste-fonctionnalites',
  templateUrl: './liste-fonctionnalites.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss', '../../../styles/liste.scss']
})
export class ListeFonctionnalitesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  localStorageChanges$ = this.sessionStorageService.changes$;
  public groupeId!: number;
  public groupe!: Groupe;
  public fonctionnalites!: Fonctionnalite[] | undefined;
  public fonctionnalite!: Fonctionnalite;
  public editFonctionnalite!: Fonctionnalite;
  public deleteFonctionnalite!: Fonctionnalite;


  // modals
  showDeleteModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  submitted = false;

  // modal test
  testDetails!: Fonctionnalite;
  public bugSignaled!: boolean;
  public showForm!: boolean;
  public typeErreurs!: TypeErreur[];
  public fonctionnaliteTestplan!: Plan;
  public fonctionnaliteProjet!: Projet;
  public fonctionnaliteBugs!: Bug[];

  constructor(
    private fonctionnaliteService: FonctionnaliteService,
    private groupeFonctionnalitesService: GroupeFonctionnaliteService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router,
    private fonctionnalitesService: FonctionnaliteService,
    private typesErreursService: TypesErreursService,
    private bugService: BugsService,
    private projetsService: ProjetService,
    private testplansService: PlanService
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    // this.route.data.subscribe(value=>{
    //  this.groupe = value['groupe'];
    //  // @ts-ignore
    //   this.fonctionnalites = this.groupe.fonctionnalites;
    // });
    this.groupeId = this.route.snapshot.params['id'];

    this.groupeFonctionnalitesService.getGroupeById(this.groupeId).subscribe(
      (response: Groupe) => {
        this.groupe = response;
        this.fonctionnalites = this.groupe.fonctionnalites;

        // @ts-ignore
        for(let fonctionnalite of this.fonctionnalites){
          // @ts-ignore
          this.bugService.getBugsByFonctionnaliteId(fonctionnalite.id.toString()).subscribe(
            // @ts-ignore
            (response: Bug[]) => {
              fonctionnalite.bugs = response.filter(bug => !bug.archive);
            }
          );
        }
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    )

    this.typesErreursService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.typeErreurs = response;
      }
    );

  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${ListeFonctionnalitesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, fonctionnalite: Fonctionnalite) {
    this.bugSignaled = false;
    this.testDetails = fonctionnalite;
    this.getFonctionnaliteTestplan(this.groupeId)
    this.getFonctionnaliteProjet(this.fonctionnaliteTestplan)
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(fonctionnalite: Fonctionnalite) {
    this.submitted = true;
    this.fonctionnaliteService.updateFonctionnalite(fonctionnalite).subscribe(
      (response: Fonctionnalite) => {
        alert('La fonctionnalité ' +  response.nom+ ' a bien été mise à jour !');
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchiveFonctionnalite(fonctionnalite: Fonctionnalite) {
    this.submitted = true;
    // @ts-ignore
    if(fonctionnalite.archive) {
      fonctionnalite.archive = false;
    } else if(!fonctionnalite.archive) {
      fonctionnalite.archive = true;
    }
    this.fonctionnaliteService.updateFonctionnalite(fonctionnalite).subscribe(
      (response) => {
        alert('La fonctionnalité ' +  response.nom + ' a bien été archivée !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, fonctionnalite: any)
  {
    this.editFonctionnalite = fonctionnalite;
    this.showModal = true; // Show-Hide Modal Check
  }

  onDelete(event: any, fonctionnalite: Fonctionnalite) {
    this.deleteFonctionnalite = fonctionnalite;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    this.showModal = false;
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    }
  }

  onRedirect() {
    this.router.navigate([`gestionnaire-de-fonctionnalites/${this.groupeId}`])
  }

  // navigateTo(plan: Plan) {
  //   if(plan.id) {
  //     // preparing data before navigate
  //     // display manager set data
  //     this.displayService.setData('plan', plan);
  //     this.displayService.setData('composant', 'dashboard-plan');
  //     // display manager set url with data previously setted in service
  //     const url = this.displayService.displayManager();
  //     console.log('url');
  //     console.log(url);
  //    this.router.navigate([`${url}`]
  //    ).then();
  //   }
  // }

  // Validation test
  validateTest(event: any, test: Fonctionnalite) {
    event.preventDefault();
    test.testee = new Date(Date.now());
    this.fonctionnalitesService.updateFonctionnalite(test).subscribe(
      (response: Fonctionnalite) => {
        alert('le test # ' + response.id + ' ' + response?.nom + ' a bien été validé.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  obsolete(test: Fonctionnalite) {
    const periodicite = test.periodicite;
    const lastTested = test.testee;
    let obsolete = false;

    if(lastTested) {
      const date = new Date(lastTested);
      // @ts-ignore
      const dateButtoire = date.setMonth(date.getMonth()+periodicite);

      if(dateButtoire < Date.now()) {
        obsolete = true;
      }
    } else if(!lastTested ){
      obsolete = true
    }
    return obsolete;
  }

  showFormDiv() {
    this.showForm = true;
  }

  signalerBug(signaleForm: FormGroup, test: Fonctionnalite) {
    this.submitted = true;
    // @ts-ignore
    if(signaleForm['commentaire'] && test && signaleForm['typeErreur']){
      const newBug: Bug = {
        archive: false,
        // @ts-ignore
        commentaire: signaleForm['commentaire'],
        date: new Date(Date.now()),
        details: test.details,
        groupeFonctionnalites: this.groupe,
        // @ts-ignore
        testPlan: this.fonctionnaliteTestplan,
        // @ts-ignore
        projet_projet_id: this.fonctionnaliteProjet,
        status: true,
        version_ouvert: this.fonctionnaliteProjet.version,
        // @ts-ignore
        typeErreur: signaleForm['typeErreur'],
        fonctionnalite: test
      }

      this.bugService.addBug(newBug).subscribe(
        (response: Bug) => {
          alert('le test # ' + response.id + ' ' + response?.typeErreur?.nom + ' a bien été signalé.')
          window.location.reload();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    } else {
      alert('Veuillez remplir tous les champs');
    }
  }

  resetPopup(){
    this.signalerBug
  }

  getFonctionnaliteTestplan(groupeFonctionnalite: number){
    this.testplansService.getPlans().subscribe(
      // @ts-ignore
      (response: Plan[]) => {
        for(let testplan of response) {
          if(testplan.groupeFonctionnalites){
            for(let groupe of testplan.groupeFonctionnalites){
              if(groupe.id == groupeFonctionnalite ){
                this.fonctionnaliteTestplan = testplan;
                this.getFonctionnaliteProjet(this.fonctionnaliteTestplan);
              }
            }
          }
        }
      }
    )
  }

  getFonctionnaliteProjet(testplan: Plan){
    this.projetsService.getProjets().subscribe(
      // @ts-ignore
      (response: Projet[]) => {
        for(let projet of response) {
          if(projet.testPlans){
            for(let testPlan of projet.testPlans){
              if(testPlan.id == testplan.id ){
                this.fonctionnaliteProjet = projet;
              }
            }
          }
        }
      }
    )
  }

  isBugSignaled(fonctionnalite: Fonctionnalite){
    let fonctionnaliteBugs = fonctionnalite.bugs
    fonctionnaliteBugs?.filter(bug => !bug.archive)
    // @ts-ignore
    if(fonctionnaliteBugs.length != 0)
    return true
    else return false
  }

}
