import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeFonctionnalitesComponent } from './liste-fonctionnalites.component';

describe('GestionnairePlansComponent', () => {
  let component: ListeFonctionnalitesComponent;
  let fixture: ComponentFixture<ListeFonctionnalitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeFonctionnalitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeFonctionnalitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
