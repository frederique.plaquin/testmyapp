import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ProjetService} from "../../services/projet.service";
import {Projet} from "../../model/projet";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-derniers-bugs-repertories',
  templateUrl: './derniers-bugs-repertories.component.html',
  styleUrls: ['./derniers-bugs-repertories.component.scss', '../../../styles/gestionnaires.scss', '../../../styles/bugs.scss',  ]
})
export class DerniersBugsRepertoriesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  // modal
  closeResult!: string;
  modalOptions:NgbModalOptions;
  // fin modal
  public derniersBugsRepertoriesNonAssignes: Bug[] = [];
  public derniersBugsRepertories!: Bug[];
  bugDetails!: Bug;

  constructor(private router: Router,
              private bugsService: BugsService,
              private projetsService: ProjetService,
              private modalService: NgbModal) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    // @ts-ignore
    const strigifiedUserId = this.loggedInUser.id.toString();
    this.projetsService.getUserProjets(strigifiedUserId).subscribe(
      (response: Projet[]) => {
        let derniersBugsRepertories: Bug[] = [];
        for(let projet of response) {
          // @ts-ignore
          const stringyfiedProjetId = projet.id.toString();
          this.bugsService.getBugsByProjetId(stringyfiedProjetId).subscribe(
            (response: Bug[]) => {
              for(let bug of response) {
                derniersBugsRepertories.push(bug);
              }
              this.derniersBugsRepertories = derniersBugsRepertories;
              this.derniersBugsRepertoriesNonAssignes = derniersBugsRepertories.filter(bug => !bug?.assigne_a_utilisateur_id).sort((x, y) => +new Date(x.date) + +new Date(y.date));
              },
            (error: HttpErrorResponse) => {
              alert(error.message);
            }
          );
        }
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${DerniersBugsRepertoriesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, bug: Bug) {
    this.modalService.open(content, { centered: true });
    this.bugDetails = bug;
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  navigateTo(type: string, objet?: any) {
    if(type == 'bug') {
      this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
    }
  }

  assignToMe(bug: Bug) {
    bug.assigne_a_utilisateur_id = this.loggedInUser;
    this.bugsService.updateBug(bug).subscribe(
      (response: Bug) => {
        alert('le bug # ' + response.id + ' ' + response.typeErreur?.nom + ' vous a bien été assigné.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
}
