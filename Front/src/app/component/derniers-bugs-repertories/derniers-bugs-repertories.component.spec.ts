import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DerniersBugsRepertoriesComponent } from './derniers-bugs-repertories.component';

describe('DerniersBugsRepertoriesComponent', () => {
  let component: DerniersBugsRepertoriesComponent;
  let fixture: ComponentFixture<DerniersBugsRepertoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DerniersBugsRepertoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DerniersBugsRepertoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
