import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjetService} from "../../services/projet.service";
import {Projet} from "../../model/projet";
import {HttpErrorResponse} from "@angular/common/http";
import {Version} from "../../model/version";
import {VersionService} from "../../services/version.service";

@Component({
  selector: 'app-add-projet-popup',
  templateUrl: './add-projet-popup.component.html',
  styleUrls: ['./add-projet-popup.component.scss']
})
export class AddProjetPopupComponent implements OnInit {
  // popup(modal)
  closeResult!: string;
  modalOptions:NgbModalOptions;

  // create plan form
  createProjetForm!: FormGroup;
  submitted = false;

  constructor(
    private modalService: NgbModal,
    private projetService: ProjetService,
    private formBuilder: FormBuilder,
    private versionsService: VersionService
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.createProjetForm = this.formBuilder.group({
      nom: ['', Validators.required],
      details: ['', Validators.required],
      version: ['', [Validators.required]],
      archive: false
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddProjetPopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createProjetForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createProjetForm.invalid) {
      return;
    }
    // set new projet
    const newProjet: Projet = {
      nom: this.createProjetForm.value.nom,
      details: this.createProjetForm.value.details,
      version: this.createProjetForm.value.version,
      archive: false
    }

    // send new projet to service for saving to bdd
    this.projetService.addProjet(newProjet).subscribe({
   next:   (response: Projet) => {
        alert('Le projet ' + newProjet.nom + ' a bien été enregistré !')
        this.createProjetForm.reset();

         const newVersion: Version = {
           // @ts-ignore
           projet_projet_id: response.id,
           version: response.version
         }

         this.versionsService.addVersion(newVersion).subscribe(
           (response: Version) => {
             alert('La version du projet '  +  newProjet.nom + ' est désormais ' + response.version);

             window.location.reload();
           },
           (error: HttpErrorResponse) => {
             console.log(error.message);
           }
         );
      },
     error: (error: HttpErrorResponse) => {
       console.log(error.message);
      //  this.createProjetForm.reset();
      }
    })
  }

  onReset() {
    this.submitted = false;
    this.createProjetForm.reset();
  }

}
