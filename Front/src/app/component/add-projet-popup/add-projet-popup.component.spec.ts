import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProjetPopupComponent } from './add-projet-popup.component';

describe('AddProjetPopupComponent', () => {
  let component: AddProjetPopupComponent;
  let fixture: ComponentFixture<AddProjetPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProjetPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProjetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
