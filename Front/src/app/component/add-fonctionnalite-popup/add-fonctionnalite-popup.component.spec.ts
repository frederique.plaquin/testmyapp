import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFonctionnalitePopupComponent } from './add-fonctionnalite-popup.component';

describe('AddPlanPopupComponent', () => {
  let component: AddFonctionnalitePopupComponent;
  let fixture: ComponentFixture<AddFonctionnalitePopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddFonctionnalitePopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFonctionnalitePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
