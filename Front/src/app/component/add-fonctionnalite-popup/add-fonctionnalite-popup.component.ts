import {Component, Input, OnInit} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {SessionStorageService} from "../../services/session-storage.service";
import {Groupe} from "../../model/groupe";
import {FonctionnaliteService} from "../../services/fonctionnalite.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-add-fonctionnalite-popup',
  templateUrl: './add-fonctionnalite-popup.component.html',
  styleUrls: ['./add-fonctionnalite-popup.component.scss']
})

export class AddFonctionnalitePopupComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  // popup(modal)
  closeResult!: string;
  modalOptions!: NgbModalOptions;

  // create plan form
  createFonctionnaliteForm!: FormGroup;
  submitted = false;

  groupe!: Groupe;
  subscription!: Subscription;

  constructor(
    private modalService: NgbModal,
    private fonctionnaliteService: FonctionnaliteService,
    private formBuilder: FormBuilder,
    private sessionStorageService: SessionStorageService
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }
    this.groupe = this.sessionStorageService.get('groupe');

  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.createFonctionnaliteForm = this.formBuilder.group({
      nom: ['', Validators.required],
      periodicite: [''],
      priorite: [''],
      details: ['', Validators.required],
      groupe_fonctionnalites_groupe_id: this.groupe?.id,
      testee: false,
      resultatAttendu: [''],
      scenario: [''],
      archive: false
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddFonctionnalitePopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createFonctionnaliteForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createFonctionnaliteForm.invalid) {
      return;
    }

    const newFonctionnalite: Fonctionnalite = {
      nom: this.createFonctionnaliteForm.value.nom,
      periodicite: this.createFonctionnaliteForm.value.periodicite,
      priorite: this.createFonctionnaliteForm.value.priorite,
      // testee: new Date(Date.now()),
      details: this.createFonctionnaliteForm.value.details,
      archive: false,
      groupe_fonctionnalites_groupe_id: this.groupe.id,
      resultatAttendu: this.createFonctionnaliteForm.value.resultatAttendu,
      scenario: this.createFonctionnaliteForm.value.scenario
    }
    // send new plan to service for saving to bdd
    this.fonctionnaliteService.addFonctionnalite(newFonctionnalite).subscribe(
      (response: Fonctionnalite) => {
        alert('La fonctionnalité ' +  response.nom+ ' a bien été créée !')
        this.createFonctionnaliteForm.reset();
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.createFonctionnaliteForm.reset();
      }
    )
  }

  onReset() {
    this.submitted = false;
    this.createFonctionnaliteForm.reset();
  }

}
