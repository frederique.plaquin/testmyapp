// source router queryparams: https://angular.io/api/router/UrlCreationOptions#queryParams
// source editable fields: https://netbasal.com/keeping-it-simple-implementing-edit-in-place-in-angular-4fd92c4dfc70
// source editable fields: https://stackblitz.com/edit/angular-inline-edit-a494su?file=src%2Fapp%2Fedit-input%2Fedit-input.component.html
import {Component, Input, OnInit} from '@angular/core';
import {Plan} from "../../model/plan";
import {PlanService} from "../../services/plan.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {DataService} from "../../services/data.service";
import {SessionStorageService} from "../../services/session-storage.service";
import {Groupe} from "../../model/groupe";
import {DisplayService} from "../../services/display.service";
import { FormControl, FormGroup } from '@angular/forms';
import {HttpErrorResponse} from "@angular/common/http";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-tableau-groupes-fonctionnalites',
  templateUrl: './tableau-groupes-fonctionnalites.component.html',
  styleUrls: ['./tableau-groupes-fonctionnalites.component.scss']
})
export class TableauGroupesFonctionnalitesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  navigationSubscription: any;
  public alreadyVisitedProjet!: boolean;
  localStorageChanges$ = this.sessionStorageService.changes$;
  showModal!: boolean;
  reloaded!: boolean;

  plan!: any;
  planId!: number;
  subscription!: Subscription;
  planName!: string;

  public groupes!: Groupe[];
  groupeId!: number;
  groupeNom!: string;
  groupePeriodicite!: string;
  groupePlan!: Plan;
  draft!: boolean;
  archive!: boolean;

  form=new FormGroup({
    choix: new FormControl
  })

  constructor(
    private router: Router,
    private planService: PlanService,
    private data: DataService,
    private sessionStorageService: SessionStorageService,
    private displayService: DisplayService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    // this.route.data.subscribe(value => {
    //   this.plan = value['plan'];
    //   this.groupes = this.plan.groupeFonctionnalites;
    // });

    this.groupeId = this.route.snapshot.params['id'];

    this.planService.getPlanById(this.planId).subscribe(
      (response: Plan) => {
        this.plan = response;
        this.groupes = this.plan.groupeFonctionnalites;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    )

    this.plan = this.sessionStorageService.get('plan');
    this.planName = this.plan.nom;

    // @ts-ignore
    this.planService.getPlanById(this.plan.id).subscribe(plan => this.groupes = plan['groupeFonctionnalites']);
    // this.plans = this.projet.testPlans;

    this.reloaded = Boolean(sessionStorage.getItem('reloaded'));
  }

  onClick(event: any, objet: any)
  {
    this.showModal = true; // Show-Hide Modal Check
    this.groupeId = objet.id;
    this.groupeNom = objet.nom;
    this.groupePeriodicite = objet.periodicite;
    this.groupePlan = objet.groupePlan;
    this.draft = objet.draft;
    this.archive = false;

  }

  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }

  // source: https://edupala.com/how-to-pass-data-in-angular-routed-component/
  redirect(to: string, objet?: any) {
    // display manager set data
    this.displayService.setData('groupe', objet);
    this.displayService.setData('composant', 'dashboard-groupe');
    // display manager set url with data previously setted in service
    const url = this.displayService.displayManager();
    if(objet) {
      this.router.navigate([`/${url}/`], {
        state: {
          navigateTo: to
        }
      }).then();
    } else if(to == 'gestionnaire-de-groupes-de-fonctionnalites') {
      // @ts-ignore
      this.router.navigate([`/${to}/${this.plan['id']}`], {
        state: {
          navigateTo: to,
          // @ts-ignore
          projetId: this.plan['id'],
        }
      }).then();
    } else {
      this.router.navigate([`/${to}/`], {
        state: {
          navigateTo: to
        }
      }).then();
    }
  }
//----------------------------------------------------------------------------------------------
  search(key:string){
    const results:Groupe[]=[];
    for(let groupe of this.groupes){
      if(groupe.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase() )!== -1){
        results.push(groupe)
      }
    }
    this.groupes=results;

    if(results.length==0 || !key){
      this.route.data.subscribe(value => {
        this.plan = value['plan'];
        this.groupes = this.plan.groupeFonctionnalites;
      });
    }
  }
//---------------------------------------------------------------------------------------------------
  submit(){
    this.route.data.subscribe(value => {
      this.plan = value['plan'];
      this.groupes = this.plan.groupeFonctionnalites;
    });
    if((this.form.value.periodicite==null || this.form.value.periodicite==0 ) && (this.form.value.choix==null || this.form.value.choix=="aucun" )  ){

      this.route.data.subscribe(value => {
        this.plan = value['plan'];
        this.groupes = this.plan.groupeFonctionnalites;
      });

    }
//-------------------------------------------------------------------------------------------------------------
    else if ((this.form.value.periodicite==null || this.form.value.periodicite==0 ) && this.form.value.choix!=null ){
      const results:Groupe[]=[];
    if(this.form.value.choix=="brouillon"){
      for(let groupe of this.groupes){
        if(groupe.draft){
          results.push(groupe);
        }
      }
      this.groupes=results;
    }

    else if(this.form.value.choix=="actif"){
      for(let groupe of this.groupes){
        if(!groupe.draft && !groupe.archive){
          results.push(groupe);
        }
      }
      this.groupes=results;

    }else if(this.form.value.choix=="inactif"){
      for(let groupe of this.groupes){
        if(!groupe.draft && groupe.archive){
          results.push(groupe);
        }
      }
      this.groupes=results;
    }
    else{
      this.route.data.subscribe(value => {
        this.plan = value['plan'];
        this.groupes = this.plan.groupeFonctionnalites;
      });
    }

  }
    else if ((this.form.value.periodicite!=null && this.form.value.periodicite!=0 ) && (this.form.value.choix==null || this.form.value.choix=="aucun" ) ){
      const results:Groupe[]=[];
      console.log("here");

      for(let groupe of this.groupes){
        console.log( groupe.periodicite?.charAt(0));
        if(Number(groupe.periodicite?.charAt(0))==this.form.value.periodicite){
          results.push(groupe);
          this.groupes=results;
        }

      }

}
    else{
      const results:Groupe[]=[];
      if(this.form.value.choix=="brouillon"){
        for(let groupe of this.groupes){
          if(groupe.draft===true && (Number(groupe.periodicite?.charAt(0))==this.form.value.periodicite) ){
            results.push(groupe);
          }
        }
        this.groupes=results;
      }

      else if(this.form.value.choix=="actif"){
        console.log("this is actif")
        for(let groupe of this.groupes){
          if(groupe.draft===false && groupe.archive===false && (Number(groupe.periodicite?.charAt(0))==this.form.value.periodicite)){
            results.push(groupe);
          }
        }
        this.groupes=results;

      }
//   else if(this.form.value.choix=="inactif"){
//     const results:Groupe[]=[];
//     console.log("this is NOt actif")
//     for(let groupe of this.groupes){
//       if(groupe.draft===false && groupe.archive===true && (Number(groupe.periodicite?.charAt(0))==this.form.value.periodicite)){
//         results.push(groupe);
//       }
//   }
//   this.groupes=results;
// }
      else{
        this.route.data.subscribe(value => {
          this.plan = value['plan'];
          this.groupes = this.plan.groupeFonctionnalites;
        });
      }

    }

  }


}
