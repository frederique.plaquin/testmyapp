import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauGroupesFonctionnalitesComponent } from './tableau-groupes-fonctionnalites.component';

describe('TableauPlansComponent', () => {
  let component: TableauGroupesFonctionnalitesComponent;
  let fixture: ComponentFixture<TableauGroupesFonctionnalitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauGroupesFonctionnalitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauGroupesFonctionnalitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
