import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ProjetService} from "../../services/projet.service";
import {Projet} from "../../model/projet";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FonctionnaliteService} from "../../services/fonctionnalite.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {FormGroup, NgForm} from "@angular/forms";
import {Bug} from "../../model/bug";
import {TypeErreur} from "../../model/typeErreur";
import {PlanService} from "../../services/plan.service";
import {Plan} from "../../model/plan";
import {BugsService} from "../../services/bugs.service";
import {TypesErreursService} from "../../services/types-erreurs.service";
import {ProjetFull} from "../../model/projetFull";
import {UtilisateurService} from "../../services/utilisateur.service";

@Component({
  selector: 'app-derniers-tests-crees',
  templateUrl: './derniers-tests-crees.component.html',
  styleUrls: ['./derniers-tests-crees.component.scss', '../../../styles/gestionnaires.scss', '../../../styles/bugs.scss', '../../../styles/tests.scss'  ]
})
export class DerniersTestsCreesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  @Input() projet!: Projet;
  public projetId!: number;
  public projetFull!: ProjetFull;
  // modal
  closeResult!: string;
  modalOptions:NgbModalOptions;
  // fin modal
  public dateNow = new Date(Date.now());
  public derniersTestsRepertoriesNonAssignes!: Fonctionnalite[];
  public derniersTestsRepertories!: Fonctionnalite[];
  public derniersTestsRepertoriesEffectues!: Fonctionnalite[];
  testDetails!: Fonctionnalite;
  public userProjets!: ProjetFull[];
  public membresProjet: Utilisateur[] = [];

  public showForm!: boolean;
  public signaleBugForm!: NgForm;
  public typeErreurs!: TypeErreur[];
  public submitted = false;
  public fonctionnaliteTestplan!: Plan;
  public fonctionnaliteProjet!: Projet;
  public bugSignaled!: boolean;

  constructor(private router: Router,
              private fonctionnalitesService: FonctionnaliteService,
              private projetsService: ProjetService,
              private modalService: NgbModal,
              private testplansService: PlanService,
              private bugService: BugsService,
              private typesErreursService: TypesErreursService,
              private utilisateursService: UtilisateurService) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];

    this.projetsService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;
        this.projetFull = this.projetsService.getProjetDetails(response);

        this.bugService.getBugs().subscribe(
          (response: Bug[]) => {
            this.projetFull.bugs = response.filter(bug => bug.projet_projet_id.id == this.projetFull.id);
          }
        );
        if(this.projet.membresProjets)
        for(let membre of this.projet.membresProjets){
          if(membre.utilisateur_utilisateur_id)
          this.utilisateursService.getUtilisateurById(membre.utilisateur_utilisateur_id).subscribe(
            (response: Utilisateur) => {
              this.membresProjet.push(response);
            }
          )
        }
        this.projetFull.membres = this.membresProjet;
        if(this.projetFull.fonctionnalites)
        this.derniersTestsRepertories = this.projetFull.fonctionnalites.filter(test => !test.archive);

        // @ts-ignore
        this.derniersTestsRepertoriesNonAssignes = this.derniersTestsRepertories.filter(test => !test.testee || new Date(new Date(test.testee).setMonth(new Date(test.testee).getMonth() + test.periodicite)) < new Date(Date.now()))
          // @ts-ignore
          .sort((x, y) => x?.priorite - +y?.priorite);

        // @ts-ignore
        this.derniersTestsRepertoriesEffectues = this.derniersTestsRepertories.filter(test => test.testee);
      }
    );
    this.typesErreursService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.typeErreurs = response;
      }
    );
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${DerniersTestsCreesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, fonctionnalite: Fonctionnalite) {
    this.bugSignaled = false;
    this.modalService.open(content, { centered: true });
    this.testDetails = fonctionnalite;
    this.getFonctionnaliteTestplan(this.testDetails.groupe_fonctionnalites_groupe_id);
    this.isBugSignaled(this.testDetails);
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  navigateTo(type: string, objet?: any) {
    if(type == 'bug') {
      this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
    }
  }

  validateTest(event: any, test: Fonctionnalite) {
    event.preventDefault();
    test.testee = new Date(Date.now());
    this.fonctionnalitesService.updateFonctionnalite(test).subscribe(
      (response: Fonctionnalite) => {
        alert('le test # ' + response.id + ' ' + response?.nom + ' a bien été validé.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  signalerBug(signaleForm: FormGroup, test: Fonctionnalite) {
    this.submitted = true;
    // @ts-ignore
    if(signaleForm['commentaire'] && test && signaleForm['typeErreur']){
      const newBug: Bug = {
        archive: false,
        // @ts-ignore
        commentaire: signaleForm['commentaire'],
        date: new Date(Date.now()),
        details: test.details,
        groupeFonctionnalites: test.groupe_fonctionnalites_groupe_id.id,
        // @ts-ignore
        testPlan: this.fonctionnaliteTestplan,
        // @ts-ignore
        projet_projet_id: this.fonctionnaliteProjet,
        status: true,
        version_ouvert: this.fonctionnaliteProjet.version,
        // @ts-ignore
        typeErreur: signaleForm['typeErreur'],
        fonctionnalite: test
      }

      this.bugService.addBug(newBug).subscribe(
        (response: Bug) => {
          alert('le test # ' + response.id + ' ' + response?.typeErreur?.nom + ' a bien été signalé.')
          window.location.reload();
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    } else {
      alert('Veuillez remplir tous les champs');
    }
  }

  getFonctionnaliteTestplan(groupeFonctionnalite: number){
    this.testplansService.getPlans().subscribe(
      // @ts-ignore
      (response: Plan[]) => {
        for(let testplan of response) {
          if(testplan.groupeFonctionnalites){
            for(let groupe of testplan.groupeFonctionnalites){
              if(groupe.id == groupeFonctionnalite ){
                this.fonctionnaliteTestplan = testplan;
                this.getFonctionnaliteProjet(this.fonctionnaliteTestplan);
              }
            }
          }
        }
      }
    )
  }

  getFonctionnaliteProjet(testplan: Plan){
    this.projetsService.getProjets().subscribe(
      // @ts-ignore
      (response: Projet[]) => {
        for(let projet of response) {
          if(projet.testPlans){
            for(let testPlan of projet.testPlans){
              if(testPlan.id == testplan.id ){
                this.fonctionnaliteProjet = projet;
              }
            }
          }
        }
      }
    )
  }

  showFormDiv() {
    this.showForm = true;
  }

  isBugSignaled(fonctionnalite: Fonctionnalite){
    this.bugSignaled = false;
    this.bugService.getBugs().subscribe(
      (response: Bug[]) => {
        for(let bug of response) {
          if(bug.fonctionnalite?.id == fonctionnalite.id) {
            this.bugSignaled = true;
          }
        }
      }
    );
  }

  obsolete(test: Fonctionnalite) {
    const periodicite = test.periodicite;
    const lastTested = test.testee;
    let obsolete = false;

    if(lastTested) {
      const date = new Date(lastTested);
      // @ts-ignore
      const dateButtoire = date.setMonth(date.getMonth()+periodicite);

      if(dateButtoire < Date.now()) {
        obsolete = true;
      }
    } else if(!lastTested ){
      obsolete = true
    }
    return obsolete;
  }


  // @ts-ignore
  getDateButtoire(fonctionnalite: Fonctionnalite){
    if(fonctionnalite.testee){
      const periodicite = fonctionnalite.periodicite;
      const lastTested = fonctionnalite.testee;
      const date = new Date(lastTested);
      // @ts-ignore
      return new Date(date.setMonth(date.getMonth() + periodicite))
    }
  }

  verifyObsolete(fonctionnalite: Fonctionnalite){
    let obsolete = false;
    let now = new Date(Date.now());
    if(fonctionnalite.testee){
      let dateButtoire = this.getDateButtoire(fonctionnalite);
      // @ts-ignore
      if(dateButtoire < now){
        obsolete = true;
      }
    } else {
      if(this.projetFull.bugs?.length != 0) {
        let fonctionnaliteBugs: Bug[] = [];

        // @ts-ignore
        for(let bug of this.projetFull.bugs) {
          if(bug.id){
            if(bug.fonctionnalite?.id == fonctionnalite.id) {
              fonctionnaliteBugs.push(bug);
            }
          }

          if(fonctionnaliteBugs.length != 0){
            for(let bug of fonctionnaliteBugs){
              obsolete = bug.archive;
            }
          } else {
            obsolete = true;
          }
        }
      } else {
        if(fonctionnalite.testee == null) {
          obsolete = true
        }
      }
    }
    return obsolete;
  }

  // @ts-ignore
  getGroupeFonctionnalite(groupeId: number) {
    if(this.projetFull.groupesFonctionnalites)
      for(let groupe of this.projetFull.groupesFonctionnalites){
        if(groupe.id == groupeId)
          return groupe;
      }
  }
}
