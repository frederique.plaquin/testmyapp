// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, Input, OnInit} from '@angular/core';
import { Plan } from "../../model/plan";
import { PlanService } from "../../services/plan.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {Projet} from "../../model/projet";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {DisplayService} from "../../services/display.service";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-gestionnaire-plans',
  templateUrl: './gestionnaire-plans.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss']
})
export class GestionnairePlansComponent implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;
  @Input() loggedInUser!: Utilisateur;
  public projet!: Projet;
  public plans!: Plan[];
  public plan!: Plan;
  public editPlan!: Plan;
  public deletePlan!: Plan;


  // modals
  showDeleteModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  submitted = false;

  constructor(
    private planService: PlanService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router,
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
this.getPlans();
  }

  getPlans(){
    this.route.data.subscribe(value=>{
      this.projet = value['projet'];
      // @ts-ignore
      this.plans = this.projet['testPlans'];
    });
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnairePlansComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(plan: Plan) {
    this.submitted = true;
    this.planService.updatePlan(plan).subscribe(
      (response: Plan) => {
        alert('Le plan de test ' +  response.nom+ ' a bien été mis à jour !');
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message)
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchivePlan(plan: Plan) {
    this.submitted = true;
    // @ts-ignore
    if(plan.archive) {
      plan.archive = false;
    } else if(!plan.archive) {
      plan.archive = true;
    }
    this.planService.updatePlan(plan).subscribe(
      (response) => {
        alert('Le plan de test ' +  response.nom + ' a bien été archivé !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message)
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, plan: any)
  {
    this.editPlan = plan;
    this.showModal = true; // Show-Hide Modal Check
  }

  onDelete(event: any, plan: Plan) {
    this.deletePlan = plan;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    this.showModal = false;
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    }
  }

  navigateTo(plan: Plan) {
    if(plan.id) {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('plan', plan);
      this.displayService.setData('composant', 'dashboard-plan');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then();
    }
  }
  //#############################################################################
  public receivedPlanTest(myPlans:Plan []){
    this.plans=myPlans;
}
}
