import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnairePlansComponent } from './gestionnaire-plans.component';

describe('GestionnairePlansComponent', () => {
  let component: GestionnairePlansComponent;
  let fixture: ComponentFixture<GestionnairePlansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnairePlansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnairePlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
