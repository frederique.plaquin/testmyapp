import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToFavoritesPopupComponent } from './add-to-favorites-popup.component';

describe('AddPlanPopupComponent', () => {
  let component: AddToFavoritesPopupComponent;
  let fixture: ComponentFixture<AddToFavoritesPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddToFavoritesPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToFavoritesPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
