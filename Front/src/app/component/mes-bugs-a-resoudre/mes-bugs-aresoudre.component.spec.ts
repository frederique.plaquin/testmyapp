import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesBugsAResoudreComponent } from './mes-bugs-aresoudre.component';

describe('MesBugsAResoudreComponent', () => {
  let component: MesBugsAResoudreComponent;
  let fixture: ComponentFixture<MesBugsAResoudreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesBugsAResoudreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MesBugsAResoudreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
