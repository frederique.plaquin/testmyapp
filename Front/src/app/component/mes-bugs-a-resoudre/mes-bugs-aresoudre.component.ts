import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {MembreProjet} from "../../model/membreProjet";
import {MembreProjetService} from "../../services/membre-projet.service";
import {UtilisateurService} from "../../services/utilisateur.service";

@Component({
  selector: 'app-mes-bugs-aresoudre',
  templateUrl: './mes-bugs-aresoudre.component.html',
  styleUrls: ['./mes-bugs-aresoudre.component.scss', '../../../styles/gestionnaires.scss', '../../../styles/bugs.scss']
})
export class MesBugsAResoudreComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  closeResult!: string;
  modalOptions:NgbModalOptions;
  bugDetails!: Bug;
  showAssignModal!: boolean;

  public bugToAssign!: Bug;
  public assignProjet!: Projet;
  public devsProjet!: Utilisateur[];

  public bugsAResoudre: Bug[] = [];
  public totalBugsAResoudre!: Bug[];

  constructor(private router: Router,
              private bugsService: BugsService,
              private projetsService: ProjetService,
              private modalService: NgbModal,
              private membresService: MembreProjetService,
              private utilisateurService: UtilisateurService) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.bugsService.getBugs().subscribe(
      (response: Bug[]) => {
        const bugsAResoudre= response.filter(bugs => bugs?.assigne_a_utilisateur_id?.id == this.loggedInUser.id );


        this.bugsAResoudre =  bugsAResoudre.filter(bugs => !bugs?.archive );
      }
    );


    // @ts-ignore
    const strigifiedUserId = this.loggedInUser.id.toString();
    this.projetsService.getUserProjets(strigifiedUserId).subscribe(
      (response: Projet[]) => {
        let derniersBugsRepertories: Bug[] = [];
        for(let projet of response) {
          // @ts-ignore
          const stringyfiedProjetId = projet.id.toString();
          this.bugsService.getBugsByProjetId(stringyfiedProjetId).subscribe(
            (response: Bug[]) => {
              derniersBugsRepertories= response.filter(bug=> !bug.archive);


              // @ts-ignore
              this.totalBugsAResoudre = derniersBugsRepertories.sort((x, y) => x.typeErreur?.priorite - +y.typeErreur?.priorite);
              },
            (error: HttpErrorResponse) => {
              alert(error.message);
            }
          );
        }
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${MesBugsAResoudreComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, bug: Bug) {
    this.modalService.open(content, { centered: true });
    this.bugDetails = bug;
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  navigateTo(type: string, objet?: any) {
    if(type == 'bug') {
      this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
    }
  }

  markAsResolved(event: any, bug: Bug) {
    event.preventDefault();
    bug.archive = true;
    bug.version_resolu = bug.projet_projet_id.version;
    bug.status = false;
    this.bugsService.updateBug(bug).subscribe(
      (response: Bug) => {
        alert('le bug # ' + response.id + ' ' + response.typeErreur?.nom + ' a bien été archivé.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onAssignTo(event: any, bug: Bug)
  {
    this.showAssignModal = true; // Show-Hide Modal Check
    this.bugToAssign = bug;
    let projetMembres: MembreProjet[] =  this.membresService.getDevs(this.assignProjet);
    for(let membre of projetMembres){
      if(membre.id){
        this.utilisateurService.getUtilisateurById(membre.id).subscribe(
          (response: Utilisateur) => {
            this.devsProjet.push(response);
          }
        )
      }
    }
  }
}
