import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauPlansComponent } from './tableau-plans.component';

describe('TableauPlansComponent', () => {
  let component: TableauPlansComponent;
  let fixture: ComponentFixture<TableauPlansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauPlansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
