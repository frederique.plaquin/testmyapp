// source router queryparams: https://angular.io/api/router/UrlCreationOptions#queryParams
// source editable fields: https://netbasal.com/keeping-it-simple-implementing-edit-in-place-in-angular-4fd92c4dfc70
// source editable fields: https://stackblitz.com/edit/angular-inline-edit-a494su?file=src%2Fapp%2Fedit-input%2Fedit-input.component.html
import {Component, Input, OnInit} from '@angular/core';
import {Plan} from "../../model/plan";
import {Projet} from "../../model/projet";
import {PlanService} from "../../services/plan.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {DataService} from "../../services/data.service";
import {SessionStorageService} from "../../services/session-storage.service";
import {ProjetService} from "../../services/projet.service";
import {DisplayService} from "../../services/display.service";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-tableau-plans',
  templateUrl: './tableau-plans.component.html',
  styleUrls: ['./tableau-plans.component.scss']
})
export class TableauPlansComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  navigationSubscription: any;
  public alreadyVisitedProjet!: boolean;
  localStorageChanges$ = this.sessionStorageService.changes$;
  showModal!: boolean;
  reloaded!: boolean;
  url!: string;

  projet!: Projet;
  projetId!: number;
  subscription!: Subscription;
  projetName!: string;

  public plans!: Plan[];
  planId!: number;
  planNom!: string;
  planDetails!: string;
  planProjet!: Projet;
  archive!: boolean;

  constructor(
    private router: Router,
    private planService: PlanService,
    private data: DataService,
    private sessionStorageService: SessionStorageService,
    private projetService: ProjetService,
    private displayService: DisplayService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    // this.route.data.subscribe(value => {
    //  this.projet = value['projet'];
    //  this.plans = this.projet.testPlans;
    // });

    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];

    this.projetService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;

        if(this.projet.testPlans) this.plans = this.projet.testPlans;
      }
    );
  }

  onClick(event: any, objet: any)
  {
    this.showModal = true; // Show-Hide Modal Check
    this.planId = objet.id;
    this.planNom = objet.nom;
    this.planDetails = objet.details;
    this.planProjet = objet.projet;
    this.archive = false;
  }

  //Bootstrap Modal Close event
  hide()
  {
      this.showModal = false;
  }

  // source: https://edupala.com/how-to-pass-data-in-angular-routed-component/
  redirect(to: string, objet?: any, type?: string) {
    sessionStorage.removeItem('projetId');
    if(objet) {
      if(type == 'plan') {
        this.displayService.setData('plan', objet);
        this.displayService.setData('composant', 'dashboard-plan');
        // display manager set url with data previously setted in service
        this.url = this.displayService.displayManager();
        this.router.navigate([`${this.url}`], {
          state: {
            navigateTo: to
          }
        }).then(() => {
          // window.location.reload();
        });
      } else {
        this.router.navigate([`/${to}/`, objet.id], {
          state: {
            navigateTo: to
          }
        }).then(() => {
          // window.location.reload();
        });
      }
    } else if(to == 'gestionnaire-de-plans') {
      // @ts-ignore
      this.router.navigate([`/${to}/${this.projet['id']}`], {
        state: {
          navigateTo: to,
          // @ts-ignore
          projetId: this.projet['id'],
        }
      }).then(() => {
        // window.location.reload();
      });
    } else {
      this.router.navigate([`/${to}/`], {
        state: {
          navigateTo: to
        }
      }).then(() => {
        // window.location.reload();
      });
    }
  }
//----------------------------------------------------------------------------------------------
search(key:string){
  const results:Plan[]=[];

  for(let plan of this.plans){
    if(plan.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1){
      results.push(plan);
    }
  }
  this.plans=results;
  if(results.length==0 || !key){
    this.route.data.subscribe(value => {
      this.projet = value['projet'];
      if(this.projet.testPlans) this.plans = this.projet.testPlans;
    });
  }
}
}
