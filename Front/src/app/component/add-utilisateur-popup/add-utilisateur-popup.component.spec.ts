import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUtilisateurPopupComponent } from './add-utilisateur-popup.component';

describe('AddUtilisateurPopupComponent', () => {
  let component: AddUtilisateurPopupComponent;
  let fixture: ComponentFixture<AddUtilisateurPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUtilisateurPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUtilisateurPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
