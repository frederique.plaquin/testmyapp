// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {DisplayService} from "../../services/display.service";
import {Utilisateur} from "../../model/Utilisateur";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";
import {UtilisateurService} from "../../services/utilisateur.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {Groupe} from "../../model/groupe";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Plan} from "../../model/plan";
import {MembreProjetService} from "../../services/membre-projet.service";

@Component({
  selector: 'app-liste-projets',
  templateUrl: './liste-projets.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss', '../../../styles/liste.scss', 'liste-projets.component.scss'],
})
export class ListeProjetsComponent implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;
  public userId!: string;
  public utilisateur!: Utilisateur;
  public projets!: Projet[] | undefined;
  public projetsFull!: Projet[];
  public projet!: Projet;
  public content!: any;
  public deleteMembre!: Utilisateur;
  public obsolete!: boolean;
  public projetBugs!: Bug[];
  public assignProjet!: Projet;
  public projetDevs: Utilisateur[] = [];
  public bugToAssign!: Bug;
  public projetTestsObsoletes!: Fonctionnalite[];
  public projetTestPlans!: Plan[] | undefined;
  public projetGroupesFonctionnalites!: Groupe[];
  public projetFonctionnalites!: Fonctionnalite[];
  public projetBugsNonAttribues: Bug[] = [];
  public chefsProjet!: Utilisateur[];
  public devsProjet: Utilisateur[] = [];
  public betasProjet!: Utilisateur[];


  // modals
  showDeleteModal!: boolean;
  showModal!: boolean;
  showAssignModal!: boolean;
  showTestsObsoletesModal!: boolean;
  showTestsPlansModal!: boolean;
  showGroupesFonctionnalitesModal!: boolean;
  showfonctionnalitesModal!: boolean;
  showbugsNonAttribuesModal!: boolean;
  showChefsProjetModal!: boolean;
  showDevsProjetModal!: boolean;
  showbetasProjetModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  deleteForm!: FormGroup;
  assignToForm!: FormGroup;
  assignToFormB!: FormGroup;
  submitted = false;
  versionActuelle = [];
  toutesVersions = [];

  // alerte
  alert: boolean = false;

  constructor(
    private projetService: ProjetService,
    private utilisateurService: UtilisateurService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router,
    private bugsService: BugsService
  ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.utilisateur =  this.sessionStorageService.get('loggedInUtilisateur');
    // @ts-ignore
    this.userId = this.utilisateur.id.toString();

    this.projetService.getUserProjets(this.userId).subscribe(
      (response: Projet[]) => {
        this.projets = response;
        for(let projet of this.projets) {
          if(projet.membresProjets){
            let membresProjet: Utilisateur[] = [];
            for(let membre of projet.membresProjets){
              if(membre.utilisateur_utilisateur_id.id){
                this.utilisateurService.getUtilisateurById(membre.utilisateur_utilisateur_id.id).subscribe(
                  (response: Utilisateur) => {
                    if(projet.id){
                      membresProjet.push(response);
                    }
                  }
                )
              }
            }
            let projetMembresProjets = membresProjet;
          }
          // @ts-ignore
          const toString = projet.id.toString();
          this.bugsService.getBugsByProjetId(toString).subscribe(
            (response: Bug[]) => {
              projet.bugs = response;
              if(projet.bugs){
                for(let bug of projet.bugs){
                  if(!bug.archive && !bug.assigne_a_utilisateur_id){
                    this.alert = true;
                  }
                }
              }
            }
          );
        }
        this.projetsFull = this.projets;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );

  }


}
