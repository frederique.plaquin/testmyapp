import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";


@Component({
  selector: 'app-bugs-actifs',
  templateUrl: './bugs-actifs.component.html',
  styleUrls: ['./bugs-actifs.component.scss']
})
export class BugsActifsComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;

  constructor() {}

  ngOnInit(): void {

  }

}
