import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauTypesErreursComponent } from './tableau-types-erreurs.component';

describe('TableauPlansComponent', () => {
  let component: TableauTypesErreursComponent;
  let fixture: ComponentFixture<TableauTypesErreursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauTypesErreursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauTypesErreursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
