// source router queryparams: https://angular.io/api/router/UrlCreationOptions#queryParams
// source editable fields: https://netbasal.com/keeping-it-simple-implementing-edit-in-place-in-angular-4fd92c4dfc70
// source editable fields: https://stackblitz.com/edit/angular-inline-edit-a494su?file=src%2Fapp%2Fedit-input%2Fedit-input.component.html
import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {DataService} from "../../services/data.service";
import {SessionStorageService} from "../../services/session-storage.service";
import {DisplayService} from "../../services/display.service";
import {HttpErrorResponse} from "@angular/common/http";
import {TypeErreur} from "../../model/typeErreur";
import {TypesErreursService} from "../../services/types-erreurs.service";

@Component({
  selector: 'app-tableau-types-erreurs',
  templateUrl: './tableau-types-erreurs.component.html',
  styleUrls: ['./tableau-types-erreurs.component.scss']
})
export class TableauTypesErreursComponent implements OnInit {
  navigationSubscription: any;
  localStorageChanges$ = this.sessionStorageService.changes$;
  showModal!: boolean;
  reloaded!: boolean;

  public types!: TypeErreur[];
  typeId!: number;
  typeNom!: string;
  typeDetails!: string;
  typePriorite!: string;
  typeNiveau!: string;
  typeCouleur!: string;
  archive!: boolean;

  constructor(
    private router: Router,
    private typeErreurService: TypesErreursService,
    private data: DataService,
    private sessionStorageService: SessionStorageService,
    private displayService: DisplayService
  ) {}

  ngOnInit(): void {
    this.typeErreurService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.types = response;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    )
  }

  onClick(event: any, objet: any)
  {
    this.showModal = true; // Show-Hide Modal Check
    this.typeId = objet.id;
    this.typeDetails = objet.details;
    this.typeNom = objet.nom;
    this.typePriorite = objet.priorite;
    this.typeNiveau = objet.niveau;
    this.typeCouleur = objet.couleur;
    this.archive = false;

  }

  //Bootstrap Modal Close event
  hide()
  {
      this.showModal = false;
  }

  // source: https://edupala.com/how-to-pass-data-in-angular-routed-component/
  redirect(to: string, objet?: any) {
    // display manager set data
    this.displayService.setData('type', objet);
    this.displayService.setData('composant', 'dashboard-type');
    // display manager set url with data previously setted in service
    const url = this.displayService.displayManager();
    if(objet) {
      this.router.navigate([`/${url}/`], {
        state: {
          navigateTo: to
        }
      }).then();
    } else if(to == 'gestionnaire-de-types-d-erreurs') {
      // @ts-ignore
      this.router.navigate([`/${to}`], {
        state: {
          navigateTo: to,
        }
      }).then();
    } else {
      this.router.navigate([`/${to}/`], {
        state: {
          navigateTo: to
        }
      }).then();
    }
  }
}
