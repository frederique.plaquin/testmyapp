import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlanPopupSidebarComponent } from './add-plan-popup-sidebar.component';

describe('AddPlanPopupComponent', () => {
  let component: AddPlanPopupSidebarComponent;
  let fixture: ComponentFixture<AddPlanPopupSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPlanPopupSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlanPopupSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
