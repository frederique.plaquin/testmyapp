import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProjetPopupSidebarComponent } from './add-projet-popup-sidebar.component';

describe('AddProjetPopupComponent', () => {
  let component: AddProjetPopupSidebarComponent;
  let fixture: ComponentFixture<AddProjetPopupSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProjetPopupSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProjetPopupSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
