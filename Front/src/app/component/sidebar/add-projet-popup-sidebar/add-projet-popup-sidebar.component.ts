import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProjetService} from "../../../services/projet.service";
import {Projet} from "../../../model/projet";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-add-projet-popup-sidebar',
  templateUrl: './add-projet-popup-sidebar.component.html',
  styleUrls: ['./add-projet-popup-sidebar.component.scss']
})
export class AddProjetPopupSidebarComponent implements OnInit {
  // popup(modal)
  closeResult!: string;
  modalOptions:NgbModalOptions;

  // create plan form
  createProjetForm!: FormGroup;
  submitted = false;

  constructor(
    private modalService: NgbModal,
    private projetService: ProjetService,
    private formBuilder: FormBuilder,
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.createProjetForm = this.formBuilder.group({
      nom: ['', Validators.required],
      details: ['', Validators.required],
      version: ['', [Validators.required]]
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddProjetPopupSidebarComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createProjetForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createProjetForm.invalid) {
      return;
    }
    // set new projet
    const newProjet: Projet = {
      nom: this.createProjetForm.value.nom,
      details: this.createProjetForm.value.details,
      version: this.createProjetForm.value.version
    }

    // send new projet to service for saving to bdd
    this.projetService.addProjet(newProjet).subscribe({
   next:   (response: Projet) => {
        alert('Le projet ' + response.nom + ' a bien été enregistré !')
        this.createProjetForm.reset();
        window.location.reload();
      },
     error: (error: HttpErrorResponse) => {
       console.log(error);
       console.log(error.message);
      //  this.createProjetForm.reset();
      }
    })
  }

  onReset() {
    this.submitted = false;
    this.createProjetForm.reset();
  }

}
