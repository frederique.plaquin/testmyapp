import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Plan} from "../../model/plan";
import {DisplayService} from "../../services/display.service";
import {Projet} from "../../model/projet";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjetService} from "../../services/projet.service";
import {SessionStorageService} from "../../services/session-storage.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Groupe} from "../../model/groupe";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-sidebar',
  animations: [
    trigger('slideInOut', [
      state('in', style({
        width: '*'
      })),
      state('out', style({
        overflow: 'hidden',
        width: '50px'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  collapsed!: string;
  projets!: Projet[];
  plans!: Plan[];
  groupes!: Groupe[];
  user!: Utilisateur;

  constructor(
    private displayService: DisplayService,
    private router: Router,
    private route: ActivatedRoute,
    private projetService: ProjetService,
    private sessionStorageService: SessionStorageService
    ) { }

  ngOnInit() {
    this.collapsed = 'out';
    // getting data from session storage
    this.user = this.sessionStorageService.get('loggedInUtilisateur');
    // verification du rôle de l'utilisateur  avant affichage des projets
    if(this.user?.role?.id == 1) {
      this.projetService.getProjets().subscribe(
        (response: Projet[]) => {
          this.projets = response;
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      )
    } else {
      // @ts-ignore
      this.projetService.getUserProjets(this.user.id.toString()).subscribe(
        (response: Projet[]) => {
          this.projets = response;
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      );
    }
  }

  // accordion sidebar toggle state
  toggleSidebar() {
    this.collapsed = this.collapsed === 'out' ? 'in' : 'out';
  }

  // accordion sidebar toggle state
  toggleSidebarAccordion() {
    this.collapsed = 'in';
  }

  // redirection
  navigateTo(type: string, objet?: object) {
    if(type=='plan') {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('plan', objet);
      this.displayService.setData('composant', 'dashboard-plan');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then(() => {
        window.location.reload();
      });
    } else if(type=='projet') {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('projet', objet);
      this.displayService.setData('composant', 'dashboard-projet');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then(() => {
        window.location.reload();
      });
    } else if (type == 'dashboard'){
      // display manager set data
      this.displayService.setData('user', this.user);
      this.displayService.setData('composant', 'dashboard');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`], {
      }).then();
    } if(type=='groupe') {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('groupe', objet);
      this.displayService.setData('composant', 'dashboard-groupe');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then(() => {
        window.location.reload();
      });
    }
  }

}
