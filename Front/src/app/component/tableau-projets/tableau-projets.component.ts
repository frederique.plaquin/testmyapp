import { Component, OnInit } from '@angular/core';
import { Projet } from "../../model/projet";
import { ProjetService } from "../../services/projet.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SessionStorageService } from "../../services/session-storage.service";
import { DisplayService } from "../../services/display.service";
import { HttpErrorResponse } from "@angular/common/http";
import { Utilisateur } from "../../model/Utilisateur";
import { FormGroup } from "@angular/forms";
import { PlanService } from "../../services/plan.service";
import { GroupeFonctionnaliteService } from "../../services/groupe-fonctionnalite.service";
import { FonctionnaliteService } from "../../services/fonctionnalite.service";
import { MembreProjetService } from "../../services/membre-projet.service";
import { VersionService } from "../../services/version.service";
import { Version } from "../../model/version";


@Component({
  selector: 'app-tableau',
  templateUrl: './tableau-projets.component.html',
  styleUrls: ['./tableau-projets.component.scss']
})
export class TableauProjetsComponent implements OnInit {

  // forcing reloading component on same route
  navigationSubscription: any;
  public alreadyVisited!: boolean;
  // recuperation donnees du localStorage
  localStorageChanges$ = this.sessionStorageService.changes$;
  public storedProjets: Projet[] = [];

  showModalProjet!: boolean;
  showDeleteModal!: boolean;
  showEditModal!: boolean;
  showEditVersionModal!: boolean;

  projetId!: number;
  projetNom!: string;
  projetDetails!: string;
  projetVersion!: string;
  deleteProjet!: Projet;
  editProjet!: Projet;
  editVersionProjet!: Projet;

  utilisateur!: Utilisateur;
  utilisateurId!: string;

  projet!: Object;

  isValid!: boolean;
  infoMessage!: string;

  submitted!: boolean;

  deleteForm!: FormGroup;
  editForm!: FormGroup;
  editVersionForm!: FormGroup;

  constructor(
    private projetService: ProjetService,
    private plansService: PlanService,
    private groupesFonctionnalitesService: GroupeFonctionnaliteService,
    private fonctionnalitesService: FonctionnaliteService,
    private membresService: MembreProjetService,
    private versionsService: VersionService,
    private router: Router,
    private sessionStorageService: SessionStorageService,
    private route: ActivatedRoute,
    private displayService: DisplayService
  ) {
  }

  // @ts-ignore
  ngOnInit(){
    this.utilisateur = this.sessionStorageService.get('loggedInUtilisateur');
    this.utilisateurId = this.route.snapshot.params['id'];

    if(this.utilisateur.role?.id == 1) {
      this.projetService.getProjets().subscribe(
        (response: Projet[]) => {
          this.storedProjets = response;
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      );
    } else {
      this.projetService.getUserProjets(this.utilisateurId).subscribe(
        (response: Projet[]) => {
          this.storedProjets = response;
        },
        (error: HttpErrorResponse) => {
          console.log(error.message);
        }
      );
    }
  }

  onClick(event: any, objet: any)
  {
      this.showModalProjet = true; // Show-Hide Modal Check
      // @ts-ignore
      this.projetId = objet.id;
      // @ts-ignore
      this.projetNom = objet.nom;
      // @ts-ignore
      this.projetDetails = objet.details;
      // @ts-ignore
      this.projetVersion = objet.version;
  }

  onEdit(event: any, projet: Projet)
  {
    this.editProjet = projet;
    this.showEditModal = true; // Show-Hide Modal Check
  }

  onEditVersion(event: any, projet: Projet)
  {
    this.editVersionProjet = projet;
    this.showEditVersionModal = true; // Show-Hide Modal Check
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  //Bootstrap Modal Close event
  hide(modal: String)
  {
    if(modal == "projet") {
      this.showModalProjet = false;
    } else if(modal == "deleteModal") {
      this.showDeleteModal = false;
    } else if(modal == 'editVersionModal') {
      this.showEditVersionModal = false;
    } else if(modal == 'editModal') {
      this.showEditModal = false;
    }
  }

  // source: https://edupala.com/how-to-pass-data-in-angular-routed-component/
  redirect(objet: any) {
    if(objet.id) {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('projet', objet);
      this.displayService.setData('composant', 'dashboard-projet');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`], {
        state: {
          navigateTo: "projetDashboard",
          projetId: objet.id,
        }
      }).then(() => {
         // window.location.reload();
      });
    }
  }
  //-------------------------------------------------------------
  search(key:string){
    const results:Projet[]=[];
    for(let projet of this.storedProjets ){
      if(projet.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase()) !== -1){
        results.push(projet);
      }
    }
    this.storedProjets=results;
      if(results.length==0 || !key){

      this.route.data.subscribe(value => {
        this.storedProjets = value['userProjets'];
      });
    }
  }

  onDelete(event: any, projet: Projet) {
    this.deleteProjet = projet;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  onArchiveProjet(projet: Projet) {
    let message: string;
    this.submitted = true;
    // @ts-ignore
    if(projet.archive) {
      message = 'désarchivé !';
      projet.archive = false;
    } else if(!projet.archive) {
      message = 'archivé !';
      projet.archive = true;
    }
    this.projetService.updateProjet(projet).subscribe(
      (response) => {
        alert('Le projet ' +  response.nom + ' a bien été ' + message + ' !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onSubmit(projet: Projet) {
    this.submitted = true;

    this.projetService.updateProjet(projet).subscribe(
      (response: Projet) => {
        alert('Le plan de test ' +  response.nom + ' a bien été mis à jour !');

        const newVersion: Version = {
          // @ts-ignore
          projet_projet_id: response.id,
          version: response.version
        }

        if(this.editProjet.version != response.version) {
          this.versionsService.addVersion(newVersion).subscribe(
            (response: Version) => {
              alert('La version du projet '  +  projet.nom + ' est désormais ' + response.version);
              window.location.reload();
            },
            (error: HttpErrorResponse) => {
              console.log(error.message);
            }
          );
        }

        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        this.editForm.reset();
        console.log(error.message);
      }
    );
    this.showEditModal = false; // Show-Hide Modal Check
  }

  onSaveNewVersion(projet: Projet) {
    this.submitted = true;

    this.editVersionProjet.version = projet.version

    this.projetService.updateProjet(this.editVersionProjet).subscribe(
      (response: Projet) => {
        const newVersion: Version = {
          // @ts-ignore
          projet_projet_id: response.id,
          version: response.version
        }

        this.versionsService.addVersion(newVersion).subscribe(
          (response: Version) => {
            alert('La version du projet '  +  projet.nom + ' est désormais ' + response.version);
            window.location.reload();
          },
          (error: HttpErrorResponse) => {
            console.log(error.message);
          }
        );
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
    this.showEditModal = false; // Show-Hide Modal Check

  }
}
