// source router queryparams: https://angular.io/api/router/UrlCreationOptions#queryParams
// source editable fields: https://netbasal.com/keeping-it-simple-implementing-edit-in-place-in-angular-4fd92c4dfc70
// source editable fields: https://stackblitz.com/edit/angular-inline-edit-a494su?file=src%2Fapp%2Fedit-input%2Fedit-input.component.html
import {Component, Input, OnInit} from '@angular/core';
import {Projet} from "../../model/projet";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {DataService} from "../../services/data.service";
import {SessionStorageService} from "../../services/session-storage.service";
import {ProjetService} from "../../services/projet.service";
import {DisplayService} from "../../services/display.service";
import {BugsService} from "../../services/bugs.service";
import {Bug} from "../../model/bug";
import {TypesErreursService} from "../../services/types-erreurs.service";
import {TypeErreur} from "../../model/typeErreur";
import {Utilisateur} from "../../model/Utilisateur";
import {Plan} from "../../model/plan";
import {GroupeFonctionnaliteService} from "../../services/groupe-fonctionnalite.service";
import {Groupe} from "../../model/groupe";
import {groups} from "d3";
import {Fonctionnalite} from "../../model/fonctionnalite";

@Component({
  selector: 'app-tableau-bugs-repertories',
  templateUrl: './tableau-bugs-repertories.component.html',
  styleUrls: ['./tableau-bugs-repertories.component.scss']
})
export class TableauBugsRepertoriesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  navigationSubscription: any;
  public alreadyVisitedProjet!: boolean;
  localStorageChanges$ = this.sessionStorageService.changes$;
  showModal!: boolean;
  reloaded!: boolean;
  url!: string;
  //----------------------------------------------------------------------------------------------
  typesErreurs: any;
  testPlans!: Plan[] | undefined;

  projet!: Projet;
  projetId!: number | undefined;
  subscription!: Subscription;
  projetName!: string;

  public bugs!: Bug[] | undefined;
  bugId!: number | undefined;
  bugNom!: string | undefined;
  bugDetails!: string | undefined;
  bugCommentaire!: string | undefined;
  bugProjet!: Projet;
  archive!: boolean;


  projetBugs!: Bug[];

  constructor(
    private router: Router,
    private bugService: BugsService,
    private data: DataService,
    private sessionStorageService: SessionStorageService,
    private projetService: ProjetService,
    private displayService: DisplayService,
    private route: ActivatedRoute,
    private typesErreursService: TypesErreursService,
    private gpeFonctionnalitesService: GroupeFonctionnaliteService
  ) {}

  ngOnInit(): void {
    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];

    this.projetService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;

        this.testPlans = this.projet.testPlans;
        // @ts-ignore
        const stringId = this.projetId.toString();

        this.bugService.getBugsByProjetId(stringId).subscribe(
          (response: Bug[]) => {
            this.bugs = response;

            for(let bug of this.bugs){
              if(bug.fonctionnalite?.groupe_fonctionnalites_groupe_id)
                this.gpeFonctionnalitesService.getGroupeById(bug.fonctionnalite.groupe_fonctionnalites_groupe_id).subscribe(
                  (response: Groupe) => {
                    bug.groupeFonctionnalites = response
                    console.log(bug)
                  }
                )
            }
          }
        );

      }
    );

    this.typesErreursService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.typesErreurs = response;
      }
    )

    this.reloaded = Boolean(sessionStorage.getItem('reloaded'));
  }

  onClick(event: any, bug: Bug)
  {
    this.showModal = true; // Show-Hide Modal Check
    this.bugId = bug.id;
    this.bugNom = bug.fonctionnalite?.nom;
    this.bugDetails = bug.details;
    this.bugProjet = bug.projet_projet_id;
    this.archive = false;
    this.bugCommentaire = bug.commentaire
  }

  //Bootstrap Modal Close event
  hide()
  {
      this.showModal = false;
  }

  // source: https://edupala.com/how-to-pass-data-in-angular-routed-component/
  redirect(to: string, objet?: any, type?: string) {
    sessionStorage.removeItem('projetId');
    if(objet) {
      if(type == 'plan') {
        this.displayService.setData('plan', objet);
        this.displayService.setData('composant', 'dashboard-plan');
        // display manager set url with data previously setted in service
        this.url = this.displayService.displayManager();
        this.router.navigate([`${this.url}`], {
          state: {
            navigateTo: to
          }
        }).then(() => {
          // window.location.reload();
        });
      } else {
        this.router.navigate([`/${to}/`, objet.id], {
          state: {
            navigateTo: to
          }
        }).then(() => {
          // window.location.reload();
        });
      }
    } else if(to == 'gestionnaire-de-plans') {
      // @ts-ignore
      this.router.navigate([`/${to}/${this.projet['id']}`], {
        state: {
          navigateTo: to,
          // @ts-ignore
          projetId: this.projet['id'],
        }
      }).then(() => {
        // window.location.reload();
      });
    } else if(to == 'gestionnaire-de-bugs') {
      // @ts-ignore
      this.displayService.setData('bugs', this.bugs);
      this.sessionStorageService.set('target', 'projet');
           this.displayService.setData('composant', 'gestionnaire-de-bugs');
           // display manager set url with data previously setted in service
           const url = this.displayService.displayManager();
          this.router.navigate([`${url}/${this.projetId}`]
          ).then(() => {
            // window.location.reload();
          });
    } else {
      this.router.navigate([`/${to}/`], {
        state: {
          navigateTo: to
        }
      }).then(() => {
        // window.location.reload();
      });
    }
  }

  search(key:string){
    const results:Bug[]=[];

    if(this.bugs) {
      for(let bug of this.bugs){
        if(bug.fonctionnalite?.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1){
          results.push(bug);
        }
      }
    }
    this.bugs=results;
    if(results.length==0 || !key){
      this.route.data.subscribe(value => {
        this.projet = value['projet'];
        if(this.projet.bugs) {
          this.bugs = this.projet.bugs;
        }
      });
    }
  }
}
