import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauBugsRepertoriesComponent } from './tableau-bugs-repertories.component';

describe('TableauPlansComponent', () => {
  let component: TableauBugsRepertoriesComponent;
  let fixture: ComponentFixture<TableauBugsRepertoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauBugsRepertoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauBugsRepertoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
