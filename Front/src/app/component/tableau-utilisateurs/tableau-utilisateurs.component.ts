import { Component, OnInit } from '@angular/core';

import {Router} from "@angular/router";

import {HttpErrorResponse} from "@angular/common/http";
import {UtilisateurService} from "../../services/utilisateur.service";
import {Utilisateur} from "../../model/Utilisateur";

@Component({
  selector: 'app-tableau-utilisateurs',
  templateUrl: './tableau-utilisateurs.component.html',
  styleUrls: ['./tableau-utilisateurs.component.scss']
})
export class TableauUtilisateursComponent implements OnInit {

  public utilisateurs: Utilisateur[] = [];


  constructor(
    private utilisateurService: UtilisateurService,
    private router: Router) { }

  ngOnInit(): void {
    this.getProjets();
  }


  // envoi requete au service pour récupération des projets depuis la bdd
  public getProjets(): void {
    this.utilisateurService.getUtilisateurs().subscribe(
      (response: Utilisateur[]) => {
        this.utilisateurs= response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  clickRouteRole() {
    this.router.navigateByUrl('app-gestionnaire-roles');
  }

  onValidate(event: any, utilisateur: Utilisateur) {
    if(utilisateur.confirmed){
      utilisateur.confirmed = false;
    } else {
      utilisateur.confirmed = true;
    }
    utilisateur.confirmed
    this.utilisateurService.updateUtilisateurs(utilisateur).subscribe(
      (response: Utilisateur) => {
        if(response.confirmed) {
          alert(utilisateur.prenom + ' ' + utilisateur.nom + ' ' + ' a bien été activé !')
        } else {
          alert(utilisateur.prenom + ' ' + utilisateur.nom + ' ' + ' a bien été désactivé !')
        }

      }
    )
  }
}
