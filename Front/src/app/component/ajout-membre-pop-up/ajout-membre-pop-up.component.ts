import { Component, OnInit, Type } from '@angular/core';
import {ModalDismissReasons, NgbActiveModal, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-ajout-membre-pop-up-confirm',
  template: `
  <div class="modal-header">
    <h4 class="modal-title" id="modal-title">Ajout membre</h4>
    <button type="button" class="btn-close" aria-describedby="modal-title" (click)="modal.dismiss('Cross click')"></button>
  </div>
  <div class="modal-body">
    <form [formGroup]="registerForm" (ngSubmit)="onSubmit()">
      <div class="form-row">
        <div class="form-group">
          <label class="mb-1">Rechercher membre</label>
          <input type="text" formControlName="member" class="form-control mb-2" [ngClass]="{ 'is-invalid': submitted && f['member'].errors }" />
          <div *ngIf="submitted && f['member'].errors" class="invalid-feedback">
            <div *ngIf="f['member'].errors['required']">Champs requis</div>
          </div>
        </div>
        <div class="form-group">
          <label class="mb-1">Rôle</label>
          <input type="text" formControlName="role" class="form-control mb-2" [ngClass]="{ 'is-invalid': submitted && f['role'].errors }" />
          <div *ngIf="submitted && f['role'].errors" class="invalid-feedback">
            <div *ngIf="f['role'].errors['required']">>Champs requis</div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label class="mb-1">Projet</label>
        <input type="text" formControlName="project" class="form-control mb-2" [ngClass]="{ 'is-invalid': submitted && f['project'].errors }" />
        <div *ngIf="submitted && f['project'].errors" class="invalid-feedback">
          <div *ngIf="f['project'].errors['required']">>Champs requis</div>
        </div>
      </div>
      <div class="mt-3 mb-2">
        <button class="btn btn-primary w-100">Enregistrer</button>
      </div>
    </form>
  </div>
  `
})
export class AjoutMembrePopUpComponentConfirm implements OnInit{
  constructor(
    public modal: NgbActiveModal,
    private modalService: NgbModal,
  ) {}

  ngOnInit(): void {
    }

  closeResult!: string;
  modalOptions!: NgbModalOptions;
  registerForm!: FormGroup;
  submitted = false;

  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AjoutMembrePopUpComponentConfirm.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}

const MODALS: {[name: string]: Type<any>} = {
  focusFirst: AjoutMembrePopUpComponentConfirm
};
@Component({
  selector: 'app-ajout-membre-pop-up',
  templateUrl: './ajout-membre-pop-up.component.html',
  styleUrls: ['./ajout-membre-pop-up.component.scss']

})
export class AjoutMembrePopUpComponent implements OnInit {
  constructor(public modal: NgbModal) { }

  ngOnInit(): void {
  }

  open(name: string) { this.modal.open(MODALS[name]); }
}
