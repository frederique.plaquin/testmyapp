import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauContextuelComponent } from './tableau-contextuel.component';

describe('TableauContextuelComponent', () => {
  let component: TableauContextuelComponent;
  let fixture: ComponentFixture<TableauContextuelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauContextuelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauContextuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
