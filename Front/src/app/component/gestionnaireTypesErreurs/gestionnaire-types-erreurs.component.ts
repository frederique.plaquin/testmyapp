// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, OnInit} from '@angular/core';
import { Plan } from "../../model/plan";
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {DisplayService} from "../../services/display.service";
import {TypeErreur} from "../../model/typeErreur";
import {TypesErreursService} from "../../services/types-erreurs.service";

@Component({
  selector: 'app-gestionnaire-groupes-fonctionnalites',
  templateUrl: './gestionnaire-types-erreurs.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss']
})
export class GestionnaireTypesErreursComponent implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;
  public plan!: Plan;
  public typeErreurs!: TypeErreur[] | undefined;
  public typeErreur!: TypeErreur;
  public editType!: TypeErreur;
  public deleteType!: TypeErreur;
  public confirmGroupe!: TypeErreur;
  public setPriorite!: TypeErreur;
  public setNiveau!: TypeErreur;
  public setCouleur!: TypeErreur;
  public unDraftGroupe!: TypeErreur;


  // modals
  showDeleteModal!: boolean;
  showConfirmModal!: boolean;
  showPrioriteModal!: boolean;
  showNiveauModal!: boolean;
  showCouleurModal!: boolean;
  showActivateModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  prioriteForm!: FormGroup;
  niveauForm!: FormGroup;
  couleurForm!: FormGroup;
  submitted = false;

  // ############ COLOR PICKER
  public color1: string = '#2889e9';
  couleur!: string;
  // ############ COLOR PICKER END

  constructor(
    private typeErreurService: TypesErreursService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.route.data.subscribe(value=>{
      this.typeErreurs = value['types'];
    });
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireTypesErreursComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(type: TypeErreur) {
    this.submitted = true;
    this.editType = type;
    this.typeErreurService.updateTypeErreur(this.editType).subscribe(
      (response: TypeErreur) => {
        alert('Le type d\'erreur ' +  response.nom + ' a bien été mis à jour !');
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchiveType(type: TypeErreur) {
    this.submitted = true;
    // @ts-ignore
    if(type.archive) {
      type.archive = false;
    } else if(!type.archive) {
      type.archive = true;
    }
    this.typeErreurService.updateTypeErreur(type).subscribe(
      (response) => {
        alert('Le type d\'erreur ' +  response.nom + ' a bien été archivé !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onModifyPriorite(prioriteForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setPriorite.priorite = prioriteForm['priorite'];
    this.typeErreurService.updateTypeErreur(this.setPriorite).subscribe(
      (response) => {
        alert('Priorité définie à ' +  this.setPriorite.priorite + ' !');
        this.showPrioriteModal = false;
        this.prioriteForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onModifyNiveau(niveauForm: any) {
    this.submitted = true;
    // @ts-ignore
    this.setNiveau.niveau = niveauForm.value.niveau;
    this.setNiveau.couleur = this.couleur;
    this.typeErreurService.updateTypeErreur(this.setNiveau).subscribe(
      (response) => {
        alert('Niveau défini à ' +  this.setNiveau.niveau + ' !');
        this.showNiveauModal = false;
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onModifyCouleur(couleurForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setCouleur.couleur = this.couleur;
    this.typeErreurService.updateTypeErreur(this.setCouleur).subscribe(
      (response) => {
        alert('Couleur définie à ' +  this.setCouleur.couleur + ' !');
        this.showCouleurModal = false;
        // this.couleurForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, type: any)
  {
    this.editType = type;
    this.showModal = true; // Show-Hide Modal Check
    if(this.editType.couleur) {
      this.color1 = this.editType.couleur;
    }
  }

  onDelete(event: any, type: TypeErreur) {
    this.deleteType = type;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  onSetPriorite(event: any, type: TypeErreur) {
    this.setPriorite = type;
    this.showPrioriteModal = true; // Show-Hide Modal Check
  }

  onSetNiveau(event: any, type: TypeErreur) {
    this.setNiveau = type;
    this.showNiveauModal = true; // Show-Hide Modal Check
    if(this.setNiveau.couleur) {
      this.color1 = this.setNiveau.couleur;
    }
  }

  onSetCouleur(event: any, type: TypeErreur) {
    this.setCouleur = type;
    this.showCouleurModal = true; // Show-Hide Modal Check
    if(this.setCouleur.couleur) {
      this.color1 = this.setCouleur.couleur;
    }
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    this.showModal = false;
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    }  else if(modal == "setPrioriteModal") {
      this.showPrioriteModal = false;
    } else if(modal == "setNiveauModal") {
      this.showNiveauModal = false;
    } else if(modal == "setCouleurModal") {
      this.showCouleurModal = false;
    }
  }

  navigateTo(type: TypeErreur) {
    if(type.id) {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('type', type);
      this.displayService.setData('composant', 'dashboard-type');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then();
    }
  }
  // ############ COLOR PICKER
  public onEventLog(event: string, data: any): void {
    console.log('""""');
    console.log(event, data);
    console.log('""""');
    this.couleur = data;
    console.log('###############################this.couleur');
    console.log(this.couleur);
  }
  // ############ COLOR PICKER END
}
