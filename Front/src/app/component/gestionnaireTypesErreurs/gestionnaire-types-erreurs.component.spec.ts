import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireTypesErreursComponent } from './gestionnaire-types-erreurs.component';

describe('GestionnairePlansComponent', () => {
  let component: GestionnaireTypesErreursComponent;
  let fixture: ComponentFixture<GestionnaireTypesErreursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireTypesErreursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireTypesErreursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
