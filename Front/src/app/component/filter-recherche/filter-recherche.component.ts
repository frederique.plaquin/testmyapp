import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Plan } from 'src/app/model/plan';
import { Projet } from 'src/app/model/projet';

@Component({
  selector: 'app-filter-recherche',
  templateUrl: './filter-recherche.component.html',
  styleUrls: ['./filter-recherche.component.scss']
})
export class FilterRechercheComponent implements OnInit {

  @Output() filterPlan= new EventEmitter<Plan[]>();


  public projet!: Projet;
  public plans!: Plan[];

  form=new FormGroup({
    choix:new FormControl()
  });


  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.getPlans();
  }
  getPlans(){
    this.route.data.subscribe(value=>{
      this.projet = value['projet'];
      // @ts-ignore
      this.plans = this.projet['testPlans'];
    });
  }

  //##########################################################
  sendTogestionnaireDePlan(key:string){
    const results:Plan[]=[];
    for(const plan of this.plans){
      if(plan.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase()) !==-1
      || plan.details?.toLocaleLowerCase().indexOf(key.toLocaleLowerCase())!==-1 ){
        results.push(plan);

      this.filterPlan.emit(results);
    }
      if(results.length ===0 && false){

        this.getPlans()
        this.filterPlan.emit(this.plans);
      }
    }
  }
  //################################################################################"
  submit(){
    const results:Plan[]=[];
    if(this.form.value.choix=="actif"){
      for(let plan of this.plans){
        if(!plan.archive){
          results.push(plan);
        }
      }
      this.filterPlan.emit(results);
    } else if(this.form.value.choix=="inactif"){
        for(let plan of this.plans){
          if(plan.archive){
            results.push(plan);
          }
      }
      this.filterPlan.emit(results);
    }else{
      this.getPlans();
      this.filterPlan.emit(this.plans);
    }

  }
}

