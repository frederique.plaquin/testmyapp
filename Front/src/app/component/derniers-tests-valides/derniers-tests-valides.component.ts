import {Component, Input, OnInit} from '@angular/core';
import {Utilisateur} from "../../model/Utilisateur";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ProjetService} from "../../services/projet.service";
import {Projet} from "../../model/projet";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {ProjetFull} from "../../model/projetFull";
import {UtilisateurService} from "../../services/utilisateur.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {Plan} from "../../model/plan";

@Component({
  selector: 'app-derniers-tests-valides',
  templateUrl: './derniers-tests-valides.component.html',
  styleUrls: ['./derniers-tests-valides.component.scss', '../../../styles/gestionnaires.scss', '../../../styles/bugs.scss', '../../../styles/tests.scss'  ]
})
export class DerniersTestsValidesComponent implements OnInit {
  @Input() loggedInUser!: Utilisateur;
  @Input() projet!: Projet;
  public projetId!: number;
  public projetFull!: ProjetFull;
  public membresProjet: Utilisateur[] = [];
  public derniersTestsRepertories!: Fonctionnalite[];
  public testDetails!: Fonctionnalite;

  // modal
  closeResult!: string;
  modalOptions: NgbModalOptions;
  // fin modal
  public testValides!: Fonctionnalite[];

  constructor(private router: Router,
              private bugsService: BugsService,
              private projetsService: ProjetService,
              private modalService: NgbModal,
              private bugService: BugsService,
              private utilisateursService: UtilisateurService,) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    }
  }

  ngOnInit(): void {
    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = +splitUrl[2];


    this.projetsService.getProjetById(this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;
        this.projetFull = this.projetsService.getProjetDetails(response);

        this.bugService.getBugs().subscribe(
          (response: Bug[]) => {
            this.projetFull.bugs = response.filter(bug => bug.projet_projet_id.id == this.projetFull.id);
          }
        );
        if (this.projet.membresProjets)
          for (let membre of this.projet.membresProjets) {
            if (membre.utilisateur_utilisateur_id)
              this.utilisateursService.getUtilisateurById(membre.utilisateur_utilisateur_id).subscribe(
                (response: Utilisateur) => {
                  this.membresProjet.push(response);
                }
              )
          }
        this.projetFull.membres = this.membresProjet;
        if (this.projetFull.fonctionnalites)
          this.derniersTestsRepertories = this.projetFull.fonctionnalites.filter(test => !test.archive);

        // @ts-ignore
        this.derniersTestsRepertoriesNonAssignes = this.derniersTestsRepertories.filter(test => !test.testee || new Date(new Date(test.testee).setMonth(new Date(test.testee).getMonth() + test.periodicite)) < new Date(Date.now()))
          // @ts-ignore
          .sort((x, y) => x?.priorite - +y?.priorite);

        if(this.projetFull.testPlans)
          for (let plan of this.projetFull.testPlans) {
            this.testValides = this.getTestsValides();
          }
      }
    );
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${DerniersTestsValidesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any, fonctionnalite: Fonctionnalite) {
    this.modalService.open(content, {centered: true});
    this.testDetails = fonctionnalite;
  }

  // @ts-ignore
  getGroupeFonctionnalite(groupeId: number) {
    if(this.projetFull.groupesFonctionnalites)
      for(let groupe of this.projetFull.groupesFonctionnalites){
        if(groupe.id == groupeId)
          return groupe;
      }
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  // END popup(modal)

  navigateTo(type: string, objet?: any) {
    if (type == 'bug') {
      this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
    }
  }

  assignToMe(bug: Bug) {
    bug.assigne_a_utilisateur_id = this.loggedInUser;
    this.bugsService.updateBug(bug).subscribe(
      (response: Bug) => {
        alert('le bug # ' + response.id + ' ' + response.typeErreur?.nom + ' vous a bien été assigné.')
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  getTestplanDerniersTests(testplan: Plan) {
    let testplanFonctionnalites: Fonctionnalite[] = [];
    if (testplan.groupeFonctionnalites)
      for (let groupe of testplan.groupeFonctionnalites) {
        if (groupe.fonctionnalites)
          for (let fonctionnalite of groupe.fonctionnalites) {
            testplanFonctionnalites.push(fonctionnalite);
          }
      }
    return testplanFonctionnalites
  }

  getTestsValides() {
    let bugFonctionnalites: Fonctionnalite[] = [];
    let testValides: Fonctionnalite[] = [];
    if(this.projetFull) {
      let projetBugs = this.projetFull.bugs;
      let projetFonctionnalites = this.projetFull.fonctionnalites;

      // @ts-ignore
      if (projetBugs.length != 0) { // @ts-ignore
        for (let bug of projetBugs) {
          if (!bug.archive)
            if (bug.fonctionnalite)
              bugFonctionnalites.push(bug.fonctionnalite)
        }
      }

      // @ts-ignore
      if(projetFonctionnalites.length != 0)// @ts-ignore
        for (let fonctionnalite of projetFonctionnalites){
          if(fonctionnalite.testee && !bugFonctionnalites.includes(fonctionnalite)) { // @ts-ignore
            if(new Date(new Date(fonctionnalite.testee).setMonth(new Date(fonctionnalite.testee).getMonth() + fonctionnalite.periodicite)) > new Date(Date.now())){
              testValides.push(fonctionnalite);
            }
          }
        }
    }
    return testValides
  }
}
