import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTypeErreurPopupComponent } from './add-type-erreur-popup.component';

describe('AddPlanPopupComponent', () => {
  let component: AddTypeErreurPopupComponent;
  let fixture: ComponentFixture<AddTypeErreurPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTypeErreurPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTypeErreurPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
