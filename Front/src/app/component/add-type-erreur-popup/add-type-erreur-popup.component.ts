import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {TypeErreur} from "../../model/typeErreur";
import {TypesErreursService} from "../../services/types-erreurs.service";

@Component({
  selector: 'app-add-type-erreur-popup',
  templateUrl: './add-type-erreur-popup.component.html',
  styleUrls: ['./add-type-erreur-popup.component.scss']
})

export class AddTypeErreurPopupComponent implements OnInit {
  // popup(modal)
  closeResult!: string;
  modalOptions!: NgbModalOptions;

  // create plan form
  createTypeForm!: FormGroup;
  submitted = false;

  type!: TypeErreur;
  subscription!: Subscription;

  // ############ COLOR PICKER
  public color1: string = '#2889e9';

  couleur!: string;
  // ############ COLOR PICKER END

  constructor(
    private modalService: NgbModal,
    private typeErreurService: TypesErreursService,
    private formBuilder: FormBuilder,
    public vcRef: ViewContainerRef
  ) {
    // popup(modal)
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      keyboard: false
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.createTypeForm = this.formBuilder.group({
      nom: ['', Validators.required],
      details: [''],
      niveau: ['', Validators.required],
      priorite: [''],
      couleur: [''],
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${AddTypeErreurPopupComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.createTypeForm.controls; }

  // envoi du formulaire
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.createTypeForm.invalid) {
      return;
    }

    // set new plan

    const newType: TypeErreur = {
      nom: this.createTypeForm.value.nom,
      details: this.createTypeForm.value.details,
      niveau: this.createTypeForm.value.niveau,
      priorite: this.createTypeForm.value.priorite,
      couleur: this.couleur,
      archive: false
    }

    // send new plan to service for saving to bdd
      this.typeErreurService.addTypeErreur(newType).subscribe(
      (response: TypeErreur) => {
        alert('Le type d\'erreur ' +  response.nom + ' a bien été créé !')
        this.createTypeForm.reset();
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.createTypeForm.reset();
      }
    )
  }

  onReset() {
    this.submitted = false;
    this.createTypeForm.reset();
  }

  // ############ COLOR PICKER

  public onEventLog(event: string, data: any): void {
    this.couleur = data;
  }

  // ############ COLOR PICKER END

}
