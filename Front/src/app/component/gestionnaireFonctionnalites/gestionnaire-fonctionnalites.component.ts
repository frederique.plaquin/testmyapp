// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Groupe} from "../../model/groupe";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {FonctionnaliteService} from "../../services/fonctionnalite.service";

@Component({
  selector: 'app-gestionnaire-fonctionnalites',
  templateUrl: './gestionnaire-fonctionnalites.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss']
})
export class GestionnaireFonctionnalitesComponent implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;
  public groupe!: Groupe;
  public fonctionnalites!: Fonctionnalite[];
  public fonctionnalite!: Fonctionnalite;
  public editFonctionnalite!: Fonctionnalite;
  public deleteFonctionnalite!: Fonctionnalite;
  public setPeriodicite!: Fonctionnalite;
  public setPriorite!: Fonctionnalite;
  public priorite!: string;


  // modals
  showDeleteModal!: boolean;
  showPeriodiciteModal!: boolean;
  showPrioriteModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  periodiciteForm!: FormGroup;
  prioriteForm!: FormGroup;
  submitted = false;

  constructor(
    private fonctionnaliteService: FonctionnaliteService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {

    this.route.data.subscribe(value=>{
      this.groupe = value['groupe'];
      // @ts-ignore
      this.fonctionnalites = this.groupe.fonctionnalites;
    });
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireFonctionnalitesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(fonctionnalite: Fonctionnalite) {
    this.submitted = true;
    this.fonctionnaliteService.updateFonctionnalite(fonctionnalite).subscribe(
      (response: Fonctionnalite) => {
        alert('La fonctionnalité ' +  response.nom+ ' a bien été mise à jour !');
        window.location.reload();
        this.editForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchiveFonctionnalite(fonctionnalite: Fonctionnalite) {
    this.submitted = true;
    // @ts-ignore
    if(fonctionnalite.archive) {
      fonctionnalite.archive = false;
    } else if(!fonctionnalite.archive) {
      fonctionnalite.archive = true;
    }
    this.fonctionnaliteService.updateFonctionnalite(fonctionnalite).subscribe(
      (response) => {
        alert('La fonctionnalité ' +  response.nom + ' a bien été archivée !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.showDeleteModal = false;
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onModifyPeriodicite(periodiciteForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setPeriodicite.periodicite = periodiciteForm['periodicite'];
    this.fonctionnaliteService.updateFonctionnalite(this.setPeriodicite).subscribe(
      (response) => {
        alert('Périodicité définie à ' +  this.setPeriodicite.periodicite + ' !');
        this.showPeriodiciteModal = false;
        this.periodiciteForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onModifyPriorite(prioriteForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setPriorite.priorite = prioriteForm['priorite'];
    if(this.setPriorite.priorite == 1) { this.priorite = "urgent" }
    else if (this.setPriorite.priorite == 2) { this.priorite = "important" }
    else if (this.setPriorite.priorite == 3) { this.priorite = "standard" }
    else if (this.setPriorite.priorite == 4) { this.priorite = "optionnel" }

    this.fonctionnaliteService.updateFonctionnalite(this.setPriorite).subscribe(
      (response) => {
        alert('Priorité définie à ' +  this.priorite + ' !');

        this.showPrioriteModal = false;
        this.prioriteForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    } else if (modal == 'setPeriodiciteModal'){
      this.periodiciteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, fonctionnalite: any)
  {
    this.editFonctionnalite = fonctionnalite;
    this.showModal = true; // Show-Hide Modal Check
  }

  onDelete(event: any, fonctionnalite: Fonctionnalite) {
    this.deleteFonctionnalite = fonctionnalite;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    } else if(modal == "setPeriodiciteModal") {
      this.showPeriodiciteModal = false;
    } else if(modal == "setPrioriteModal") {
      this.showPrioriteModal = false;
    }
  }

  onSetPeriodicite(event: any, fonctionnalite: Fonctionnalite) {
    this.setPeriodicite = fonctionnalite;
    this.showPeriodiciteModal = true; // Show-Hide Modal Check
  }

  onSetPriorite(event: any, fonctionnalite: Fonctionnalite) {
    this.setPriorite = fonctionnalite;
    this.showPrioriteModal = true; // Show-Hide Modal Check
  }
  // navigateTo(fonctionnalite: Fonctionnalite) {
    // if(fonctionnalite.id) {
      // preparing data before navigate
      // display manager set data
      // this.displayService.setData('fonctionnalite', fonctionnalite);
      // this.displayService.setData('composant', 'dashboard-fonctionnalite');
      // display manager set url with data previously setted in service
      // const url = this.displayService.displayManager();
      // console.log('url');
      // console.log(url);
      // this.router.navigate([`${url}`]
      // ).then();
      // }
    // }


}
