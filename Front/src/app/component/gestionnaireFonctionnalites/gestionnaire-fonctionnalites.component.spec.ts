import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireFonctionnalitesComponent } from './gestionnaire-fonctionnalites.component';

describe('GestionnairePlansComponent', () => {
  let component: GestionnaireFonctionnalitesComponent;
  let fixture: ComponentFixture<GestionnaireFonctionnalitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireFonctionnalitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireFonctionnalitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
