// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {TypeErreur} from "../../model/typeErreur";
import {TypesErreursService} from "../../services/types-erreurs.service";
import {Utilisateur} from "../../model/Utilisateur";
import {UtilisateurService} from "../../services/utilisateur.service";
import {Plan} from "../../model/plan";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";

@Component({
  selector: 'app-gestionnaire-bugs',
  templateUrl: './gestionnaire-bugs.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss']
})
export class GestionnaireBugs implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;

  public bugs!: Bug[];
  public bug!: Bug;
  public editBug!: Bug;
  public deleteBug!: Bug;
  public setPeriodicite!: Bug;
  public setPriorite!: Bug;
  public priorite!: string;
  public typesErreurs!: TypeErreur[];
  public devs!: Utilisateur[];

  // modals
  showDeleteModal!: boolean;
  showPeriodiciteModal!: boolean;
  showPrioriteModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  periodiciteForm!: FormGroup;
  prioriteForm!: FormGroup;
  submitted = false;

  assigneA!: Utilisateur;
  projetId!: string;

  loggedInUser!: Utilisateur;

  projet!: Projet;
  testPlans!: Plan[] | undefined;


  constructor(
    private bugService: BugsService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private typeErreurService: TypesErreursService,
    private utilisateurService: UtilisateurService,
    private bugsService: BugsService,
    private router: Router,
    private projetService: ProjetService
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {

    // this.route.data.subscribe(value => {
     //  this.bugs = value['bugs'];
    // });

    // const url = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname + window.location.search;
    const url = this.router.url
    const splitUrl = url.split('/');

    this.projetId = splitUrl[2];

    this.loggedInUser = this.sessionStorageService.get('loggedInUtilisateur');

    this.projetService.getProjetById(+this.projetId).subscribe(
      (response: Projet) => {
        this.projet = response;

        this.testPlans = this.projet.testPlans;
        // @ts-ignore
        const stringId = this.projetId.toString();

        this.bugService.getBugsByProjetId(stringId).subscribe(
          (response: Bug[]) => {
            this.bugs = response;
          }
        );

      }
    );

    this.typeErreurService.getTypesErreurs().subscribe(
      (response: TypeErreur[]) => {
        this.typesErreurs = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )

    this.utilisateurService.getUtilisateurs().subscribe(
      {
        next:utilisateurs => {
          // @ts-ignore
          this.devs = utilisateurs.filter(utilisateur => utilisateur.role?.id == 3 || utilisateur.role?.id == 2);
        }
      }
    );
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireBugs.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(bug: Bug) {
    this.submitted = true;
    this.editBug = bug;
    if(this.assigneA){
      this.editBug.assigne_a_utilisateur_id = this.assigneA;
    }

    this.bugService.updateBug(this.editBug).subscribe(
      (response: Bug) => {
        alert('Le bug ' +  response.id + ' a bien été mise à jour !');
        window.location.reload();
        this.editForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchiveFonctionnalite(bug: Bug) {
    this.submitted = true;
    // @ts-ignore
    if(bug.archive) {
      bug.archive = false;
    } else if(!bug.archive) {
      bug.archive = true;
    }
    this.bugService.updateBug(bug).subscribe(
      (response) => {
        alert('Le bug ' +  response.id + ' a bien été archivée !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.showDeleteModal = false;
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onModifyPeriodicite(periodiciteForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setPeriodicite.periodicite = periodiciteForm['periodicite'];
    this.bugService.updateBug(this.setPeriodicite).subscribe(
      (response) => {
        alert('Périodicité définie à ' +  this.setPeriodicite + ' !');
        this.showPeriodiciteModal = false;
        this.periodiciteForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    } else if (modal == 'setPeriodiciteModal'){
      this.periodiciteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, bug: any)
  {
    this.editBug = bug;
    this.showModal = true; // Show-Hide Modal Check
  }

  onDelete(event: any, bug: Bug) {
    this.deleteBug = bug;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    } else if(modal == "setPeriodiciteModal") {
      this.showPeriodiciteModal = false;
    } else if(modal == "setPrioriteModal") {
      this.showPrioriteModal = false;
    }
  }

  onSetPeriodicite(event: any, bug: Bug) {
    this.setPeriodicite = bug;
    this.showPeriodiciteModal = true; // Show-Hide Modal Check
  }

  onSetPriorite(event: any, bug: Bug) {
    this.setPriorite = bug;
    this.showPrioriteModal = true; // Show-Hide Modal Check
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2  ? c1.typeErreur.id === c2.typeErreur.id : c1 === c2;
  }

  compare(c3: any, c4: any): boolean {
    return c3 && c4  ? c3.assigneA.id === c4.assigneA.id : c3 === c4;
  }

  // @ts-ignore
  setItem(type: string, objet?: any){
    if(type == "developpeur") {
      this.assigneA = objet;
    }
  }
  // navigateTo(fonctionnalite: Fonctionnalite) {
    // if(fonctionnalite.id) {
      // preparing data before navigate
      // display manager set data
      // this.displayService.setData('fonctionnalite', fonctionnalite);
      // this.displayService.setData('composant', 'dashboard-fonctionnalite');
      // display manager set url with data previously setted in service
      // const url = this.displayService.displayManager();
      // console.log('url');
      // console.log(url);
      // this.router.navigate([`${url}`]
      // ).then();
      // }
    // }


}
