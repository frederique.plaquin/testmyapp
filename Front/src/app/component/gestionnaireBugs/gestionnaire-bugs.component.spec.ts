import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireBugs } from './gestionnaire-bugs.component';

describe('GestionnairePlansComponent', () => {
  let component: GestionnaireBugs;
  let fixture: ComponentFixture<GestionnaireBugs>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireBugs ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireBugs);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
