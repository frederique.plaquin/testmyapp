// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormGroup} from "@angular/forms";
import {DisplayService} from "../../services/display.service";
import {Utilisateur} from "../../model/Utilisateur";
import {Projet} from "../../model/projet";
import {ProjetService} from "../../services/projet.service";
import {UtilisateurService} from "../../services/utilisateur.service";
import {Fonctionnalite} from "../../model/fonctionnalite";
import {Groupe} from "../../model/groupe";
import {Bug} from "../../model/bug";
import {BugsService} from "../../services/bugs.service";
import {Plan} from "../../model/plan";
import {MembreProjetService} from "../../services/membre-projet.service";
import {MembreProjet} from "../../model/membreProjet";
import {projet} from "../../../environments/display";
import {ProjetFull} from "../../model/projetFull";
import {GestionnaireRolesComponent} from "../gestionnaireRoles/gestionnaire-roles.component";
import {RoleService} from "../../services/roleService";
import {Role} from "../../model/role";

@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss', '../../../styles/liste.scss', 'projet.component.scss'],
})
export class ProjetComponent implements OnInit {
  @Input() projet!: Projet;
  public now: Date = new Date(Date.now());
  localStorageChanges$ = this.sessionStorageService.changes$;
  public userId!: string;
  public utilisateur!: Utilisateur;
  public projetsFull!: ProjetFull[];
  public projetFull!: Projet;
  public projetMembres!: Utilisateur[];
  public content!: any;
  public deleteMembre!: Utilisateur;
  public obsolete!: boolean;
  public projetBugs!: Bug[];
  public assignProjet!: Projet;
  public projetDevs: Utilisateur[] = [];
  public bugToAssign!: Bug;
  public projetTestsObsoletes!: Fonctionnalite[];
  public projetTestPlans!: Plan[] | undefined;
  public projetGroupesFonctionnalites!: Groupe[];
  public projetFonctionnalites!: Fonctionnalite[];
  public projetBugsNonAttribues: Bug[] = [];
  public chefsProjet!: Utilisateur[];
  public devsProjet: Utilisateur[] = [];
  public betasProjet!: Utilisateur[];


  // modals
  showDeleteModal!: boolean;
  showModal!: boolean;
  showAssignModal!: boolean;
  showTestsObsoletesModal!: boolean;
  showTestsPlansModal!: boolean;
  showGroupesFonctionnalitesModal!: boolean;
  showfonctionnalitesModal!: boolean;
  showbugsNonAttribuesModal!: boolean;
  showChefsProjetModal!: boolean;
  showDevsProjetModal!: boolean;
  showbetasProjetModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  deleteForm!: FormGroup;
  assignToForm!: FormGroup;
  assignToFormB!: FormGroup;
  submitted = false;
  versionActuelle = [];
  toutesVersions = [];

  // alerte
  alert: boolean = false;

  constructor(
    private projetService: ProjetService,
    private utilisateurService: UtilisateurService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router,
    private bugsService: BugsService,
    private membresService: MembreProjetService,
    private roleService : RoleService
  ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.utilisateur =  this.sessionStorageService.get('loggedInUtilisateur');
    // @ts-ignore
    this.userId = this.utilisateur.id.toString();

    this.projetFull = this.projetService.getProjetDetails(projet);

    if(projet.membresProjets){
      let membresProjet: Utilisateur[] = [];
      for(let membre of projet.membresProjets){
        if(membre.utilisateur_utilisateur_id){
          this.utilisateurService.getUtilisateurById(membre.utilisateur_utilisateur_id).subscribe(
            (response: Utilisateur) => {
              if(projet.id){
                this.projetMembres.push(response);
              }
            }
          )
        }
      }
      projet.membresProjets = membresProjet;
    }


    if(projet.id){
      const toString = projet.id.toString();
      this.bugsService.getBugsByProjetId(toString).subscribe(
        (response: Bug[]) => {
          projet.bugs = response;
          if(projet.bugs){
            for(let bug of projet.bugs){
              if(!bug.archive && !bug.assigne_a_utilisateur_id){
                this.alert = true;
              }
            }
          }
        }
      );
    }
  }

  // count fonctionnalités obsolètes
  testsObsoletes(projet: Projet){
    let fonctionnalites = this.getFonctionnalites(projet);
    let testsObsoletes: Fonctionnalite[] = [];
    for(let fonctionnalite of fonctionnalites) {
      this.obsolete = this.verifyObsolete(fonctionnalite);
      if(this.obsolete) {
        this.alert = true;
        testsObsoletes.push(fonctionnalite);
      }
    }
    return testsObsoletes;
  }

  // @ts-ignore
  getDateButtoire(fonctionnalite: Fonctionnalite){
    if(fonctionnalite.testee){
      const periodicite = fonctionnalite.periodicite;
      const lastTested = fonctionnalite.testee;
      const date = new Date(lastTested);
      // @ts-ignore
      return new Date(date.setMonth(date.getMonth() + periodicite))
    }
  }

  verifyObsolete(fonctionnalite: Fonctionnalite){
    let obsolete = false;
    let now = new Date(Date.now());
    if(fonctionnalite.testee){
      let dateButtoire = this.getDateButtoire(fonctionnalite);
      // @ts-ignore
      if(dateButtoire < now){
        obsolete = true;
      }
    } else {
      if(this.projet.bugs) {
        let fonctionnaliteBugs: Bug[] = [];

        for(let bug of this.projet.bugs) {
          if(bug.id){
            if(bug.fonctionnalite?.id == fonctionnalite.id) {
              fonctionnaliteBugs.push(bug);
            }
          }

          if(fonctionnaliteBugs.length != 0){
            for(let bug of fonctionnaliteBugs){
              obsolete = bug.archive;
            }
          } else {
            obsolete = true;
          }
        }
      }
    }
    return obsolete;
  }

  bugsCritiques(projet: Projet) {
    let projetBugs: Bug[] = [];
    if(projet.bugs) {
      for(let bug of projet.bugs) {
        if(bug.typeErreur?.niveau == 1 && !bug.archive) {
          projetBugs.push(bug);
        }
      }
    }
    return projetBugs;
  }

  getGroupesFonctionnalites(projet: Projet){
    const testplans = projet.testPlans;
    let projetGroupesFonctionnalites: Groupe[] = [];
    if(testplans){
      for(let testplan of testplans){
        if(testplan.groupeFonctionnalites){
          const testplanGroupes: Groupe[]  = testplan.groupeFonctionnalites;
          for(let groupe of testplanGroupes){
            projetGroupesFonctionnalites.push(groupe);
          }
        }
      }
    }
    return  projetGroupesFonctionnalites;
  }

  getFonctionnalites(projet: Projet) {
    const testplans = projet.testPlans;
    let projetFonctionnalites: Fonctionnalite[] = [];
    if(testplans){
      for(let testplan of testplans){
        if(testplan.groupeFonctionnalites){
          const testplanGroupes: Groupe[]  = testplan.groupeFonctionnalites;
          for(let groupe of testplanGroupes){
            if(groupe.fonctionnalites){
              for(let fonctionnalite of groupe.fonctionnalites) {
                projetFonctionnalites.push(fonctionnalite);
              }
            }
          }
        }
      }
    }
    return  projetFonctionnalites;
  }

  getBugsNonAttribues(projet: Projet) {
    let bugs: Bug[] = [];
    if(projet.bugs){
      for(let bug of projet.bugs) {
        if(!bug.assigne_a_utilisateur_id  && !bug.archive){
          bugs.push(bug);
        }
      }
    }
    return bugs
  }

  getChefsProjet(projet: Projet) {
    let chefsProjet: Utilisateur[] = [];
    if(projet.membresProjets){
      for(let membre of projet.membresProjets) {
        let userRole = null;
        console.log(membre)
        this.roleService.getUserRole(membre.utilisateur_utilisateur_id).subscribe(
          (response: Role) => {
            userRole = response;
          }
        )
        if(userRole == 2){
          chefsProjet.push(membre);
        }
      }
    }
    return chefsProjet;
  }

  getDevProjet(projet: Projet) {
    let devs: Utilisateur[] = [];
    if(projet.membresProjets){
      for(let membre of projet.membresProjets) {
        let userRole = null;
        console.log(membre)
        this.roleService.getUserRole(membre.utilisateur_utilisateur_id).subscribe(
          (response: Role) => {
            userRole = response;
          }
        )
        if(userRole == 3 ) {
          devs.push(membre);
        }
      }
    }
    return devs;
  }

  getBetaProjet(projet: Projet) {
    let betas: Utilisateur[] = [];
    if(projet.membresProjets){
      for(let membre of projet.membresProjets) {
        if(membre.role.id == 4) {
          betas.push(membre);
        }
      }
    }
    return betas;
  }

  generateCharts(projet: Projet) {
    // generate charts variables
    // all bugs
    let critiques: Bug[] = [];
    let moderes: Bug[] = [];
    let standards: Bug[] = [];
    let faibles: Bug[] = [];
    if(projet.id){
      if(projet.bugs){
        for(let bug of projet.bugs){
          if(bug.typeErreur?.niveau == 1  && !bug.archive){
            critiques.push(bug);
          } else if(bug.typeErreur?.niveau == 2  && !bug.archive){
            moderes.push(bug);
          } if(bug.typeErreur?.niveau == 3  && !bug.archive){
            standards.push(bug);
          } if(bug.typeErreur?.niveau == 4  && !bug.archive){
            faibles.push(bug);
          }
        }
      }
      this.versionActuelle[projet.id] = [
        // @ts-ignore
        {name: "Critique", value: critiques.length},
        // @ts-ignore
        {name: "Modéré", value: moderes.length},
        // @ts-ignore
        {name: "Standard", value: standards.length},
        // @ts-ignore
        {name: "Faible", value: faibles.length},
      ];

      // all tests
      const fonctionnalites = this.getFonctionnalites(projet);
      const fonctionnalitesNonTestees = this.getFonctionnalitesNonTestees(projet);
      const fonctionnalitesTestees = this.getFonctionnalitesTestees(fonctionnalites);
      // const testsObsoletes = this.testsObsoletes(projet);
      // const fonctionnalitesNTNonObsoletes = fonctionnalitesNonTestees.length - testsObsoletes.length;
      const testsEchec = this.getTestsEchec(projet);
      // @ts-ignore
      const testsValide = this.getTestsValides(fonctionnalitesTestees, projet);

      this.toutesVersions[projet.id] = [
        // @ts-ignore
        {name: "Non executés", value: fonctionnalitesNonTestees.length},
        // @ts-ignore
        // {name: "Obsolètes", value: testsObsoletes.length},
        // @ts-ignore
        {name: "Valides", value: testsValide.length},
        // @ts-ignore
        {name: "Invalides", value: testsEchec?.length}
      ];
    }
  }

  getTestsEchec(projet: Projet) {
    const echecs: Fonctionnalite[] = [];

    if(projet.bugs) {
      for(let bug of projet.bugs) {
        if(!bug.archive) {
          if (bug.fonctionnalite) {
            echecs.push(bug.fonctionnalite);
          }
        }
      }
    }

    return echecs;
  }

  getTestsValides(fonctionnalitesTestees: Fonctionnalite[], projet: Projet) {
    let valides: Fonctionnalite[] = [];
    let echecsFonctionnalite: Fonctionnalite[] = [];
    const echecs = projet.bugs;

    if(echecs?.length) {
      for (let echec of echecs) {
        if(echec.fonctionnalite){
          echecsFonctionnalite.push(echec.fonctionnalite);
        }
      }
      for(let fonctionnalite of fonctionnalitesTestees) {
        const isInArray = echecsFonctionnalite.includes(fonctionnalite);

        if(!isInArray) valides.push(fonctionnalite);
      }

    } else {
      valides = fonctionnalitesTestees;
    }
    return valides;
  }

  getFonctionnalitesNonTestees(projet: Projet){
    this.projetTestsObsoletes = this.testsObsoletes(projet);

    return this.projetTestsObsoletes
  }

  getFonctionnalitesTestees(fonctionnalites: Fonctionnalite[]){
    const testees: Fonctionnalite[] = [];

    if(fonctionnalites) {
      for(let fonctionnalite of fonctionnalites) {
        if(fonctionnalite.testee) {
          testees.push(fonctionnalite);
        }
      }
    }

    return testees
  }

  getMembres(projet: Projet) {
      let membresProjet: Utilisateur[] = [];
      if(projet.id){
        // generate membres
        if(projet.membresProjets){
          for(let membre of projet.membresProjets){
            if(membre.utilisateur_utilisateur_id){
              this.utilisateurService.getUtilisateurById(membre.utilisateur_utilisateur_id).subscribe(
                (response: Utilisateur) => {
                  if(projet.id){
                    membresProjet.push(response);
                  }
                }
              )
            }
          }
        }
      }
      return membresProjet;
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${ProjetComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)
  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, projet: Projet)
  {
    if(projet.bugs){
      this.projetBugs = projet.bugs;
      this.assignProjet = projet;
      this.getDevProjet(projet);
    }
    this.showModal = true; // Show-Hide Modal Check
  }

  onAssignTo(event: any, bug: Bug)
  {
    this.showAssignModal = true; // Show-Hide Modal Check
    this.bugToAssign = bug;
    let projetDevs: MembreProjet[] =  this.membresService.getDevs(this.assignProjet);
    this.getUtilisateurFromMembre(projetDevs);
  }

  // assignToModalB(event: any, bug: Bug) {
  //   this.showAssignModal = true; // Show-Hide Modal Check
  //   this.bugToAssign = bug;
  //   let projetDevs: MembreProjet[] =  this.membresService.getDevs(this.assignProjet);
  //   this.getUtilisateurFromMembre(projetDevs);
  // }

  getUtilisateurFromMembre(membres: MembreProjet[] ) {
    this.projetDevs = [];
    for(let membre of membres){
      if(membre.id){
        this.utilisateurService.getUtilisateurById(membre.id).subscribe(
          (response: Utilisateur) => {
            this.projetDevs.push(response);
          }
        )
      }
    }
  }

  assignTo(assignForm: FormGroup){
    this.submitted = true;
    // @ts-ignore
    this.bugToAssign.assigne_a_utilisateur_id = assignForm['assignToDev'];
    this.bugsService.updateBug(this.bugToAssign).subscribe(
      (response: Bug) => {
        alert('Bug assigné à '+ response.assigne_a_utilisateur_id?.prenom + ' ' + response.assigne_a_utilisateur_id?.nom);
        this.showAssignModal = false;
        this.showModal = false;
        this.assignToForm.reset();
      }
    );
  }

  assignToB(assignForm: FormGroup){
    this.submitted = true;
    // @ts-ignore
    this.bugToAssign.assigne_a_utilisateur_id = assignForm['assignToDev'];
    this.bugsService.updateBug(this.bugToAssign).subscribe(
      (response: Bug) => {
        alert('Bug assigné à '+ response.assigne_a_utilisateur_id?.prenom + ' ' + response.assigne_a_utilisateur_id?.nom);
        this.showbugsNonAttribuesModal = false;
        this.showAssignModal = false;
        this.assignToForm.reset();
      }
    );
  }


  onShowTestsObsoletes(event: any, projet: Projet){
    this.projetTestsObsoletes = this.testsObsoletes(projet);
    this.showTestsObsoletesModal = true;
  }

  onShowTestsPlans(event: any, projet: Projet) {
    if(projet.testPlans){
      this.projetTestPlans = projet.testPlans;
    }
    this.showTestsPlansModal = true;
  }

  onShowGroupesFonctionnalites(event: any, projet: Projet) {
    this.projetGroupesFonctionnalites = this.getGroupesFonctionnalites(projet);
    this.showGroupesFonctionnalitesModal = true;
  }

  onShowFonctionnalites(event: any, projet: Projet) {
    this.projetFonctionnalites = this.getFonctionnalites(projet);
    this.showfonctionnalitesModal = true;
  }

  onShowBugsNonAttribues(event: any, projet: Projet) {
    this.assignProjet = projet;
    if(projet.bugs) {
      this.projetBugsNonAttribues = this.getBugsNonAttribues(projet);
    }
    this.showbugsNonAttribuesModal = true;
  }

  onShowChefsProjet(event: any, projet: Projet) {
    if(projet.membresProjets) {
      const chefsProjet: Utilisateur[] = [];
      for(let membre of projet.membresProjets) {
        if(membre.role.id == 2) {
          chefsProjet.push(membre);
        }
      }
      this.chefsProjet = chefsProjet;
    }
    this.showChefsProjetModal = true;
  }

  onShowDevsProjet(event: any, projet: Projet) {
    if(projet.membresProjets) {
      const devsProjet: Utilisateur[] = [];
      for(let membre of projet.membresProjets) {
        if(membre.role.id == 3 || membre.role.id == 2) {
          devsProjet.push(membre);
        }
      }
      this.devsProjet = devsProjet;
    }
    this.showDevsProjetModal = true;
  }

  onShowBetasProjet(event: any, projet: Projet) {
    if(projet.membresProjets) {
      const betasProjet: Utilisateur[] = [];
      for(let membre of projet.membresProjets) {
        if(membre.role.id == 4) {
          betasProjet.push(membre);
        }
      }
      this.betasProjet = betasProjet;
    }
    this.showbetasProjetModal = true;
  }


  onDelete(event: any, membre: Utilisateur) {
    this.deleteMembre = membre;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    } else if(modal == 'assignToModal') {
      this.showAssignModal = false;
    } else if(modal == 'testsObsoletes') {
      this.showTestsObsoletesModal =false;
    } else if(modal == 'testsPlans') {
      this.showTestsPlansModal = false;
    } else if(modal == 'groupesFonctionnalites') {
      this.showGroupesFonctionnalitesModal = false;
    } else if(modal == 'fonctionnalites') {
      this.showfonctionnalitesModal = false;
    } else if ( modal == 'bugsNonAttribuesModal') {
      this.showbugsNonAttribuesModal = false;
    } else if (modal == 'showChefsProjetModal') {
      this.showChefsProjetModal = false;
    } else if(modal == 'showDevsProjetModal') {
      this.showDevsProjetModal = false;
    } else if(modal == 'showbetasProjetModal') {
      this.showbetasProjetModal = false;
    }
  }

  onRedirect() {
    this.router.navigate([`gestionnaire-de-fonctionnalites/${this.userId}`])
  }

  navigateTo(type: string, objet: any) {
    if(objet.id) {
      if(type == 'projet') {
        // preparing data before navigate
        // display manager set data
        this.displayService.setData('projet', objet);
        this.displayService.setData('composant', 'dashboard-projet');
        // display manager set url with data previously setted in service
        const url = this.displayService.displayManager();
        this.router.navigate([`${url}`]).then();
      } else if(type == 'groupe'){
        this.router.navigate([`/dashboard-groupe/${objet.groupe_fonctionnalites_groupe_id}`]).then();
      } else if(type == 'testplans') {
        this.router.navigate([`/dashboard-plan/${objet.id}`]);
      } else if(type == 'groupeFonctionnalite'){
        this.router.navigate([`/dashboard-groupe/${objet.id}`]).then();
      } else if(type == 'bug') {
        this.router.navigate([`/gestionnaire-de-bugs/${objet.projet_projet_id.id}`]).then();
      }
    }
  }
}
