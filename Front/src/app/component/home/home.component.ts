import { HttpErrorResponse } from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserServiceService } from 'src/app/services/user-service.service';
import {Subscription} from "rxjs";
import {DataService} from "../../services/data.service";
import {Utilisateur} from "../../model/Utilisateur";
import {SessionStorageService} from "../../services/session-storage.service";
import {Display} from "../../model/display";
import {DisplayService} from "../../services/display.service";
import {User} from "../../model/user";
import {Password} from "../../model/password";
import {EncrDecrService} from "../../services/EncrDecrService ";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  display!: Display;
  userDisplay!: User;
  utilisateur!: JSON;
  subscription!: Subscription;

  registerForm!: FormGroup;
  submitted:boolean=false;
  user!:Utilisateur;
  infoMessage : string="";
  isValid:boolean=false;


  constructor(private formBuilder : FormBuilder,
              private userService : UserServiceService,
              private router : Router,
              private data: DataService,
              private displayService: DisplayService,
              private sessionStorageService: SessionStorageService,
              private EncrDecr: EncrDecrService) {  }

  ngOnInit(): void {
    this.initForm();
    this.subscription = this.data.currentProjet.subscribe(utilisateur => this.utilisateur = utilisateur)

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setUtilisateur(utilisateur: Utilisateur) {
    this.data.changeUtilisateur(utilisateur);
  }

  persist(key: string, value: any) {
    this.sessionStorageService.set(key, value);
  }

  initForm(): any{
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  get email(){ return this.registerForm.get("email");}
  get password(){ return this.registerForm.get("password"); }

//------------------------------------------------------------------
  onSubmit(){
    this.submitted=true;
    if(this.registerForm.invalid){
      return;
    }
    // hash password
    const hash = this.EncrDecr.set('123456$#@$^@1ERF', this.registerForm.value.password);

    console.log(hash)

    // console.log("hash : " + hash);
    // console.log("mdp : " + this.EncrDecr.get('123456$#@$^@1ERF', hash));

    const password: Password = {
      hash: hash
    }

   let auth={
    "mail": this.registerForm.value.email ,
     "password":password
   };

    this.persist('auth', auth);

    this.userService.login(auth).subscribe({
      next: (response: any )=> {
        this.setUtilisateur(response);
        this.userDisplay = {
          'id': response.id,
          'nom': response.nom,
          'prenom': response.prenom,
          'imageUrl': response.imageUrl,
          'role': response.role.role
        }
        this.user=response;

        //redirection vers la page de dahsborad admin
        let jwt = "Bearer "+this.user.token;
        localStorage.setItem("token",jwt);
        this.userService.islogin=true;

        // display manager set data
        this.displayService.setData('user', this.userDisplay);
        this.displayService.setData('composant', 'dashboard');
        // display manager set url with data previously setted in service
        const url = this.displayService.displayManager();
        this.router.navigate([`${url}`], {
        }).then();
    this.persist('auth', auth);

   this.userService.login(auth).subscribe({
     next: (response: any )=> {
       this.setUtilisateur(response);
        this.userDisplay = {
         'id': response.id,
         'nom': response.nom,
         'prenom': response.prenom,
         'imageUrl': response.imageUrl,
         'role': response.role
       }
       this.user=response;
       //redirection vers la page de dahsborad admin
       let jwt = "Bearer "+this.user.token;
       localStorage.setItem("token",jwt);
       this.userService.islogin=true;

       // display manager set data
       this.displayService.setData('user', this.userDisplay);
       this.displayService.setData('composant', 'dashboard');
       // display manager set url with data previously setted in service
       const url = this.displayService.displayManager();
       this.router.navigate([`${url}`], {
       }).then();

      },
      error:(error : HttpErrorResponse) => {
        //Bad credentials : mean that email or password is not correct
        //User is disabled : mean not confirmed by the admin yet
        // console.log(error.error.trace);
        this.isValid=true;

        if(error.error.trace.search("User is disabled")==-1){
          //  console.log("Votre email ou mot de passe est erroné");
          this.infoMessage="Votre email ou mot de passe est erroné";

        }else{
          //  console.log("votre demande en attente");
          this.infoMessage=("votre demande est en attente");
        }

      }
    })
     },
     error:(error : HttpErrorResponse) => {
        //Bad credentials : mean that email or password is not correct
        //User is disabled : mean not confirmed by the admin yet
        // console.log(error.error.trace);
        this.isValid=true;

        if(error.error.trace.search("User is disabled")==-1){
        //  console.log("Votre email ou mot de passe est erroné");
        this.infoMessage="Votre email ou mot de passe est erroné";

      } else{
        //  console.log("votre demande en attente");
        this.infoMessage=("votre demande est en attente");
      }

      }
   })

  }
  //----------------------------------------------------------------------
  onClearMessage(){
    this.infoMessage="";
    this.isValid=false;
  }
}
