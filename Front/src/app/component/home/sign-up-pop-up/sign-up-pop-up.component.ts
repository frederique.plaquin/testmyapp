import { Component, OnInit } from '@angular/core';
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MustMatch} from "./providers/must-match.validators";
import { UserServiceService } from 'src/app/services/user-service.service';
import { HttpErrorResponse } from '@angular/common/http';
import {UtilisateurService} from "../../../services/utilisateur.service";
import {Password} from "../../../model/password";
import {PasswordService} from "../../../services/password.service";
import {EncrDecrService} from "../../../services/EncrDecrService ";

@Component({
  selector: 'app-sign-up-pop-up',
  templateUrl: './sign-up-pop-up.component.html',
  styleUrls: ['./sign-up-pop-up.component.scss']
})
export class SignUpPopUpComponent implements OnInit {
  closeResult!: string;
  modalOptions:NgbModalOptions;

  isValid:boolean=false;
  infoMessage:string="";

  registerForm!: FormGroup;
  submitted = false;

  constructor(
    // popup(modal)
    private userService:UserServiceService,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private utilisateurService: UtilisateurService,
    private passwordService: PasswordService,
    private EncrDecr: EncrDecrService
  ){
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  // initialisation formulaire
  initForm(): any{
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  // popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${SignUpPopUpComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  // envoi du formulaire
//##############################################################
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      this.infoMessage="vous devez vérifier et remplir tous les champs !"
      this.isValid=false;
      return;
    }
    // hash password
    const hash = this.EncrDecr.set('123456$#@$^@1ERF', this.registerForm.value.password);

    // console.log("hash : " + hash);
    // console.log("mdp : " + this.EncrDecr.get('123456$#@$^@1ERF', hash));

    const password: Password = {
      hash: hash
    }

    // this.passwordService.addPassword(password).subscribe(
    //  (response: Password) => {
    //    console.log(response);
    //  }
    // )

    // set new Utilisateur
    /* const newUtilisateur: Utilisateur = {
      nom: this.registerForm.value.lastName,
      prenom: this.registerForm.value.firstName,
      mail: this.registerForm.value.email,
      password: password,
      token:""
    }*/

    //object to store in database
    let user={
      "nom":this.registerForm.value.firstName,
      "prenom":this.registerForm.value.lastName,
      "mail":this.registerForm.value.email,
      "imageUrl":"",
      "confirmed":false,
      "password": password,
      "role":{
        id: 3,
        role: 'dev'
      }
    };

    //console.log(user);
    // display form values on success
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));

    //console.log(user);

      this.userService.createUtilisateur(user)
      .subscribe({
        next:(response: Object) =>{
          this.isValid=true;
          this.infoMessage="Votre demande est enregistrée, l'administrateur va l'examiner. Merci";
          this.onReset();
        },
        error:(error: HttpErrorResponse)=>{
          //alert("Email already taken");
          console.log(error.message)
          this.isValid=false;
          this.infoMessage="Il y a un erreur ou cette adresse mail est déja pris"
      }
      });
  }
  //############################################################################################################

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  onClearMessage(){
    this.infoMessage="";
  }

}
