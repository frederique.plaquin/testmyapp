import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpDeconnecterComponent } from './pop-up-deconnecter.component';

describe('PopUpDeconnecterComponent', () => {
  let component: PopUpDeconnecterComponent;
  let fixture: ComponentFixture<PopUpDeconnecterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopUpDeconnecterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopUpDeconnecterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
