// source : https://www.devfaq.fr/question/comment-remplir-une-valeur-pour-formarray-agrave-laide-de-la-m-eacute-thode-de-contr-ocirc-le-set
import {Component, OnInit} from '@angular/core';
import { Plan } from "../../model/plan";
import {ActivatedRoute, Router} from "@angular/router";
import {SessionStorageService} from "../../services/session-storage.service";
import {ModalDismissReasons, NgbModal, NgbModalOptions} from "@ng-bootstrap/ng-bootstrap";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";
import {DisplayService} from "../../services/display.service";
import {Groupe} from "../../model/groupe";
import {GroupeFonctionnaliteService} from "../../services/groupe-fonctionnalite.service";

@Component({
  selector: 'app-gestionnaire-groupes-fonctionnalites',
  templateUrl: './gestionnaire-groupe-fonctionnalites.component.html',
  styleUrls: ['../../../styles/gestionnaires.scss']
})
export class GestionnaireGroupeFonctionnalitesComponent implements OnInit {
  localStorageChanges$ = this.sessionStorageService.changes$;
  public plan!: Plan;
  public groupes!:any;
  public groupe!: Groupe;
  public editGroupe!: Groupe;
  public deleteGroupe!: Groupe;
  public confirmGroupe!: Groupe;
  public setPeriodicite!: Groupe;
  public unDraftGroupe!: Groupe;


  // modals
  showDeleteModal!: boolean;
  showConfirmModal!: boolean;
  showPeriodiciteModal!: boolean;
  showActivateModal!: boolean;
  showModal!: boolean;
  closeResult!: string;
  modalOptions!:NgbModalOptions;
  isValid:boolean=false;
  public infoMessage:string="";
  editForm!: FormGroup;
  deleteForm!: FormGroup;
  periodiciteForm!: FormGroup;
  undraftForm!: FormGroup;
  submitted = false;


  form=new FormGroup({
    choix: new FormControl,
    periodicite: new FormControl,
  })

  constructor(
    private groupeFonctionnalitesService: GroupeFonctionnaliteService,
    private route: ActivatedRoute,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private displayService: DisplayService,
    private router: Router
    ) {
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop'
    }
  }

  ngOnInit(): void {
    this.route.data.subscribe(value=>{
      this.plan = value['plan'];
      this.groupes = this.plan.groupeFonctionnalites;
    });
  }

// popup(modal)
  open(content: any) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${GestionnaireGroupeFonctionnalitesComponent.getDismissReason(reason)}`;
    });
  }

  openVerticallyCentered(content: any) {
    this.modalService.open(content, { centered: true });
  }

  private static getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  // END popup(modal)


  // envoi du formulaire
  //##############################################################
  onSubmit(groupe: Groupe) {
    this.submitted = true;
    this.groupeFonctionnalitesService.updateGroupe(groupe).subscribe(
      (response: Groupe) => {
        alert('Le groupe de fonctionnalités ' +  response.nom + ' a bien été mis à jour !');
        window.location.reload();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.editForm.reset();
      }
    );
    this.showModal = false; // Show-Hide Modal Check
  }

  onArchivePlan(groupe: Groupe) {
    this.submitted = true;
    // @ts-ignore
    if(groupe.archive) {
      groupe.archive = false;
    } else if(!groupe.archive) {
      groupe.archive = true;
    }
    this.groupeFonctionnalitesService.updateGroupe(groupe).subscribe(
      (response) => {
        alert('Le groupe de fonctionnalités ' +  response.nom + ' a bien été archivé !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.deleteForm.reset();
      }
    );
    this.showDeleteModal = false; // Show-Hide Modal Check
  }

  onModifyPeriodicite(periodiciteForm: FormGroup) {
    this.submitted = true;
    // @ts-ignore
    this.setPeriodicite.periodicite = periodiciteForm['periodicite'];
    this.groupeFonctionnalitesService.updateGroupe(this.setPeriodicite).subscribe(
      (response) => {
        alert('Périodicité définie à ' +  this.setPeriodicite.periodicite + ' !');
        this.showPeriodiciteModal = false;
        this.periodiciteForm.reset();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
      }
    );
  }

  onUndraft(groupe: Groupe) {
    this.submitted = true;
    // @ts-ignore
    if(groupe.draft) {
      groupe.draft = false;
    } else if(!groupe.draft) {
      groupe.draft = true;
    }
    this.groupeFonctionnalitesService.updateGroupe(groupe).subscribe(
      (response) => {
        alert('Le groupe de fonctionnalités ' +  response.nom + ' a bien été activé !');
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        this.undraftForm.reset();
      }
    );
    this.showActivateModal = false; // Show-Hide Modal Check
  }

  onReset(modal: string) {
    this.submitted = false;
    if(modal == 'editModal'){
      this.editForm.reset();
    } else if(modal == 'deleteModal') {
      this.deleteForm.reset();
    }
  }

  onClearMessage(){
    this.infoMessage="";
  }

  onClick(event: any, plan: any)
  {
    this.editGroupe = plan;
    this.showModal = true; // Show-Hide Modal Check
  }

  onDelete(event: any, groupe: Groupe) {
    this.deleteGroupe = groupe;
    this.showDeleteModal = true; // Show-Hide Modal Check
  }

  onConfirm(event: any, groupe: Groupe) {
    this.unDraftGroupe = groupe;
    this.showActivateModal = true; // Show-Hide Modal Check
  }

  onSetPeriodicite(event: any, groupe: Groupe) {
    this.setPeriodicite = groupe;
    this.showPeriodiciteModal = true; // Show-Hide Modal Check
  }

  //Bootstrap Modal Close event
  hide(modal: string)
  {
    this.showModal = false;
    if(modal == 'editModal'){
      this.showModal = false;
    } else if(modal == 'deleteModal') {
      this.showDeleteModal = false;
    } else if(modal == 'confirmModal') {
      this.showConfirmModal = false;
    } else if(modal == "setPeriodiciteModal") {
      this.showPeriodiciteModal = false;
    }
  }

  navigateTo(groupe: Groupe) {
    if(groupe.id) {
      // preparing data before navigate
      // display manager set data
      this.displayService.setData('groupe', groupe);
      this.displayService.setData('composant', 'dashboard-groupe');
      // display manager set url with data previously setted in service
      const url = this.displayService.displayManager();
      this.router.navigate([`${url}`]
      ).then();
    }
  }
  //------------------------------------------------------------------------------
  search(key:string){

    const results:Groupe[]=[];
    for(let groupe of this.groupes ){
      if(groupe.nom.toLocaleLowerCase().indexOf(key.toLocaleLowerCase() )!== -1){
        results.push(groupe)
      }
    }
    this.groupes=results;

    if(results.length==0 || !key){
      this.route.data.subscribe(value=>{
        this.plan = value['plan'];
        this.groupes = this.plan.groupeFonctionnalites;
      });
    }
  }


//--------------------------------------------------------------------------------
submit(){

  this.route.data.subscribe(value => {
    this.plan = value['plan'];
    this.groupes = this.plan.groupeFonctionnalites;
  });

  const results:Groupe[]=[];
  console.log(this.form.value.choix);
//------------------------------------------------------------------------------------------------------------
  if((this.form.value.periodicite==null || this.form.value.periodicite==0 ) && (this.form.value.choix==null || this.form.value.choix=="aucun" )  ){

    this.route.data.subscribe(value => {
      this.plan = value['plan'];
      this.groupes = this.plan.groupeFonctionnalites;
    });

  }
//-------------------------------------------------------------------------------------------------------------
  else if ((this.form.value.periodicite==null || this.form.value.periodicite==0 ) && this.form.value.choix!=null ){
    const results:Groupe[]=[];
  if(this.form.value.choix=="brouillon"){
    for(let groupe of this.groupes){
      if(groupe.draft===true){
        results.push(groupe);
      }
    }
    this.groupes=results;
  } else if(this.form.value.choix=="actif"){
      for(let groupe of this.groupes){
        if(groupe.draft===false && groupe.archive===false){
          results.push(groupe);
        }
    }
    this.groupes=results;

  } else if(this.form.value.choix=="inactif"){
    for(let groupe of this.groupes){
      if(groupe.draft===false && groupe.archive===true){
        results.push(groupe);
      }
  }
  this.groupes=results;
}
  else{
    this.route.data.subscribe(value => {
      this.plan = value['plan'];
      this.groupes = this.plan.groupeFonctionnalites;
    });
  }
}
//--------------------------------------------------------------------------------------------------------------
else if ((this.form.value.periodicite!=null && this.form.value.periodicite!=0 ) && (this.form.value.choix==null || this.form.value.choix=="aucun" ) ){
  const results:Groupe[]=[];
  console.log("here");

  for(let groupe of this.groupes){
   console.log( groupe.periodicite.charAt(0));
    if(Number(groupe.periodicite.charAt(0))==this.form.value.periodicite){
      results.push(groupe);
      this.groupes=results;
    }

}
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
else{
  const results:Groupe[]=[];
  if(this.form.value.choix=="brouillon"){
    for(let groupe of this.groupes){
      if(groupe.draft===true && (Number(groupe.periodicite.charAt(0))==this.form.value.periodicite) ){
        results.push(groupe);
      }
    }
    this.groupes=results;
  }

    else if(this.form.value.choix=="actif"){
      console.log("this is actif")
      for(let groupe of this.groupes){
        if(groupe.draft===false && groupe.archive===false && (Number(groupe.periodicite.charAt(0))==this.form.value.periodicite)){
          results.push(groupe);
        }
    }
    this.groupes=results;

  }else if(this.form.value.choix=="inactif"){
    const results:Groupe[]=[];
    console.log("this is NOt actif")
    for(let groupe of this.groupes){
      if(groupe.draft===false && groupe.archive===true && (Number(groupe.periodicite.charAt(0))==this.form.value.periodicite)){
        results.push(groupe);
      }
  }
  this.groupes=results;
}
  else{
    this.route.data.subscribe(value => {
      this.plan = value['plan'];
      this.groupes = this.plan.groupeFonctionnalites;
    });
  }

}

}




}
