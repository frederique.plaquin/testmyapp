import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionnaireGroupeFonctionnalitesComponent } from './gestionnaire-groupe-fonctionnalites.component';

describe('GestionnairePlansComponent', () => {
  let component: GestionnaireGroupeFonctionnalitesComponent;
  let fixture: ComponentFixture<GestionnaireGroupeFonctionnalitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionnaireGroupeFonctionnalitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionnaireGroupeFonctionnalitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
