import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { PopupModifMembreComponent } from './component/gestionnaireRoles/popup-modif-membre/popup-modif-membre.component';
import { HeaderComponent } from './component/header/header.component';
import { AjoutMembrePopUpComponent, AjoutMembrePopUpComponentConfirm } from './component/ajout-membre-pop-up/ajout-membre-pop-up.component';
import { SignUpPopUpComponent } from './component/home/sign-up-pop-up/sign-up-pop-up.component';
import { NgbDatepickerModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule} from "@angular/router";
import { UtilisateurService } from "./services/utilisateur.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { EncrDecrService } from "./services/EncrDecrService ";
import { GestionnairePlansComponent } from './component/gestionnairePlans/gestionnaire-plans.component';
import { AddProjetPopupComponent } from './component/add-projet-popup/add-projet-popup.component';
import { TableauProjetsComponent } from './component/tableau-projets/tableau-projets.component';
import { TableauContextuelComponent } from './component/tableau-contextuel/tableau-contextuel.component';
import { AddPlanPopupComponent } from "./component/add-plan-popup/add-plan-popup.component";
import { TableauPlansComponent } from './component/tableau-plans/tableau-plans.component';
import { DashboardComponent } from './component/dashboard/dashboard/dashboard.component';
import { GestionnaireRolesComponent } from './component/gestionnaireRoles/gestionnaire-roles.component';
import { TableauUtilisateursComponent } from './component/tableau-utilisateurs/tableau-utilisateurs.component';
import { AddUtilisateurPopupComponent } from './component/add-utilisateur-popup/add-utilisateur-popup.component';
import { ProjetService } from "./services/projet.service";
import { PopUpDeconnecterComponent } from './component/home/pop-up-deconnecter/pop-up-deconnecter.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { AddProjetPopupSidebarComponent } from "./component/sidebar/add-projet-popup-sidebar/add-projet-popup-sidebar.component";
import { AddPlanPopupSidebarComponent } from "./component/sidebar/add-plan-popup-sidebar/add-plan-popup-sidebar.component";
import { GestionnaireGroupeFonctionnalitesComponent } from "./component/gestionnaireGroupesFonctionnaltites/gestionnaire-groupe-fonctionnalites.component";
import { TableauGroupesFonctionnalitesComponent } from "./component/tableau-groupes-fonctionnalites/tableau-groupes-fonctionnalites.component";
import { AddGroupeFonctionnalitesPopupComponent } from "./component/add-groupe-fonctionnalites-popup/add-groupe-fonctionnalites-popup.component";
import { PlanService } from "./services/plan.service";
import { GroupeFonctionnaliteService } from "./services/groupe-fonctionnalite.service";
import { GestionnaireFonctionnalitesComponent } from "./component/gestionnaireFonctionnalites/gestionnaire-fonctionnalites.component";
import { ListeFonctionnalitesComponent } from "./component/listeFonctionnalites/liste-fonctionnalites.component";
import { AddFonctionnalitePopupComponent } from "./component/add-fonctionnalite-popup/add-fonctionnalite-popup.component";
import { GestionnaireTypesErreursComponent } from "./component/gestionnaireTypesErreurs/gestionnaire-types-erreurs.component";
import { AddTypeErreurPopupComponent } from "./component/add-type-erreur-popup/add-type-erreur-popup.component";
import { TypesErreursService } from "./services/types-erreurs.service";
import { ColorPickerModule } from "ngx-color-picker";
import { ColorPickerComponent } from './component/color-picker/color-picker.component';
import { TableauTypesErreursComponent } from "./component/tableau-types-erreurs/tableau-types-erreurs.component";
import { GestionnaireBugs } from "./component/gestionnaireBugs/gestionnaire-bugs.component";
import { FilterRechercheComponent } from './component/filter-recherche/filter-recherche.component';
import { AppRoutingModule } from "./route/app-routing.module";
import { ListeProjetsComponent } from "./component/listeProjets/liste-projets.component";
import { BugsService } from "./services/bugs.service";
import { DisplayService } from "./services/display.service";
import { FonctionnaliteService } from "./services/fonctionnalite.service";
import { AddToFavoritesPopupComponent } from "./component/add-to-favorites-popup/add-to-favorites-popup.component";
import { AddBugPopupComponent } from "./component/add-bug-popup/add-bug-popup.component";
import { TableauBugsRepertoriesComponent } from "./component/tableau-bugs-repertories/tableau-bugs-repertories.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ClickStopPropagation } from "./Directives/ClickStopPropagation";
import { BugsActifsComponent } from './component/bugs-actifs/bugs-actifs.component';
import { MesBugsAResoudreComponent } from './component/mes-bugs-a-resoudre/mes-bugs-aresoudre.component';
import { DerniersBugsRepertoriesComponent } from './component/derniers-bugs-repertories/derniers-bugs-repertories.component';
import { BugDetailsPopUpComponent } from "./component/bug-details-pop-up/bug-details-pop-up.component";
import { CleanPopUpComponent } from "./component/clean-pop-up/clean-pop-up.component";
import { TestsActifsComponent } from "./component/tests-actifs/tests-actifs.component";
import { TestsAEffectuerComponent } from "./component/tests-a-effectuer/tests-a-effectuer.component";
import { DerniersTestsValidesComponent } from "./component/derniers-tests-valides/derniers-tests-valides.component";
import { DerniersTestsCreesComponent } from "./component/derniers-tests-crees/derniers-tests-crees.component";
import { ProjetComponent } from "./component/projet/projet.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignUpPopUpComponent,
    AjoutMembrePopUpComponent,
    AjoutMembrePopUpComponentConfirm,
    SignUpPopUpComponent,
    HeaderComponent,
    PopupModifMembreComponent,
    SignUpPopUpComponent,
    HomeComponent,
    SignUpPopUpComponent,
    GestionnairePlansComponent,
    AddPlanPopupComponent,
    AddProjetPopupComponent,
    TableauProjetsComponent,
    TableauContextuelComponent,
    TableauPlansComponent,
    DashboardComponent,
    GestionnaireRolesComponent,
    TableauUtilisateursComponent,
    AddUtilisateurPopupComponent,
    TableauTypesErreursComponent,
    GestionnaireTypesErreursComponent,
    DashboardComponent,
    PopUpDeconnecterComponent,
    SidebarComponent,
    AddProjetPopupSidebarComponent,
    AddPlanPopupSidebarComponent,
    GestionnaireGroupeFonctionnalitesComponent,
    TableauGroupesFonctionnalitesComponent,
    AddGroupeFonctionnalitesPopupComponent,
    GestionnaireFonctionnalitesComponent,
    ListeFonctionnalitesComponent,
    AddFonctionnalitePopupComponent,
    GestionnaireTypesErreursComponent,
    AddTypeErreurPopupComponent,
    ColorPickerComponent,
    TableauTypesErreursComponent,
    FilterRechercheComponent,
    TableauTypesErreursComponent,
    GestionnaireBugs,
    ListeProjetsComponent,
    AddToFavoritesPopupComponent,
    AddBugPopupComponent,
    TableauBugsRepertoriesComponent,
    ClickStopPropagation,
    BugsActifsComponent,
    MesBugsAResoudreComponent,
    DerniersBugsRepertoriesComponent,
    BugDetailsPopUpComponent,
    CleanPopUpComponent,
    TestsActifsComponent,
    TestsAEffectuerComponent,
    DerniersTestsValidesComponent,
    DerniersTestsCreesComponent,
    ProjetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ColorPickerModule,
    NgxChartsModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    UtilisateurService,
    HttpClient,
    EncrDecrService,
    ProjetService,
    PlanService,
    GroupeFonctionnaliteService,
    TypesErreursService,
    BugsService,
    DisplayService,
    FonctionnaliteService
    ],
  bootstrap: [AppComponent]
})
// @ts-ignore
export class AppModule { }
