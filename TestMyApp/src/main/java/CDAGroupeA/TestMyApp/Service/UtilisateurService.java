package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Repository.UtilisateurRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

@Service
public class UtilisateurService {
    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurService(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    public Utilisateur addUtilisateur(Utilisateur utilisateur) {return utilisateurRepository.save(utilisateur);}

    public List<Utilisateur> findAllUtilisateurs() {
        return utilisateurRepository.findAll();
    }

    public Utilisateur updateUtilisateur(Utilisateur utilisateur) {
        return utilisateurRepository.save(utilisateur);
    }

    public Utilisateur findUtilisateurById(Long id) {
        return utilisateurRepository.findUtilisateurById(id)
                .orElseThrow(() -> new UserNotFoundException("L'utilisateur " + id + " n'a pas été trouvé !"));
    }

    @Transactional
    public void deleteUtilisateur(Long id) {
        utilisateurRepository.deleteUtilisateurById(id);
    }

}
