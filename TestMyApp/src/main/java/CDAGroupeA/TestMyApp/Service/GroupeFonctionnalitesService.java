package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import CDAGroupeA.TestMyApp.Modele.TestPlan;
import CDAGroupeA.TestMyApp.Repository.GroupeFonctionnalitesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class GroupeFonctionnalitesService {
    private final GroupeFonctionnalitesRepository groupeFonctionnalitesRepository;

    @Autowired
    public GroupeFonctionnalitesService(GroupeFonctionnalitesRepository groupeFonctionnalitesRepository) {
        this.groupeFonctionnalitesRepository = groupeFonctionnalitesRepository;
    }

    public GroupeFonctionnalites addGroupe(GroupeFonctionnalites groupeFonctionnalites) {
        return this.groupeFonctionnalitesRepository.save(groupeFonctionnalites);
    }

    public List<GroupeFonctionnalites> findAllGroupes() { return this.groupeFonctionnalitesRepository.findAll(); }

    public GroupeFonctionnalites findGroupeById(Long id) {
        return groupeFonctionnalitesRepository.findGroupeFonctionnalitesById(id)
                .orElseThrow(() -> new UserNotFoundException("Le groupe " + id + " n'a pas été trouvé!"));
    }

    @Modifying
    public GroupeFonctionnalites updateGroupeFonctionnalites(GroupeFonctionnalites groupeFonctionnalites) {
        return groupeFonctionnalitesRepository.save(groupeFonctionnalites);
    }

    @Transactional
    public void deleteGroupe(Long id) {
        groupeFonctionnalitesRepository.deleteGroupeFonctionnalitesById(id);
    }
}
