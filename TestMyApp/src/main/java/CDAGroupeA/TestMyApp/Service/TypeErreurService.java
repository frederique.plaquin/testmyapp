package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.TypeErreur;
import CDAGroupeA.TestMyApp.Repository.TypeErreurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TypeErreurService {
    private final TypeErreurRepository typeErreurRepository;

    @Autowired
    public TypeErreurService(TypeErreurRepository typeErreurRepository) {
        this.typeErreurRepository = typeErreurRepository;
    }

    public TypeErreur addTypeErreur(TypeErreur typeErreur) {
        return this.typeErreurRepository.save(typeErreur);
    }

    public List<TypeErreur> findAllTypesErreurs() { return this.typeErreurRepository.findAll(); }

    public TypeErreur findTypeErreurById(Long id) {
        return typeErreurRepository.findTypeErreurById(id)
                .orElseThrow(() -> new UserNotFoundException("Le type d'erreur " + id + " n'a pas été trouvée!"));
    }

    @Modifying
    public TypeErreur updateTypeErreur(TypeErreur typeErreur) {
        return typeErreurRepository.save(typeErreur);
    }

    @Transactional
    public void deleteTypeErreur(Long id) { typeErreurRepository.deleteTypeErreurByid(id); }
}
