package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.TestPlan;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Repository.TestPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TestPlanService {
    private final TestPlanRepository testPlanRepository;

    @Autowired
    public TestPlanService(TestPlanRepository testPlanRepository) {
        this.testPlanRepository = testPlanRepository;
    }

    public TestPlan addTestPlan(TestPlan testPlan) {
        return this.testPlanRepository.save(testPlan);
    }

    public List<TestPlan> findAllPlans() { return this.testPlanRepository.findAll(); }

    public TestPlan findTestPlanById(Long id) {
        return testPlanRepository.findTestPlanById(id)
                .orElseThrow(() -> new UserNotFoundException("Le plan " + id + " n'a pas été trouvé!"));
    }

    public TestPlan updateTestPlan(TestPlan testPlan) {
        return testPlanRepository.save(testPlan);
    }

    @Transactional
    public void deleteTestPlan(Long id) {
        testPlanRepository.deleteTestPlanById(id);
    }

}
