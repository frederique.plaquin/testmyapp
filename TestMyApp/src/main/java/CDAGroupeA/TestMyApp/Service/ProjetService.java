package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Repository.ProjetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjetService {
    private final ProjetRepository projetRepository;

    @Autowired
    public ProjetService(ProjetRepository projetRepository) {
        this.projetRepository = projetRepository;
    }

    public Projet addProjet(Projet projet) { return projetRepository.save(projet); }

    public List<Projet> findAllProjets() {
        return projetRepository.findAll();
    }

    public Projet findProjetById(Long id) {
        return projetRepository.findProjetById(id)
                .orElseThrow(() -> new UserNotFoundException("L'utilisateur " + id + " n'a pas été trouvé !"));
    }

    public List<Projet> findProjetsByUserID(String id) {
        return projetRepository.findProjetsByUtilisateurId(id)
                .orElseThrow(() -> new UserNotFoundException("Aucun projet trouvé pour l'utilisateur " + id));
    }

    public Projet updateProjet(Projet projet) {
        return projetRepository.save(projet);
    }

}
