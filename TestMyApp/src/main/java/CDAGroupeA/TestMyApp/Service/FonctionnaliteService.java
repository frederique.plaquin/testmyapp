package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import CDAGroupeA.TestMyApp.Repository.FonctionnaliteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FonctionnaliteService {
    private final FonctionnaliteRepository fonctionnaliteRepository;

    @Autowired
    public FonctionnaliteService(FonctionnaliteRepository fonctionnaliteRepository) {
        this.fonctionnaliteRepository = fonctionnaliteRepository;
    }

    public Fonctionnalite addFonctionnalite(Fonctionnalite fonctionnalite) {
        return this.fonctionnaliteRepository.save(fonctionnalite);
    }

    public List<Fonctionnalite> findAllFonctionnalites() { return this.fonctionnaliteRepository.findAll(); }

    public Fonctionnalite findFonctionnaliteId(Long id) {
        return fonctionnaliteRepository.findFonctionnaliteById(id)
                .orElseThrow(() -> new UserNotFoundException("La fonctionnalité " + id + " n'a pas été trouvée!"));
    }

    @Modifying
    public Fonctionnalite updateFonctionnalite(Fonctionnalite fonctionnalite) {
        return fonctionnaliteRepository.save(fonctionnalite);
    }

    @Transactional
    public void deleteFonctionnalite(Long id) {
        fonctionnaliteRepository.deleteFonctionnaliteById(id);
    }
}
