package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Modele.Password;
import CDAGroupeA.TestMyApp.Repository.PasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {
    private final PasswordRepository passwordRepository;

    @Autowired
    public PasswordService(PasswordRepository passwordRepository) {
        this.passwordRepository = passwordRepository;
    }

    public Password addPassword(Password password) {
        return passwordRepository.save(password);
    }
}
