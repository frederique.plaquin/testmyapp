package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Role;
import CDAGroupeA.TestMyApp.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role addRole(Role role) { return roleRepository.save(role); }

    public List<Role> findAllRoles() {
        return roleRepository.findAll();
    }

    public Role findRoleById(Long id) {
        return roleRepository.findRoleById(id)
                .orElseThrow(() -> new UserNotFoundException("L'utilisateur " + id + " n'a pas été trouvé !"));
    }

    public Role findRoleByUserID(String id) {
        return roleRepository.findRoleByUtilisateurId(id)
                .orElseThrow(() -> new UserNotFoundException("Aucun role trouvé pour l'utilisateur " + id));
    }

    public Role updateRole(Role role) {
        return roleRepository.save(role);
    }
}
