package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Version;
import CDAGroupeA.TestMyApp.Repository.VersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class VersionService {
    private final VersionRepository versionRepository;

    @Autowired
    public VersionService(VersionRepository versionRepository) {
        this.versionRepository = versionRepository;
    }

    public Version addVersion(Version version) {
        return versionRepository.save(version);
    }

    public List<Version> findAllVersions() {
        return versionRepository.findAll();
    }

    public Version updateVersion(Version version) {
        return versionRepository.save(version);
    }

    public Version findVersionById(Long id) {
        return versionRepository.findVersionById(id)
                .orElseThrow(() -> new UserNotFoundException("L'utilisateur " + id + "n'a pas été trouvé !"));
    }

    @Transactional
    public void deleteVersionById(Long id) {
        versionRepository.deleteVersionById(id);
    }
}
