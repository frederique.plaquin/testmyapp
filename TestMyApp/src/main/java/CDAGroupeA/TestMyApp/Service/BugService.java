package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.Bug;
import CDAGroupeA.TestMyApp.Repository.BugRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BugService {
    private final BugRepository bugRepository;

    @Autowired
    public BugService(BugRepository bugRepository) {
        this.bugRepository = bugRepository;
    }

    public Bug addBug(Bug bug) {
        return this.bugRepository.save(bug);
    }

    public List<Bug> findAllBugs() { return this.bugRepository.findAll(); }


    public List<Bug> findBugsByProjetId(String id) {
        return bugRepository.findBugByProjetId(id)
                .orElseThrow(() -> new UserNotFoundException("Aucun bugs trouvé pour le projet " + id));
    }

    public List<Bug> findBugByFontionnaliteId(String id) {
        return bugRepository.findBugByFonctionnaliteId(id)
                .orElseThrow(() -> new UserNotFoundException("Aucun bugs trouvé pour la fonctionalité " + id));
    }


    public Bug findBugById(Long id) {
        return bugRepository.findBugById(id)
                .orElseThrow(() -> new UserNotFoundException("Le bug " + id + " n'a pas été trouvée!"));
    }

    @Modifying
    public Bug updateBug(Bug bug) {
        return bugRepository.save(bug);
    }

    @Transactional
    public void deleteBug(Long id) {
        bugRepository.deleteBugById(id);
    }


}
