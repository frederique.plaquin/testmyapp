package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.ChefsProjet;
import CDAGroupeA.TestMyApp.Modele.MembresProjet;
import CDAGroupeA.TestMyApp.Repository.ChefsProjetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ChefsProjetService {
    private final ChefsProjetRepository chefsProjetRepository;

    @Autowired
    public ChefsProjetService(ChefsProjetRepository chefsProjetRepository) {
        this.chefsProjetRepository = chefsProjetRepository;
    }

    public ChefsProjet addChefProjet(ChefsProjet chefsProjet) {return chefsProjetRepository.save(chefsProjet);}

    public List<ChefsProjet> findAllChefsProjet() {
        return chefsProjetRepository.findAll();
    }

    public ChefsProjet updateChefProjet(ChefsProjet chefsProjet) {
        return chefsProjetRepository.save(chefsProjet);
    }

    public ChefsProjet findChefProjetById(Long id) {
        return chefsProjetRepository.findChefsProjetById(id)
                .orElseThrow(() -> new UserNotFoundException("Le chef de projet " + id + " n'a pas été trouvé !"));
    }

    @Transactional
    public void deleteChefProjetById(Long id) {
        chefsProjetRepository.deleteChefsProjetById(id);
    }

}
