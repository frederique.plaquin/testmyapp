package CDAGroupeA.TestMyApp.Service;

import CDAGroupeA.TestMyApp.Exception.UserNotFoundException;
import CDAGroupeA.TestMyApp.Modele.MembresProjet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Repository.MembresProjetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class MembresProjetService {
    private final MembresProjetRepository membresProjetRepository;

    @Autowired
    public MembresProjetService(MembresProjetRepository membresProjetRepository) {
        this.membresProjetRepository = membresProjetRepository;
    }

    public MembresProjet addMembres(MembresProjet membresProjet) {
        return membresProjetRepository.save(membresProjet);
    }

    public List<MembresProjet> findAllMembreProjet() {
        return membresProjetRepository.findAll();
    }

    public MembresProjet updateMembreProjet(MembresProjet membresProjet) {
        return membresProjetRepository.save(membresProjet);
    }

    public MembresProjet findMembreProjetById(Long id) {
        return membresProjetRepository.findMembresProjetById(id)
                .orElseThrow(() -> new UserNotFoundException("L'utilisateur " + id + " n'a pas été trouvé !"));
    }

    @Transactional
    public void deleteMembreProjetById(Long id) {
        membresProjetRepository.deleteMembresProjetById(id);
    }

}
