package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.TypeErreur;
import CDAGroupeA.TestMyApp.Service.TypeErreurService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/type")
public class TypeErreurResource {
    private final TypeErreurService typeErreurService;

    public TypeErreurResource(TypeErreurService typeErreurService) {
        this.typeErreurService = typeErreurService;
    }

    /**
     * @return responseEntity that containt the typeError object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<TypeErreur>> getTypesErreurs() {
        List<TypeErreur> typeErreurs = typeErreurService.findAllTypesErreurs();
        return new ResponseEntity<>(typeErreurs, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<TypeErreur> addTypeErreur(@RequestBody TypeErreur typeErreur) {
        TypeErreur newTypeErreur = typeErreurService.addTypeErreur(typeErreur);
        return new ResponseEntity<>(newTypeErreur, HttpStatus.CREATED);
    }


    /**
     * @param id the id of the type error
     * @return responseEntity that containt the tyoe error object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<TypeErreur> getTypeErreurById (@PathVariable("id") Long id) {
        TypeErreur typeErreur = typeErreurService.findTypeErreurById(id);
        return new ResponseEntity<>(typeErreur, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<TypeErreur> updateTypeErreur(@RequestBody TypeErreur typeErreur){
        TypeErreur updateTypeErreur = typeErreurService.updateTypeErreur(typeErreur);
        return new ResponseEntity<>(updateTypeErreur, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTypeErreur(@PathVariable("id") Long id){
        typeErreurService.deleteTypeErreur(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
