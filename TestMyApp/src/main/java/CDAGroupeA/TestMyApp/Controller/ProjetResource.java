package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Service.ProjetService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projet")
public class ProjetResource {
    private final ProjetService projetService;

    public ProjetResource(ProjetService projetService) { this.projetService = projetService; }
    /**
     * @return responseEntity that containt the project object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<Projet>> getAllProjets () {
        List<Projet> projets = projetService.findAllProjets();
        return new ResponseEntity<>(projets, HttpStatus.OK);
    }
    /**
     * @param id the id of the project
     * @return responseEntity that containt the project object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<Projet> getProjetById (@PathVariable("id") Long id) {
        Projet projet = projetService.findProjetById(id);
        return new ResponseEntity<>(projet, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Projet> addProjet(@RequestBody Projet projet) {
        Projet newProjet = projetService.addProjet(projet);
        return new ResponseEntity<>(newProjet, HttpStatus.CREATED);
    }

    @GetMapping("/findByUser/{id}")
    public ResponseEntity<Projet[]> findProjetsByUserId(@PathVariable("id") String id) {
        System.out.println("bonjour");
        Projet[] projets = projetService.findProjetsByUserID(id).toArray(new Projet[0]);
        System.out.println(projets);
        return new ResponseEntity<>(projets, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Projet> updateProjet(@RequestBody Projet projet){
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@test");
        Projet modifiedProjet = projetService.updateProjet(projet);
        return new ResponseEntity<>(modifiedProjet, HttpStatus.OK);
    }
}
