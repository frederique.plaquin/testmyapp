package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.MembresProjet;
import CDAGroupeA.TestMyApp.Service.MembresProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/membreProjet")
public class MembresProjetResource {


    private final MembresProjetService membresProjetService;

    @Autowired
    public MembresProjetResource(MembresProjetService membresProjetService) {
        this.membresProjetService = membresProjetService;
    }

    /**
     * @return responseEntity that containt the membre Project object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<MembresProjet>> getAllMembres() {
        List<MembresProjet> membresProjets = membresProjetService.findAllMembreProjet();
        return new ResponseEntity<>(membresProjets, HttpStatus.OK);
    }
    /**
     * @param id the id of the memebre project
     * @return responseEntity that containt the memebre project object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<MembresProjet> getMembreById (@PathVariable("id") Long id) {
        MembresProjet membresProjet = membresProjetService.findMembreProjetById(id);
        return new ResponseEntity<>(membresProjet, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addMembreProjet(@RequestBody MembresProjet membresProjet) {

        MembresProjet newMembreProjet = membresProjetService.addMembres(membresProjet);
        return new ResponseEntity<>(newMembreProjet, HttpStatus.CREATED);

    }

    @PutMapping("/update")
    public ResponseEntity<MembresProjet> updateMembreProjet(@RequestBody MembresProjet membresProjet){
        MembresProjet updateMembreProjet = membresProjetService.updateMembreProjet(membresProjet);
        return new ResponseEntity<>(updateMembreProjet, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteMembreProjet(@PathVariable("id") Long id){
        membresProjetService.deleteMembreProjetById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

