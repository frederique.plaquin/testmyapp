package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import CDAGroupeA.TestMyApp.Service.FonctionnaliteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fonctionnalite")
public class FonctionnaliteResource {
    private final FonctionnaliteService fonctionnaliteService;

    public FonctionnaliteResource(FonctionnaliteService fonctionnaliteService) {
        this.fonctionnaliteService = fonctionnaliteService;
    }
    /**
     * @return responseEntity that containt the fonctionnality object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<Fonctionnalite>> getFonctionnalites() {
        List<Fonctionnalite> fonctionnalites = fonctionnaliteService.findAllFonctionnalites();
        return new ResponseEntity<>(fonctionnalites, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Fonctionnalite> addFonctionnalite(@RequestBody Fonctionnalite fonctionnalite) {
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        System.out.println(fonctionnalite);
        Fonctionnalite newFonctionnalite = fonctionnaliteService.addFonctionnalite(fonctionnalite);
        return new ResponseEntity<>(newFonctionnalite, HttpStatus.CREATED);
    }
    /**
     * @param id the id of the functionnality
     * @return responseEntity that containt the fonctionnality object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<Fonctionnalite> getFonctionnaliteById (@PathVariable("id") Long id) {
        Fonctionnalite fonctionnalite = fonctionnaliteService.findFonctionnaliteId(id);
        return new ResponseEntity<>(fonctionnalite, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Fonctionnalite> updateFonctionnalite(@RequestBody Fonctionnalite fonctionnalite){
        System.out.println("##################groupeFonctionnalites#########");
        System.out.println(fonctionnalite);
        Fonctionnalite updateFonctionnalite = fonctionnaliteService.updateFonctionnalite(fonctionnalite);
        return new ResponseEntity<>(updateFonctionnalite, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteFonctionnalite(@PathVariable("id") Long id){
        fonctionnaliteService.deleteFonctionnalite(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
