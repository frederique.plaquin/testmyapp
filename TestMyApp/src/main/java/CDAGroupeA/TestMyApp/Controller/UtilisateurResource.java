package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Service.UtilisateurService;
import CDAGroupeA.TestMyApp.authentification.ConfigSecurity.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/utilisateur")
public class UtilisateurResource {
    private final UtilisateurService utilisateurService;
    private final MyUserDetailsService myUserDetailsService;

        @Autowired
    public UtilisateurResource(UtilisateurService utilisateurService, MyUserDetailsService myUserDetailsService) {
        this.utilisateurService = utilisateurService;
        this.myUserDetailsService = myUserDetailsService;
    }

    /**
     * @return responseEntity that containt the user object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<Utilisateur>> getAllUtilisateurs () {
        List<Utilisateur> utilisateurs = utilisateurService.findAllUtilisateurs();
        return new ResponseEntity<>(utilisateurs, HttpStatus.OK);
    }
    /**
     *  @param id the id of the type user
     * @return responseEntity that containt the user object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<Utilisateur> getUtilisateurById (@PathVariable("id") Long id) {
        Utilisateur utilisateur = utilisateurService.findUtilisateurById(id);
        return new ResponseEntity<>(utilisateur, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addUtilisateur(@RequestBody Utilisateur utilisateur) {
        Optional<Utilisateur> user = myUserDetailsService.findUserByEmail(utilisateur.getMail());
        if(! user.isPresent()){
            Utilisateur newUtilisateur = utilisateurService.addUtilisateur(utilisateur);
            return new ResponseEntity<>(newUtilisateur, HttpStatus.CREATED);
        }else{
            return  ResponseEntity
                    .badRequest()
                    .body(HttpStatus.FORBIDDEN+" : Email already taken");
        }
        }

    @PutMapping("/update")
    public ResponseEntity<Utilisateur> updateUtilisateur(@RequestBody Utilisateur utilisateur){
        Utilisateur updateUtilisateur = utilisateurService.updateUtilisateur(utilisateur);
        return new ResponseEntity<>(updateUtilisateur, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUtilisateur(@PathVariable("id") Long id){
        utilisateurService.deleteUtilisateur(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
