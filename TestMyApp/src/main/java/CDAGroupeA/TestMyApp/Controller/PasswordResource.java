package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Password;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Service.PasswordService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/password")
public class PasswordResource {
    private final PasswordService passwordService;

    public PasswordResource(PasswordService passwordService) { this.passwordService = passwordService; }

    @PostMapping("/add")
    public ResponseEntity<Password> addPassword(@RequestBody Password password) {
        Password newPassword = passwordService.addPassword(password);
        return new ResponseEntity<>(newPassword, HttpStatus.CREATED);
    }
}
