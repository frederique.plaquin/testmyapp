package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.TestPlan;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Service.TestPlanService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/plan")
public class PlanResource {
    private final TestPlanService testPlanService;

    public PlanResource(TestPlanService testPlanService) {
        this.testPlanService = testPlanService;
    }

    /**
     * @return responseEntity that containt the TestPlans object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<TestPlan>> getAllPlans () {
        List<TestPlan> testPlans = testPlanService.findAllPlans();
        return new ResponseEntity<>(testPlans, HttpStatus.OK);
    }
    /**
     * @param id the id of the test plan
     * @return responseEntity that containt the test plan object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<TestPlan> getPlanById (@PathVariable("id") Long id) {
        TestPlan testPlan = testPlanService.findTestPlanById(id);
        return new ResponseEntity<>(testPlan, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<TestPlan> addPlan(@RequestBody TestPlan testPlan) {
        TestPlan newTestPlan = testPlanService.addTestPlan(testPlan);
        return new ResponseEntity<>(newTestPlan, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<TestPlan> updatePlan(@RequestBody TestPlan testPlan){
        TestPlan updateTestPlan = testPlanService.updateTestPlan(testPlan);
        return new ResponseEntity<>(updateTestPlan, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deletePlan(@PathVariable("id") Long id){
        testPlanService.deleteTestPlan(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
