package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import CDAGroupeA.TestMyApp.Service.UtilisateurService;
import CDAGroupeA.TestMyApp.authentification.ConfigSecurity.MyUserDetails;
import CDAGroupeA.TestMyApp.authentification.ConfigSecurity.MyUserDetailsService;
import CDAGroupeA.TestMyApp.authentification.token.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class UserAuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private MyUserDetailsService myUserDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UtilisateurService utilisateurService;

    @PostMapping("/utilisateur/connexion")
   // @RequestMapping(value="/authenticate", method= RequestMethod.POST)
    public ResponseEntity<?> loginUser(@Valid @RequestBody Utilisateur user){
        System.out.println(user);
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            user.getMail(),
                            user.getPassword().getHash()
                    )

            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtTokenUtil.generateJwtToken(authentication);
          //  System.out.println("###JWT## :" + jwt);
            MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();

            Optional<Utilisateur> myUser = this.myUserDetailsService.findUserByEmail(user.getMail());


            myUser.get().setToken(jwt);
            this.utilisateurService.updateUtilisateur(myUser.get());
            Utilisateur myUserAuth = myUser.get();
            return ResponseEntity.ok(
                    myUserAuth
            );

    }
}
