package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Bug;
import CDAGroupeA.TestMyApp.Service.BugService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/bug")
public class BugResource {
    private final BugService bugService;

    public BugResource(BugService bugService) {
        this.bugService = bugService;
    }

    /**
     * @return responseEntity that containt the bugs object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<Bug>> getBugs() {
        List<Bug> bugs = bugService.findAllBugs();
        return new ResponseEntity<>(bugs, HttpStatus.OK);
    }

    @GetMapping("/findByProjet/{id}")
    public ResponseEntity<List<Bug>> getBugsByProjetId(@PathVariable("id") String id) {
        List<Bug> bugsByProjet = bugService.findBugsByProjetId(id);
        return new ResponseEntity<>(bugsByProjet, HttpStatus.OK);
    }

    @GetMapping("/findByFonctionnalite/{id}")
    public ResponseEntity<List<Bug>> getBugsByFonctionnaliteId(@PathVariable("id") String id) {
        List<Bug> bugsByFonctionnalite = bugService.findBugByFontionnaliteId(id);
        return new ResponseEntity<>(bugsByFonctionnalite, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Bug> addBug(@RequestBody Bug bug) {
        Bug newBug = bugService.addBug(bug);
        return new ResponseEntity<>(newBug, HttpStatus.CREATED);
    }
    /**
     * @param id the id of the bug
     * @return responseEntity that containt the bug object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<Bug> getBugById (@PathVariable("id") Long id) {
        Bug bug = bugService.findBugById(id);
        return new ResponseEntity<>(bug, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Bug> updateBug(@RequestBody Bug bug){
        Bug updateBug = bugService.updateBug(bug);
        return new ResponseEntity<>(updateBug, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBug(@PathVariable("id") Long id){
        bugService.deleteBug(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
