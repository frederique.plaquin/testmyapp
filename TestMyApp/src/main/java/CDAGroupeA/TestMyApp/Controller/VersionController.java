package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.Version;
import CDAGroupeA.TestMyApp.Service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/version")
public class VersionController {

    private final VersionService versionService;

    @Autowired
    public VersionController(VersionService versionService) {
        this.versionService = versionService;
    }


    @GetMapping("/all")
    public ResponseEntity<List<Version>> getAllVersions() {
        List<Version> versions = versionService.findAllVersions();
        return new ResponseEntity<>(versions, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Version> getVersionById (@PathVariable("id") Long id) {
        Version version = versionService.findVersionById(id);
        return new ResponseEntity<>(version, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addVersion(@RequestBody Version version) {

        Version newVersion = versionService.addVersion(version);
        return new ResponseEntity<>(version, HttpStatus.CREATED);

    }

    @PutMapping("/update")
    public ResponseEntity<Version> updateVersion(@RequestBody Version version){
        Version updateVersion = versionService.updateVersion(version);
        return new ResponseEntity<>(updateVersion, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteVersion(@PathVariable("id") Long id){
        versionService.deleteVersionById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
