package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import CDAGroupeA.TestMyApp.Service.GroupeFonctionnalitesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/groupe")
public class GroupeFonctionnalitesResource {
    private final GroupeFonctionnalitesService groupeFonctionnalitesService;

    public GroupeFonctionnalitesResource(GroupeFonctionnalitesService groupeFonctionnalitesService) {
        this.groupeFonctionnalitesService = groupeFonctionnalitesService;
    }
    /**
     * @return responseEntity that containt the groupe of fonctionnality object and HttpStatus.ok
     */
    @GetMapping("/all")
    public ResponseEntity<List<GroupeFonctionnalites>> getAllGroupess () {
        List<GroupeFonctionnalites> groupesFonctionnalites = groupeFonctionnalitesService.findAllGroupes();
        return new ResponseEntity<>(groupesFonctionnalites, HttpStatus.OK);
    }
    /**
     * @param id the id of the groupr of functionnality
     * @return responseEntity that containt the groupe of functionnality object and HttpStatus.ok
     */
    @GetMapping("/find/{id}")
    public ResponseEntity<GroupeFonctionnalites> getGroupeById (@PathVariable("id") Long id) {
        GroupeFonctionnalites groupeFonctionnalites = groupeFonctionnalitesService.findGroupeById(id);
        return new ResponseEntity<>(groupeFonctionnalites, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<GroupeFonctionnalites> addGroupe(@RequestBody GroupeFonctionnalites groupeFonctionnalites) {
        GroupeFonctionnalites newGroupeFonctionnalites = groupeFonctionnalitesService.addGroupe(groupeFonctionnalites);
        return new ResponseEntity<>(newGroupeFonctionnalites, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<GroupeFonctionnalites> updateGroupeFonctionnalites(@RequestBody GroupeFonctionnalites groupeFonctionnalites){
        System.out.println("##################groupeFonctionnalites#########");
        System.out.println(groupeFonctionnalites);
        GroupeFonctionnalites updateGroupeFonctionnalites = groupeFonctionnalitesService.updateGroupeFonctionnalites(groupeFonctionnalites);
        return new ResponseEntity<>(updateGroupeFonctionnalites, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteGroupe(@PathVariable("id") Long id){
        groupeFonctionnalitesService.deleteGroupe(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
