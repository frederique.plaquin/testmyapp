package CDAGroupeA.TestMyApp.Controller;
import CDAGroupeA.TestMyApp.Modele.Role;
import CDAGroupeA.TestMyApp.Service.RoleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleResource {

    private final RoleService roleService;

    public RoleResource(RoleService roleService) { this.roleService = roleService; }

    @GetMapping("/all")
    public ResponseEntity<List<Role>> getAllRoles () {
        List<Role> roles = roleService.findAllRoles();
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Role> getRoleById (@PathVariable("id") Long id) {
        Role role = roleService.findRoleById(id);
        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Role> addRole(@RequestBody Role role) {
        Role newRole = roleService.addRole(role);
        return new ResponseEntity<>(newRole, HttpStatus.CREATED);
    }

    @GetMapping("/findByUser/{id}")
    public ResponseEntity<Role>  findRoleByUserId(@PathVariable("id") String id) {
        System.out.println("bonjour");
        Role role = roleService.findRoleByUserID(id);
        System.out.println(role);
        return new ResponseEntity<Role> (role, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<Role> updateRole(@RequestBody Role role){
        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@test");
        Role modifiedRole = roleService.updateRole(role);
        return new ResponseEntity<>(modifiedRole, HttpStatus.OK);
    }
}
