package CDAGroupeA.TestMyApp.Controller;

import CDAGroupeA.TestMyApp.Modele.ChefsProjet;
import CDAGroupeA.TestMyApp.Modele.MembresProjet;
import CDAGroupeA.TestMyApp.Service.ChefsProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/chefProjet")
public class ChefsProjetResource {
    private final ChefsProjetService chefsProjetService;

    @Autowired
    public ChefsProjetResource(ChefsProjetService chefsProjetService) {
        this.chefsProjetService = chefsProjetService;
    }


    @GetMapping("/all")
    public ResponseEntity<List<ChefsProjet>> getAllChefsProjet() {
        List<ChefsProjet> chefsProjets = chefsProjetService.findAllChefsProjet();
        return new ResponseEntity<>(chefsProjets, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<ChefsProjet> getChefProjetById (@PathVariable("id") Long id) {
        ChefsProjet chefsProjet = chefsProjetService.findChefProjetById(id);
        return new ResponseEntity<>(chefsProjet, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addChefProjet(@RequestBody ChefsProjet chefsProjet) {

        ChefsProjet newChefProjet = chefsProjetService.addChefProjet(chefsProjet);
        return new ResponseEntity<>(newChefProjet, HttpStatus.CREATED);

    }

    @PutMapping("/update")
    public ResponseEntity<ChefsProjet> updateChefProjet(@RequestBody ChefsProjet chefsProjet){
        ChefsProjet updateChefProjet = chefsProjetService.updateChefProjet(chefsProjet);
        return new ResponseEntity<>(updateChefProjet, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteChefsProjet(@PathVariable("id") Long id){
        chefsProjetService.deleteChefProjetById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
