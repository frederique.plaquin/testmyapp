package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.TypeErreur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TypeErreurRepository extends JpaRepository<TypeErreur, Long> {
    void deleteTypeErreurByid(Long id);

    Optional<TypeErreur> findTypeErreurById(Long id);

}