package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.List;

public interface ProjetRepository extends JpaRepository<Projet, Long> {

    Optional<Projet> findProjetById(Long id);

    @Query(value=" " +
            "SELECT * FROM projet " +
            "JOIN membres_projet " +
            "ON membres_projet.projet_projet_id = projet.projet_id " +
            "WHERE membres_projet.utilisateur_utilisateur_id = :id ", nativeQuery = true)
    Optional<List<Projet>> findProjetsByUtilisateurId(String id);

}