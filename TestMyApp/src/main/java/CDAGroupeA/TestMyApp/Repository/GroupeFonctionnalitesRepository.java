package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GroupeFonctionnalitesRepository extends JpaRepository<GroupeFonctionnalites, Long> {

    void deleteGroupeFonctionnalitesById(Long id);

    Optional<GroupeFonctionnalites> findGroupeFonctionnalitesById(Long id);

   
}