package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Password;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordRepository extends JpaRepository<Password, Integer> {
}