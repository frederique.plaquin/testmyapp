package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.GroupeFonctionnalites;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FonctionnaliteRepository extends JpaRepository<Fonctionnalite, Long> {
    void deleteFonctionnaliteById(Long id);

    Optional<Fonctionnalite> findFonctionnaliteById(Long id);
}