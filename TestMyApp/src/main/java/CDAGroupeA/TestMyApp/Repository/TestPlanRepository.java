package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.TestPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TestPlanRepository extends JpaRepository<TestPlan, Long> {

    void deleteTestPlanById(Long id);

    Optional<TestPlan> findTestPlanById(Long id);

}