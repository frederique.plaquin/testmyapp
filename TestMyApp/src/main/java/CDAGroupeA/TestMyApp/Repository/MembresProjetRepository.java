package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.MembresProjet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MembresProjetRepository extends JpaRepository<MembresProjet, Utilisateur> {

    void deleteMembresProjetById(Long id);

    Optional<MembresProjet> findMembresProjetById(Long id);


}