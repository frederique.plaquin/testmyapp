package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.ChefsProjet;
import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ChefsProjetRepository extends JpaRepository<ChefsProjet, Utilisateur> {
    void deleteChefsProjetById(Long id);

    Optional<ChefsProjet> findChefsProjetById(Long id);
}
