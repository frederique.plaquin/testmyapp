package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Bug;
import CDAGroupeA.TestMyApp.Modele.Fonctionnalite;
import CDAGroupeA.TestMyApp.Modele.Projet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BugRepository extends JpaRepository<Bug, Long> {
    void deleteBugById(Long id);

    Optional<Bug> findBugById(Long id);

    @Query(value=" " +
            "SELECT * FROM bug " +
            "JOIN projet " +
            "ON projet.projet_id = bug.projet_projet_id " +
            "WHERE projet.projet_id = :id ", nativeQuery = true)
    Optional<List<Bug>> findBugByProjetId(String id);

    @Query(value=" " +
            "SELECT * FROM bug " +
            "JOIN fonctionnalite " +
            "ON fonctionnalite.fonctionnalite_id = bug.fonctionnalite_fonctionnalite_id " +
            "WHERE fonctionnalite.fonctionnalite_id = :id", nativeQuery = true)
    Optional<List<Bug>> findBugByFonctionnaliteId(String id);

}
