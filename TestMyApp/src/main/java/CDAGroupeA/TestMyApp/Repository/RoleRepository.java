package CDAGroupeA.TestMyApp.Repository;

import CDAGroupeA.TestMyApp.Modele.Projet;
import CDAGroupeA.TestMyApp.Modele.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findRoleById(Long id);

    @Query(value=" " +
            "SELECT * FROM role " +
            "JOIN utilisateur " +
            "ON role.role_id = utilisateur.role_role_id " +
            "WHERE utilisateur.utilisateur_id = :id ", nativeQuery = true)
    Optional<Role> findRoleByUtilisateurId(String id);
}