package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "projet")
// @CascadeOnDelete
public class Projet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="projet_id", nullable = false)
    private Long id;
    private String nom;
    @Lob
    @Column(name="details", length = 512)
    private String details;
    private Date last_visit;
    private String version;
    private Boolean archive;

    // necessaire pr cascader fk targetEntity = MembresProjet.class, cascade = CascadeType.REMOVE
    // et @JoinColumn(name="utilisateur_utilisateur_id")
    @OneToMany(targetEntity = MembresProjet.class, cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="projet_projet_id")
    private List<MembresProjet> membresProjets;

    @OneToOne(mappedBy = "projet", cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="projet_id")
    private ChefsProjet chefProjet;

    @OneToMany(targetEntity = TestPlan.class, cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="projet_projet_id")
    private List<TestPlan> testPlans;

    @OneToMany(targetEntity = Version.class, cascade = CascadeType.REMOVE)
    @JoinColumn(name="projet_projet_id")
    private List<Version> versions;

    public Projet(Long id, String nom, String details, Date last_visit, String version, Boolean archive, List<MembresProjet> membresProjets, ChefsProjet chefProjet, List<TestPlan> testPlans, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.details = details;
        this.last_visit = last_visit;
        this.version = version;
        this.archive = archive;
        this.membresProjets = membresProjets;
        this.chefProjet = chefProjet;
        this.testPlans = testPlans;
    }

    public Projet() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getLast_visit() {
        return last_visit;
    }

    public void setLast_visit(Date last_visit) {
        this.last_visit = last_visit;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public List<MembresProjet> getMembresProjets() {
        return membresProjets;
    }

    public void setMembresProjets(List<MembresProjet> membresProjets) {
        this.membresProjets = membresProjets;
    }

    public ChefsProjet getChefProjet() {
        return chefProjet;
    }

    public void setChefProjet(ChefsProjet chefProjet) {
        this.chefProjet = chefProjet;
    }

    public List<TestPlan> getTestPlans() {
        return testPlans;
    }

    public void setTestPlans(List<TestPlan> testPlans) {
        this.testPlans = testPlans;
    }

    @Override
    public String toString() {
        return "Projet{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", details='" + details + '\'' +
                ", last_visit=" + last_visit +
                ", version='" + version + '\'' +
                ", archive=" + archive +
                ", membresProjets=" + membresProjets +
                ", chefProjet=" + chefProjet +
                ", testPlans=" + testPlans +
                '}';
    }
}
