package CDAGroupeA.TestMyApp.Modele;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "membres_projet")
// cle Pk aussi FK
// @IdClass(MembresProjet.class)
public class MembresProjet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="membre_id", nullable = false, updatable = false)
    private Long id;

    // cle Pk aussi FK
    // @MapsId
    // targetEntity = Utilisateur.class, cascade = CascadeType.REMOVE,
    @JoinColumn(name = "utilisateur_utilisateur_id")
    private Long utilisateur_utilisateur_id;

    // cle Pk aussi FK
    // @MapsId
    // targetEntity = Utilisateur.class, cascade = CascadeType.REMOVE,
    @JoinColumn(name = "projet_projet_id")
    private Long projet_projet_id;

    public MembresProjet(Long membre_id, Long utilisateur_utilisateur_id, Long projet_projet_id) {
        this.id = membre_id;
        this.utilisateur_utilisateur_id = utilisateur_utilisateur_id;
        this.projet_projet_id = projet_projet_id;
    }

    public MembresProjet() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long membre_id) {
        this.id = membre_id;
    }

    public Long getUtilisateur_utilisateur_id() {
        return utilisateur_utilisateur_id;
    }

    public void setUtilisateur_utilisateur_id(Long utilisateur_utilisateur_id) {
        this.utilisateur_utilisateur_id = utilisateur_utilisateur_id;
    }

    public Long getProjet_projet_id() {
        return projet_projet_id;
    }

    public void setProjet_projet_id(Long projet_projet_id) {
        this.projet_projet_id = projet_projet_id;
    }

    @Override
    public String toString() {
        return "MembresProjet{" +
                "membre_id=" + id +
                ", utilisateur_utilisateur_id=" + utilisateur_utilisateur_id +
                ", projet_projet_id=" + projet_projet_id +
                '}';
    }
}
