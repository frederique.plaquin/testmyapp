package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "type_erreur")
public class TypeErreur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="type_erreur_id", nullable = false, updatable = false)
    private Long id;
    private String nom;
    private String niveau;
    private String couleur;
    @Lob
    @Column(name="details", length = 512)
    private String details;
    private String priorite;
    private Boolean archive;

    // @OneToMany(targetEntity = Bug.class, cascade =
    //         CascadeType.REMOVE)
    // @JoinColumn(name="typeErreur_typeErreur_id")
    // private List<Bug> bugs;
    @JsonIgnore
    @OneToMany()
    @JsonManagedReference
    private List<Bug> bugs;

    public TypeErreur(Long id, String nom, String niveau, String couleur, String details, String priorite, Boolean archive, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.niveau = niveau;
        this.couleur = couleur;
        this.details = details;
        this.priorite = priorite;
        this.archive = archive;
        this.bugs = bugs;
    }

    public TypeErreur() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPriorite() {
        return priorite;
    }

    public void setPriorite(String priorite) {
        this.priorite = priorite;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    @Override
    public String toString() {
        return "TypeErreur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", niveau='" + niveau + '\'' +
                ", couleur='" + couleur + '\'' +
                ", details='" + details + '\'' +
                ", priorite='" + priorite + '\'' +
                ", archive=" + archive +
                ", bugs=" + bugs +
                '}';
    }
}
