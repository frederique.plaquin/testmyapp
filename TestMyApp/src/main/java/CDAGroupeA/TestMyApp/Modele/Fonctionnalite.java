package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "fonctionnalite")
public class Fonctionnalite implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="fonctionnalite_id", nullable = false, updatable = false)
    private Long id;
    private String nom;
    @Lob
    @Column(name="details", length = 512)
    private String details;
    @Lob
    @Column(name="resultat_attendu", length = 512)
    private String resultatAttendu;
    @Lob
    @Column(name="scenario", length = 512)
    private String scenario;
    private Integer priorite;
    private Integer periodicite;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date testee;
    private Boolean archive;
    private Long groupe_fonctionnalites_groupe_id;

    // @OneToMany(targetEntity = Bug.class, cascade =
    //        CascadeType.REMOVE)
    // @JoinColumn(name="fonctionnalite_fonctionnalite_id")
    // private List<Bug> bugsTesteurs;
    @JsonIgnore
    @OneToMany()
    @JsonManagedReference
    private List<Bug> bugs;

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    public Fonctionnalite(Long id, String nom, String details, String resultatAttendu, String scenario, Integer priorite, Integer periodicite, Date testee, Boolean archive, Long groupe_fonctionnalites_groupe_id, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.details = details;
        this.resultatAttendu = resultatAttendu;
        this.scenario = scenario;
        this.priorite = priorite;
        this.periodicite = periodicite;
        this.testee = testee;
        this.archive = archive;
        this.groupe_fonctionnalites_groupe_id = groupe_fonctionnalites_groupe_id;
        this.bugs = bugs;
    }

    public Fonctionnalite() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getResultatAttendu() {
        return resultatAttendu;
    }

    public void setResultatAttendu(String resultat_attendu) {
        this.resultatAttendu = resultat_attendu;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public Integer getPriorite() {
        return priorite;
    }

    public void setPriorite(Integer priorite) {
        this.priorite = priorite;
    }

    public Integer getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(Integer periodicite) {
        this.periodicite = periodicite;
    }

    public Date getTestee() {
        return testee;
    }

    public void setTestee(Date testee) {
        this.testee = testee;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Long getGroupe_fonctionnalites_groupe_id() {
        return groupe_fonctionnalites_groupe_id;
    }

    public void setGroupe_fonctionnalites_groupe_id(Long groupe_fonctionnalites_groupe_id) {
        this.groupe_fonctionnalites_groupe_id = groupe_fonctionnalites_groupe_id;
    }

    @Override
    public String toString() {
        return "Fonctionnalite{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", details='" + details + '\'' +
                ", resultatAttendu='" + resultatAttendu + '\'' +
                ", scenario='" + scenario + '\'' +
                ", priorite=" + priorite +
                ", periodicite='" + periodicite + '\'' +
                ", testee=" + testee +
                ", archive=" + archive +
                ", groupe_fonctionnalites_groupe_id=" + groupe_fonctionnalites_groupe_id +
                ", bugs=" + bugs +
                '}';
    }
}
