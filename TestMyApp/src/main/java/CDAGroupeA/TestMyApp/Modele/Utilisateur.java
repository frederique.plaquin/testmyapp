package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

// Serializable = transform the class into differents types of stream so serializable will help with the whole process
@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "utilisateur")
// @CascadeOnDelete
public class Utilisateur implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="utilisateur_id", nullable = false, updatable = false)
    private Long id;
    private String nom;
    private String prenom;
    @Column(name="mail", unique=true)
    private String mail;
    private String imageUrl;
    private Boolean confirmed;
    private String token;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = Password.class, cascade =
            CascadeType.ALL)
    @JoinColumn(nullable = false)
    private Password password;

    // @ManyToOne(targetEntity = Role.class)
    // @JoinColumn(name = "role_role_id", nullable = true)
    // private Role role;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Role.class, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "role_role_id")
    private Role role;

    // necessaire pr cascader fk targetEntity = MembresProjet.class, cascade = CascadeType.REMOVE
    // et @JoinColumn(name="utilisateur_utilisateur_id")
    @OneToMany(targetEntity = MembresProjet.class, cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="utilisateur_utilisateur_id")
    private List<MembresProjet> membresProjets;


    @OneToOne(mappedBy = "utilisateur", cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="utilisateur_utilisateur_id")
    private ChefsProjet chefProjet;

    public Utilisateur(Long id, String nom, String prenom, String mail, String imageUrl, Boolean confirmed, String token, Password password, Role role, List<MembresProjet> membresProjets, ChefsProjet chefProjet, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.imageUrl = imageUrl;
        this.confirmed = confirmed;
        this.token = token;
        this.password = password;
        this.role = role;
        this.membresProjets = membresProjets;
        this.chefProjet = chefProjet;
    }

    public Utilisateur() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<MembresProjet> getMembresProjets() {
        return membresProjets;
    }

    public void setMembresProjets(List<MembresProjet> membresProjets) {
        this.membresProjets = membresProjets;
    }

    public ChefsProjet getChefProjet() {
        return chefProjet;
    }

    public void setChefProjet(ChefsProjet chefProjet) {
        this.chefProjet = chefProjet;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", mail='" + mail + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", confirmed=" + confirmed +
                ", token='" + token + '\'' +
                ", password=" + password +
                ", role=" + role +
                ", membresProjets=" + membresProjets +
                ", chefProjet=" + chefProjet +
                '}';
    }
}
