package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "role")
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="role_id", nullable = false, updatable = false)
    private Integer id;
    private String role;

    //@JsonIgnore
    //@OneToMany()
    //@JsonManagedReference
    //private List<Utilisateur> utilisateurs;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = Utilisateur.class)
    private List<Utilisateur> utilisateurs;

    public Role(Integer id, String role, List<Utilisateur> utilisateurs) {
        this.id = id;
        this.role = role;
        this.utilisateurs = utilisateurs;
    }

    public Role() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }

   public void setUtilisateurs(List<Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", utilisateurs=" + utilisateurs +
                '}';
    }
}

