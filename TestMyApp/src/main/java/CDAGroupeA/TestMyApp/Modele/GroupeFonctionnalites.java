package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "groupe_fonctionnalites")
public class GroupeFonctionnalites implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "groupe_id", nullable = false, updatable = false)
    private Long id;
    private String nom;
    private String periodicite;
    private Boolean archive;
    private Boolean draft;
    @Column(updatable = false)
    private Long test_plan_plan_id;

    @OneToMany(targetEntity = Fonctionnalite.class, cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="groupe_fonctionnalites_groupe_id")
    private List<Fonctionnalite> fonctionnalites;

    // @OneToMany(targetEntity = Bug.class, cascade =
    //        CascadeType.REMOVE)
    // @JoinColumn(name="groupe_fonctionnalites_groupe_id")
    // private List<Bug> bugs;
    @JsonIgnore
    @OneToMany()
    @JsonManagedReference
    private List<Bug> bugs;

    public GroupeFonctionnalites(Long id, String nom, String periodicite, Boolean archive, Boolean draft, Long test_plan_plan_id, List<Fonctionnalite> fonctionnalites, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.periodicite = periodicite;
        this.archive = archive;
        this.draft = draft;
        this.test_plan_plan_id = test_plan_plan_id;
        this.fonctionnalites = fonctionnalites;
        this.bugs = bugs;
    }

    public GroupeFonctionnalites() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    public Long getTest_plan_plan_id() {
        return test_plan_plan_id;
    }

    public void setTest_plan_plan_id(Long test_plan_plan_id) {
        this.test_plan_plan_id = test_plan_plan_id;
    }

    public List<Fonctionnalite> getFonctionnalites() {
        return fonctionnalites;
    }

    public void setFonctionnalites(List<Fonctionnalite> fonctionnalites) {
        this.fonctionnalites = fonctionnalites;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    @Override
    public String toString() {
        return "GroupeFonctionnalites{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", periodicite='" + periodicite + '\'' +
                ", archive=" + archive +
                ", draft=" + draft +
                ", test_plan_plan_id=" + test_plan_plan_id +
                ", fonctionnalites=" + fonctionnalites +
                ", bugs=" + bugs +
                '}';
    }
}
