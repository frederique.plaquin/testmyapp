package CDAGroupeA.TestMyApp.Modele;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "chefs_projet")
public class ChefsProjet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="chef_id")
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = Utilisateur.class)
    @JoinColumn(name = "utilisateur_utilisateur_id")
    private Utilisateur utilisateur;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = Projet.class)
    @JoinColumn(name="projet_id")
    private Projet projet;

    public ChefsProjet(Long id, Utilisateur utilisateurId, Projet projet_projet_id) {
        this.id = id;
        this.utilisateur = utilisateurId;
        this.projet = projet_projet_id;
    }

    public ChefsProjet() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    @Override
    public String toString() {
        return "ChefsProjet{" +
                "id=" + id +
                ", utilisateur=" + utilisateur +
                ", projet=" + projet +
                '}';
    }
}
