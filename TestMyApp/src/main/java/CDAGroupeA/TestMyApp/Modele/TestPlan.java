package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "test_plan")
public class TestPlan implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="plan_id", nullable = false, updatable = false)
    private Long id;
    private String nom;
    @Lob
    @Column(name="details", length = 512)
    private String details;
    private Boolean archive;
    @JoinColumn(name = "projet_projet_id")
    private Long projet_projet_id;

    @OneToMany(targetEntity = GroupeFonctionnalites.class, cascade =
            CascadeType.REMOVE)
    @JoinColumn(name="test_plan_plan_id")
    private List<GroupeFonctionnalites> groupeFonctionnalites;

    // @OneToMany(targetEntity = Bug.class, cascade =
    //        CascadeType.REMOVE)
    // @JoinColumn(name="test_plan_plan_id")
    // private List<Bug> bugs;
    @JsonIgnore
    @OneToMany()
    @JsonManagedReference
    private List<Bug> bugs;

    public TestPlan(Long id, String nom, String details, Boolean archive, Long projet_projet_id, List<GroupeFonctionnalites> groupeFonctionnalites, List<Bug> bugs) {
        this.id = id;
        this.nom = nom;
        this.details = details;
        this.archive = archive;
        this.projet_projet_id = projet_projet_id;
        this.groupeFonctionnalites = groupeFonctionnalites;
        this.bugs = bugs;
    }

    public TestPlan() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Long getProjet_projet_id() {
        return projet_projet_id;
    }

    public void setProjet_projet_id(Long projet_projet_id) {
        this.projet_projet_id = projet_projet_id;
    }

    public List<GroupeFonctionnalites> getGroupeFonctionnalites() {
        return groupeFonctionnalites;
    }

    public void setGroupeFonctionnalites(List<GroupeFonctionnalites> groupeFonctionnalites) {
        this.groupeFonctionnalites = groupeFonctionnalites;
    }

    public List<Bug> getBugs() {
        return bugs;
    }

    public void setBugs(List<Bug> bugs) {
        this.bugs = bugs;
    }

    @Override
    public String toString() {
        return "TestPlan{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", details='" + details + '\'' +
                ", archive=" + archive +
                ", projet_projet_id=" + projet_projet_id +
                ", groupeFonctionnalites=" + groupeFonctionnalites +
                ", bugs=" + bugs +
                '}';
    }
}
