package CDAGroupeA.TestMyApp.Modele;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name="version_projet")
public class Version implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="version_id", nullable = false, updatable = false)
    private Long id;
    private String version;

    @JoinColumn(name = "projet_projet_id")
    private Long projet_projet_id;

    public Version(Long id, String version, Long projet_projet_id) {
        this.id = id;
        this.version = version;
        this.projet_projet_id = projet_projet_id;
    }

    public Version() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Long getProjet_projet_id() {
        return projet_projet_id;
    }

    public void setProjet_projet_id(Long projet_projet_id) {
        this.projet_projet_id = projet_projet_id;
    }

    @Override
    public String toString() {
        return "Version{" +
                "id=" + id +
                ", version='" + version + '\'' +
                ", projet_projet_id=" + projet_projet_id +
                '}';
    }
}
