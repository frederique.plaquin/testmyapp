package CDAGroupeA.TestMyApp.Modele;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@DynamicUpdate
@SelectBeforeUpdate
@Table(name = "bug")
public class Bug implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="bug_id", nullable = false, updatable = false)
    private Long id;
    @Lob
    @Column(name="details", length = 512)
    private String details;
    private String version_ouvert;
    private String version_resolu;
    private String commentaire;
    private Date date;
    private String status;
    private Boolean archive;
    @ManyToOne(targetEntity = Projet.class, fetch=FetchType.EAGER)
    @JoinColumn(name="projet_projet_id")
    private Projet projet_projet_id;

    @ManyToOne(targetEntity = TestPlan.class, fetch=FetchType.EAGER)
    @JoinColumn(name="test_plan_plan_id")
    private TestPlan testPlan;
    @ManyToOne(targetEntity = GroupeFonctionnalites.class, fetch=FetchType.EAGER)
    @JoinColumn(name="groupe_fonctionnalites_groupe_id")
    private GroupeFonctionnalites groupeFonctionnalites;
    @ManyToOne(targetEntity = Fonctionnalite.class, fetch=FetchType.EAGER)
    @JoinColumn(name="fonctionnalite_fonctionnalite_id")
    private Fonctionnalite fonctionnalite;
    @ManyToOne(targetEntity = Utilisateur.class, fetch=FetchType.EAGER)
    @JoinColumn(name="testeur_id_utilisateur_id")
    private Utilisateur betaTesteur;
    @ManyToOne(targetEntity = Utilisateur.class, fetch=FetchType.EAGER)
    @JoinColumn(name="assigne_a_utilisateur_id", nullable = true)
    private Utilisateur assigne_a_utilisateur_id;
    @ManyToOne(targetEntity = TypeErreur.class, fetch=FetchType.EAGER)
    @JoinColumn(name="type_erreur_type_erreur_id")
    private TypeErreur typeErreur;

    public Bug(Long id, String details, String version_ouvert, String version_resolu, String commentaire, Date date, String status, Boolean archive, Projet projet_projet_id, Utilisateur assigne_a_utilisateur_id, TestPlan testPlan, GroupeFonctionnalites groupeFonctionnalites, Fonctionnalite fonctionnalite, Utilisateur betaTesteur, TypeErreur typeErreur) {
        this.id = id;
        this.details = details;
        this.version_ouvert = version_ouvert;
        this.version_resolu = version_resolu;
        this.commentaire = commentaire;
        this.date = date;
        this.status = status;
        this.archive = archive;
        this.projet_projet_id = projet_projet_id;
        this.assigne_a_utilisateur_id = assigne_a_utilisateur_id;
        this.testPlan = testPlan;
        this.groupeFonctionnalites = groupeFonctionnalites;
        this.fonctionnalite = fonctionnalite;
        this.betaTesteur = betaTesteur;
        this.typeErreur = typeErreur;
    }

    public Bug() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getVersion_ouvert() {
        return version_ouvert;
    }

    public void setVersion_ouvert(String version_ouvert) {
        this.version_ouvert = version_ouvert;
    }

    public String getVersion_resolu() {
        return version_resolu;
    }

    public void setVersion_resolu(String version_resolu) {
        this.version_resolu = version_resolu;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Projet getProjet_projet_id() {
        return projet_projet_id;
    }

    public void setProjet_projet_id(Projet projet_projet_id) {
        this.projet_projet_id = projet_projet_id;
    }

    public Utilisateur getAssigne_a_utilisateur_id() {
        return assigne_a_utilisateur_id;
    }

    public void setAssigne_a_utilisateur_id(Utilisateur assigne_a_projet_id) {
        this.assigne_a_utilisateur_id = assigne_a_projet_id;
    }

    public TestPlan getTestPlan() {
        return testPlan;
    }

    public void setTestPlan(TestPlan testPlan) {
        this.testPlan = testPlan;
    }

    public GroupeFonctionnalites getGroupeFonctionnalites() {
        return groupeFonctionnalites;
    }

    public void setGroupeFonctionnalites(GroupeFonctionnalites groupeFonctionnalites) {
        this.groupeFonctionnalites = groupeFonctionnalites;
    }

    public Fonctionnalite getFonctionnalite() {
        return fonctionnalite;
    }

    public void setFonctionnalite(Fonctionnalite fonctionnalite) {
        this.fonctionnalite = fonctionnalite;
    }

    public Utilisateur getBetaTesteur() {
        return betaTesteur;
    }

    public void setBetaTesteur(Utilisateur betaTesteur) {
        this.betaTesteur = betaTesteur;
    }

    public TypeErreur getTypeErreur() {
        return typeErreur;
    }

    public void setTypeErreur(TypeErreur typeErreur) {
        this.typeErreur = typeErreur;
    }

    @Override
    public String toString() {
        return "Bug{" +
                "id=" + id +
                ", details='" + details + '\'' +
                ", version_ouvert='" + version_ouvert + '\'' +
                ", version_resolu='" + version_resolu + '\'' +
                ", commentaire='" + commentaire + '\'' +
                ", date=" + date +
                ", status='" + status + '\'' +
                ", archive=" + archive +
                ", projet_projet_id=" + projet_projet_id +
                ", assigne_a_projet_id=" + assigne_a_utilisateur_id +
                ", testPlan=" + testPlan +
                ", groupeFonctionnalites=" + groupeFonctionnalites +
                ", fonctionnalite=" + fonctionnalite +
                ", betaTesteur=" + betaTesteur +
                ", typeErreur=" + typeErreur +
                '}';
    }
}
