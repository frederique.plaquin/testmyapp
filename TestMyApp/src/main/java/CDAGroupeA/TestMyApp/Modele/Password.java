package CDAGroupeA.TestMyApp.Modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Password implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private Integer password_id;
    @Column(nullable = false)
    private String hash;


    public Password() {}

    public Password(String hash, String crypt) {
        this.hash = hash;
    }

    public Integer getPassword_id() {
        return password_id;
    }

    public void setPassword_id(Integer password_id) {
        this.password_id = password_id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }


    @Override
    public String toString() {
        return "Password{" +
                "password_id=" + password_id +
                ", hash='" + hash + '\'' +
                '}';
    }
}
