package CDAGroupeA.TestMyApp.authentification.ConfigSecurity;

import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MyUserDetails implements UserDetails {
    //private  static  final long serialVersionUID=1L;
    private Long id;
    private String email;
    private String password;
    private boolean isActive;
    private List<GrantedAuthority> authorities;



        public MyUserDetails(Utilisateur utilisateur) {
        this.email = utilisateur.getMail();
        this.password = utilisateur.getPassword().getHash();
        this.isActive = utilisateur.getConfirmed();
        this.authorities = Arrays.stream(utilisateur.getRole().getRole().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
