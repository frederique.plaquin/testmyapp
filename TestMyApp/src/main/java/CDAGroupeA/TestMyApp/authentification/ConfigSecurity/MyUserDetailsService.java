package CDAGroupeA.TestMyApp.authentification.ConfigSecurity;

import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class MyUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    @Autowired
    public MyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<Utilisateur> findUserByEmail(String mail){
        return  this.userRepository.findByMail(mail);
    }



    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
      //  System.out.println("emaaaaaaaaaaaaaaaaaaaaaail : : "+email);
        Optional<Utilisateur> utilisateur = userRepository.findByMail(email);

        //System.out.println("uuuuuuuuuuuuuuuuuser::::"+utilisateur);

        utilisateur.orElseThrow(()-> new UsernameNotFoundException(email+ "Not found ") );
        return utilisateur.map(MyUserDetails::new).get();
    }
}

