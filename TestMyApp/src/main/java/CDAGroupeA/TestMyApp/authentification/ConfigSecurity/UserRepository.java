package CDAGroupeA.TestMyApp.authentification.ConfigSecurity;

import CDAGroupeA.TestMyApp.Modele.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Utilisateur, Long> {
    Optional<Utilisateur> findByMail(String Email);
}
